-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 06, 2017 at 05:03 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ntt191`
--

-- --------------------------------------------------------

--
-- Table structure for table `adodb_logsql`
--

CREATE TABLE `adodb_logsql` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `created` datetime NOT NULL,
  `sql0` varchar(250) NOT NULL DEFAULT '',
  `sql1` text,
  `params` text,
  `tracer` text,
  `timer` decimal(16,6) NOT NULL DEFAULT '0.000000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='to save some logs from ADOdb';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_assignment`
--

CREATE TABLE `mdl_assignment` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `duration` bigint(10) UNSIGNED NOT NULL,
  `point` bigint(7) UNSIGNED NOT NULL DEFAULT '0',
  `format` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `assignmenttype` varchar(50) NOT NULL DEFAULT '',
  `resubmit` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `preventlate` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `emailteachers` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `var1` bigint(10) DEFAULT '0',
  `var2` bigint(10) DEFAULT '0',
  `var3` bigint(10) DEFAULT '0',
  `var4` bigint(10) DEFAULT '0',
  `var5` bigint(10) DEFAULT '0',
  `maxbytes` bigint(10) UNSIGNED NOT NULL DEFAULT '100000',
  `timedue` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timeavailable` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `grade` bigint(10) NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Defines assignments';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_assignment_submissions`
--

CREATE TABLE `mdl_assignment_submissions` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `assignment` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timecreated` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `numfiles` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `data1` text,
  `data2` text,
  `grade` bigint(11) NOT NULL DEFAULT '0',
  `submissioncomment` text NOT NULL,
  `format` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `teacher` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timemarked` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `mailed` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `completeddate` varchar(55) DEFAULT NULL,
  `casestudy` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Info about submitted assignments';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_attendance_log`
--

CREATE TABLE `mdl_attendance_log` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `sessionid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `studentid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `statusid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `statusset` varchar(100) DEFAULT NULL,
  `timetaken` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `takenby` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `remarks` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='attendance_log table retrofitted from MySQL';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_attendance_sessions`
--

CREATE TABLE `mdl_attendance_sessions` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `courseid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `sessdate` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `duration` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `lasttaken` bigint(10) UNSIGNED DEFAULT NULL,
  `lasttakenby` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED DEFAULT NULL,
  `description` varchar(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='attendance_sessions table retrofitted from MySQL';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_attendance_statuses`
--

CREATE TABLE `mdl_attendance_statuses` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `courseid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `acronym` varchar(2) NOT NULL DEFAULT '',
  `description` varchar(30) NOT NULL DEFAULT '',
  `grade` smallint(3) NOT NULL DEFAULT '0',
  `visible` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='attendance_statuses table retrofitted from MySQL';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_attforblock`
--

CREATE TABLE `mdl_attforblock` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `grade` bigint(10) NOT NULL DEFAULT '100'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Module for support Attendances';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_backup_config`
--

CREATE TABLE `mdl_backup_config` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `value` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='To store backup configuration variables';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_backup_courses`
--

CREATE TABLE `mdl_backup_courses` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `courseid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `laststarttime` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `lastendtime` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `laststatus` varchar(1) NOT NULL DEFAULT '0',
  `nextstarttime` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='To store every course backup status';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_backup_files`
--

CREATE TABLE `mdl_backup_files` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `backup_code` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `file_type` varchar(10) NOT NULL DEFAULT '',
  `path` varchar(255) NOT NULL DEFAULT '',
  `old_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `new_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='To store and recode ids to user and course files';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_backup_ids`
--

CREATE TABLE `mdl_backup_ids` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `backup_code` bigint(12) UNSIGNED NOT NULL DEFAULT '0',
  `table_name` varchar(30) NOT NULL DEFAULT '',
  `old_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `new_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `info` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='To store and convert ids in backup/restore';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_backup_log`
--

CREATE TABLE `mdl_backup_log` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `courseid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `time` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `laststarttime` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `info` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='To store every course backup log info';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_block`
--

CREATE TABLE `mdl_block` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `name` varchar(40) NOT NULL DEFAULT '',
  `version` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `cron` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `lastcron` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `multiple` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='to store installed blocks';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_block_fn_my_menu`
--

CREATE TABLE `mdl_block_fn_my_menu` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `name` varchar(40) NOT NULL DEFAULT '',
  `version` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `cron` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `lastcron` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `visible` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `multiple` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='block_fn_my_menu table retrofitted from MySQL';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_block_fn_my_menu_group_settings`
--

CREATE TABLE `mdl_block_fn_my_menu_group_settings` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `groupid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(40) NOT NULL DEFAULT '',
  `courseid` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='block_fn_my_menu_group_settings table retrofitted from MySQL';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_block_instance`
--

CREATE TABLE `mdl_block_instance` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `blockid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `pageid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `pagetype` varchar(20) NOT NULL DEFAULT '',
  `position` varchar(10) NOT NULL DEFAULT '',
  `weight` smallint(3) NOT NULL DEFAULT '0',
  `visible` tinyint(1) NOT NULL DEFAULT '0',
  `configdata` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='to store block instances in pages';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_block_moodle_notifications_courses`
--

CREATE TABLE `mdl_block_moodle_notifications_courses` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course_id` bigint(11) UNSIGNED DEFAULT '0',
  `last_notification_time` bigint(11) UNSIGNED DEFAULT '0',
  `notify_by_email` tinyint(1) UNSIGNED DEFAULT '1',
  `notify_by_sms` tinyint(1) UNSIGNED DEFAULT '1',
  `notify_by_rss` tinyint(1) UNSIGNED DEFAULT '1',
  `notification_frequency` bigint(11) UNSIGNED DEFAULT '43200',
  `email_notification_preset` tinyint(1) UNSIGNED DEFAULT '1',
  `sms_notification_preset` tinyint(1) UNSIGNED DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Courses table';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_block_moodle_notifications_log`
--

CREATE TABLE `mdl_block_moodle_notifications_log` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course_id` bigint(10) UNSIGNED NOT NULL,
  `module_id` bigint(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'resource',
  `action` enum('added','updated','deleted') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'added',
  `status` enum('notified','pending') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'notified'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='block_moodle_notifications_log table retrofitted from MySQL';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_block_moodle_notifications_users`
--

CREATE TABLE `mdl_block_moodle_notifications_users` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `user_id` bigint(10) UNSIGNED DEFAULT '0',
  `course_id` bigint(10) UNSIGNED DEFAULT '0',
  `notify_by_email` tinyint(1) UNSIGNED DEFAULT '1',
  `notify_by_sms` tinyint(1) UNSIGNED DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='This table contains user preferences';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_block_my_courses`
--

CREATE TABLE `mdl_block_my_courses` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `category_name` varchar(255) NOT NULL DEFAULT '',
  `collapsed` smallint(4) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Keeps track of the collapsed/expanded status for a category ';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_block_pinned`
--

CREATE TABLE `mdl_block_pinned` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `blockid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `pagetype` varchar(20) NOT NULL DEFAULT '',
  `position` varchar(10) NOT NULL DEFAULT '',
  `weight` smallint(3) NOT NULL DEFAULT '0',
  `visible` tinyint(1) NOT NULL DEFAULT '0',
  `configdata` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='to pin blocks';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_block_rate_course`
--

CREATE TABLE `mdl_block_rate_course` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `rating` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='To store ratings given to course';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_block_rss_client`
--

CREATE TABLE `mdl_block_rss_client` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `title` text NOT NULL,
  `preferredtitle` varchar(64) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `shared` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `url` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Remote news feed information. Contains the news feed id, the';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_block_search_documents`
--

CREATE TABLE `mdl_block_search_documents` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `docid` varchar(32) NOT NULL DEFAULT '',
  `doctype` varchar(32) NOT NULL DEFAULT 'none',
  `itemtype` varchar(32) NOT NULL DEFAULT 'standard',
  `title` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `docdate` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `updated` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `courseid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `groupid` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table to store search index backups';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_booking`
--

CREATE TABLE `mdl_booking` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `format` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `bookingmanager` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sendmail` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `copymail` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `allowupdate` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `agb` text COLLATE utf8_unicode_ci NOT NULL,
  `timeopen` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timeclose` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `limitanswers` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `maxanswers` bigint(10) UNSIGNED DEFAULT '0',
  `maxoverbooking` bigint(10) UNSIGNED DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Available bookings are stored here';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_booking_answers`
--

CREATE TABLE `mdl_booking_answers` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `bookingid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `optionid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='bookings performed by users';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_booking_options`
--

CREATE TABLE `mdl_booking_options` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `bookingid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `text` text COLLATE utf8_unicode_ci,
  `maxanswers` bigint(10) UNSIGNED DEFAULT '0',
  `maxoverbooking` bigint(10) UNSIGNED DEFAULT '0',
  `location` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `venue` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `room` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bookingclosingtime` bigint(10) UNSIGNED DEFAULT '0',
  `courseid` bigint(10) UNSIGNED DEFAULT '0',
  `coursestarttime` bigint(10) UNSIGNED DEFAULT '0',
  `courseendtime` bigint(10) UNSIGNED DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `limitanswers` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='available options to booking';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_bu_description`
--

CREATE TABLE `mdl_bu_description` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `bu` varchar(20) DEFAULT '0',
  `budescription` varchar(125) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mdl_cache_filters`
--

CREATE TABLE `mdl_cache_filters` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `filter` varchar(32) NOT NULL DEFAULT '',
  `version` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `md5key` varchar(32) NOT NULL DEFAULT '',
  `rawtext` text NOT NULL,
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='For keeping information about cached data';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_cache_flags`
--

CREATE TABLE `mdl_cache_flags` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `flagtype` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `value` mediumtext NOT NULL,
  `expiry` bigint(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Cache of time-sensitive flags';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_cache_text`
--

CREATE TABLE `mdl_cache_text` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `md5key` varchar(32) NOT NULL DEFAULT '',
  `formattedtext` longtext NOT NULL,
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='For storing temporary copies of processed texts';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_capabilities`
--

CREATE TABLE `mdl_capabilities` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `captype` varchar(50) NOT NULL DEFAULT '',
  `contextlevel` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `component` varchar(100) NOT NULL DEFAULT '',
  `riskbitmask` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='this defines all capabilities';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_certificate`
--

CREATE TABLE `mdl_certificate` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `emailteachers` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `emailothers` text,
  `savecert` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `reportcert` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `delivery` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `certificatetype` varchar(50) NOT NULL DEFAULT '',
  `borderstyle` varchar(30) NOT NULL DEFAULT '0',
  `bordercolor` varchar(30) NOT NULL DEFAULT '0',
  `printwmark` varchar(30) NOT NULL DEFAULT '0',
  `printdate` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `datefmt` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `printnumber` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `printgrade` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `gradefmt` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `printoutcome` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `printhours` text,
  `lockgrade` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `requiredgrade` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `printteacher` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `customtext` text,
  `printsignature` varchar(30) NOT NULL DEFAULT '0',
  `printseal` varchar(30) NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Defines certificates';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_certificate_issues`
--

CREATE TABLE `mdl_certificate_issues` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `certificateid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timecreated` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `studentname` varchar(40) NOT NULL DEFAULT '',
  `code` varchar(40) DEFAULT NULL,
  `classname` varchar(254) NOT NULL DEFAULT '',
  `certdate` bigint(10) UNSIGNED DEFAULT '0',
  `reportgrade` varchar(10) DEFAULT NULL,
  `mailed` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Info about issued certificates';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_certificate_linked_modules`
--

CREATE TABLE `mdl_certificate_linked_modules` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `certificate_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `linkid` bigint(10) NOT NULL DEFAULT '0',
  `linkgrade` bigint(10) NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Defines certificate dependencies';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_chat`
--

CREATE TABLE `mdl_chat` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `intro` text NOT NULL,
  `keepdays` bigint(11) NOT NULL DEFAULT '0',
  `studentlogs` smallint(4) NOT NULL DEFAULT '0',
  `chattime` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `schedule` smallint(4) NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Each of these is a chat room';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_chat_messages`
--

CREATE TABLE `mdl_chat_messages` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `chatid` bigint(10) NOT NULL DEFAULT '0',
  `userid` bigint(10) NOT NULL DEFAULT '0',
  `groupid` bigint(10) NOT NULL DEFAULT '0',
  `system` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `message` text NOT NULL,
  `timestamp` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores all the actual chat messages';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_chat_users`
--

CREATE TABLE `mdl_chat_users` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `chatid` bigint(11) NOT NULL DEFAULT '0',
  `userid` bigint(11) NOT NULL DEFAULT '0',
  `groupid` bigint(11) NOT NULL DEFAULT '0',
  `version` varchar(16) NOT NULL DEFAULT '',
  `ip` varchar(15) NOT NULL DEFAULT '',
  `firstping` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `lastping` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `lastmessageping` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `sid` varchar(32) NOT NULL DEFAULT '',
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `lang` varchar(30) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Keeps track of which users are in which chat rooms';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_choice`
--

CREATE TABLE `mdl_choice` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `text` text NOT NULL,
  `format` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `publish` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `showresults` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `display` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `allowupdate` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `showunanswered` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `limitanswers` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `timeopen` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timeclose` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Available choices are stored here';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_choice_answers`
--

CREATE TABLE `mdl_choice_answers` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `choiceid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `optionid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='choices performed by users';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_choice_options`
--

CREATE TABLE `mdl_choice_options` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `choiceid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `text` text,
  `maxanswers` bigint(10) UNSIGNED DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='available options to choice';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_classroom`
--

CREATE TABLE `mdl_classroom` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `thirdparty` varchar(255) DEFAULT NULL,
  `thirdpartywaitlist` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `display` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `confirmationsubject` text,
  `confirmationinstrmngr` mediumtext,
  `confirmationmessage` mediumtext,
  `waitlistedsubject` mediumtext,
  `waitlistedmessage` mediumtext,
  `cancellationsubject` text,
  `cancellationinstrmngr` mediumtext,
  `cancellationmessage` mediumtext,
  `absenteessubject` text,
  `absenteesinstrmngr` mediumtext,
  `absenteesmessage` mediumtext,
  `remindersubject` text,
  `reminderinstrmngr` mediumtext,
  `remindermessage` mediumtext,
  `reminderperiod` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timecreated` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `trainerconfirmationmessage` mediumtext NOT NULL,
  `cancelprogram` text,
  `cancelprogrammessage` mediumtext,
  `description` text,
  `multiplesession` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Each classroom activity has an entry here';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_classroom_detailsmaster`
--

CREATE TABLE `mdl_classroom_detailsmaster` (
  `id` int(10) UNSIGNED NOT NULL,
  `account` varchar(255) DEFAULT NULL,
  `practise` varchar(255) DEFAULT NULL,
  `trainingtype` varchar(255) DEFAULT NULL,
  `trainingsource` varchar(255) DEFAULT NULL,
  `sessioncategory` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mdl_classroom_sessions`
--

CREATE TABLE `mdl_classroom_sessions` (
  `id` bigint(10) NOT NULL,
  `classroom` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `capacity` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `location` varchar(255) DEFAULT NULL,
  `venue` varchar(255) DEFAULT NULL,
  `room` varchar(255) DEFAULT NULL,
  `sessioncategory` varchar(255) DEFAULT NULL,
  `details` mediumtext,
  `reschedulereason` mediumtext,
  `cancelreason` varchar(255) DEFAULT NULL,
  `datetimeknown` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `duration` bigint(10) UNSIGNED DEFAULT NULL,
  `normalcost` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `discountcost` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `closed` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `timecreated` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(20) UNSIGNED DEFAULT '0',
  `timecancelled` bigint(20) UNSIGNED DEFAULT '0',
  `programename` varchar(255) DEFAULT NULL,
  `feedbackname` varchar(255) DEFAULT NULL,
  `trainingtype` varchar(255) DEFAULT NULL,
  `account` varchar(255) DEFAULT NULL,
  `trainingsource` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `requestor` varchar(6) DEFAULT NULL,
  `recurring` bit(1) DEFAULT b'0',
  `externaltraining` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='A given classroom activity may be given at different times ';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_classroom_sessions_dates`
--

CREATE TABLE `mdl_classroom_sessions_dates` (
  `id` bigint(10) NOT NULL,
  `sessionid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timestart` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `timefinish` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='The dates and times for each session.  Sessions can be set o';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_classroom_sessions_external`
--

CREATE TABLE `mdl_classroom_sessions_external` (
  `id` bigint(10) NOT NULL,
  `sessionid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `certificate` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `eventnamefile` varchar(105) NOT NULL,
  `issuedby` varchar(65) NOT NULL,
  `timemodified` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mdl_classroom_submissions`
--

CREATE TABLE `mdl_classroom_submissions` (
  `id` bigint(10) NOT NULL,
  `classroom` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `sessionid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `grade` bigint(10) NOT NULL DEFAULT '0',
  `mailedconfirmation` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `mailedreminder` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `mailedfeedback` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `mailedabsentees` bigint(20) DEFAULT NULL,
  `discountcode` text,
  `timegraded` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `timecreated` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `timecancelled` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `notificationtype` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `attend` bigint(3) UNSIGNED DEFAULT NULL,
  `cancelreasons` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='An entry is created here whenever a user signs up for a face';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_classroom_submissions_backup2015`
--

CREATE TABLE `mdl_classroom_submissions_backup2015` (
  `id` bigint(10) NOT NULL DEFAULT '0',
  `classroom` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `sessionid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `grade` bigint(10) NOT NULL DEFAULT '0',
  `mailedconfirmation` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `mailedreminder` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `mailedfeedback` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `mailedabsentees` bigint(20) DEFAULT NULL,
  `discountcode` text,
  `timegraded` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `timecreated` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `timecancelled` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `notificationtype` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `attend` bigint(3) UNSIGNED DEFAULT NULL,
  `cancelreasons` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mdl_classroom_trainners`
--

CREATE TABLE `mdl_classroom_trainners` (
  `id` bigint(10) NOT NULL,
  `classroom` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `sessionid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `grade` bigint(10) NOT NULL DEFAULT '0',
  `mailedconfirmation` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `mailedreminder` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `discountcode` text,
  `timegraded` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `timecreated` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `timecancelled` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `notificationtype` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `attend` bigint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='An entry is created here whenever a trainer is added';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_config`
--

CREATE TABLE `mdl_config` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Moodle configuration variables';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_config_plugins`
--

CREATE TABLE `mdl_config_plugins` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `plugin` varchar(100) NOT NULL DEFAULT 'core',
  `name` varchar(100) NOT NULL DEFAULT '',
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Moodle modules and plugins configuration variables';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_config_plugins_2017`
--

CREATE TABLE `mdl_config_plugins_2017` (
  `id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `plugin` varchar(100) NOT NULL DEFAULT 'core',
  `name` varchar(100) NOT NULL DEFAULT '',
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mdl_context`
--

CREATE TABLE `mdl_context` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `contextlevel` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `instanceid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `path` varchar(255) DEFAULT NULL,
  `depth` tinyint(2) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='one of these must be set';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_context_temp`
--

CREATE TABLE `mdl_context_temp` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `path` varchar(255) NOT NULL DEFAULT '',
  `depth` tinyint(2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Used by build_context_path() in upgrade and cron to keep con';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_course`
--

CREATE TABLE `mdl_course` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `category` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `sortorder` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `password` varchar(50) NOT NULL DEFAULT '',
  `fullname` varchar(254) NOT NULL DEFAULT '',
  `shortname` varchar(100) NOT NULL DEFAULT '',
  `idnumber` varchar(100) NOT NULL DEFAULT '',
  `summary` text,
  `format` varchar(10) NOT NULL DEFAULT 'topics',
  `showgrades` tinyint(2) UNSIGNED NOT NULL DEFAULT '1',
  `modinfo` longtext,
  `newsitems` mediumint(5) UNSIGNED NOT NULL DEFAULT '1',
  `teacher` varchar(100) NOT NULL DEFAULT 'Teacher',
  `teachers` varchar(100) NOT NULL DEFAULT 'Teachers',
  `student` varchar(100) NOT NULL DEFAULT 'Student',
  `students` varchar(100) NOT NULL DEFAULT 'Students',
  `guest` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `startdate` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `enrolperiod` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `numsections` mediumint(5) UNSIGNED NOT NULL DEFAULT '1',
  `marker` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `maxbytes` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `showreports` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `visible` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `hiddensections` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `groupmode` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `groupmodeforce` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `defaultgroupingid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `lang` varchar(30) NOT NULL DEFAULT '',
  `theme` varchar(50) NOT NULL DEFAULT '',
  `cost` varchar(10) NOT NULL DEFAULT '',
  `currency` varchar(3) NOT NULL DEFAULT 'USD',
  `timecreated` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `metacourse` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `requested` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `restrictmodules` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `expirynotify` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `expirythreshold` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `notifystudents` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `enrollable` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `enrolstartdate` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `enrolenddate` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `enrol` varchar(20) NOT NULL DEFAULT '',
  `defaultrole` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `practice` varchar(50) DEFAULT NULL,
  `stream` varchar(50) DEFAULT NULL,
  `grade` varchar(4) DEFAULT NULL,
  `level` tinyint(2) UNSIGNED DEFAULT NULL,
  `approval` tinyint(2) UNSIGNED DEFAULT NULL,
  `helpdesk` varchar(100) DEFAULT NULL,
  `prerequisite` varchar(254) DEFAULT NULL,
  `enrollgrade` varchar(4) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `enrolmentmessage` text,
  `sendmessage` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `automail` tinyint(1) DEFAULT NULL,
  `completionmessage` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Central course table';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_course_allowed_modules`
--

CREATE TABLE `mdl_course_allowed_modules` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `module` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='allowed modules foreach course' ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `mdl_course_allowed_modules_temp`
--

CREATE TABLE `mdl_course_allowed_modules_temp` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `module` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='allowed modules foreach course';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_course_categories`
--

CREATE TABLE `mdl_course_categories` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `parent` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `sortorder` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `coursecount` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `depth` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `path` varchar(255) NOT NULL DEFAULT '',
  `theme` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Course categories' ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `mdl_course_categories_temp`
--

CREATE TABLE `mdl_course_categories_temp` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `parent` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `sortorder` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `coursecount` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `depth` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `path` varchar(255) NOT NULL DEFAULT '',
  `theme` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Course categories';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_course_display`
--

CREATE TABLE `mdl_course_display` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `display` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores info about how to display the course';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_course_meta`
--

CREATE TABLE `mdl_course_meta` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `parent_course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `child_course` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='to store meta-courses relations';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_course_modules`
--

CREATE TABLE `mdl_course_modules` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `module` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `instance` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `section` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `idnumber` varchar(100) DEFAULT NULL,
  `added` bigint(10) UNSIGNED DEFAULT '0',
  `score` smallint(4) DEFAULT '0',
  `indent` mediumint(5) UNSIGNED DEFAULT '0',
  `visible` tinyint(1) DEFAULT '1',
  `visibleold` tinyint(1) DEFAULT '1',
  `groupmode` smallint(4) DEFAULT '0',
  `groupingid` bigint(10) UNSIGNED DEFAULT '0',
  `groupmembersonly` smallint(4) UNSIGNED DEFAULT '0',
  `delay` varchar(10) DEFAULT NULL,
  `visiblewhenlocked` smallint(2) UNSIGNED DEFAULT NULL,
  `checkboxforcomplete` smallint(2) UNSIGNED DEFAULT NULL,
  `checkboxesforprereqs` smallint(2) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='course_modules table retrofitted from MySQL';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_course_module_locks`
--

CREATE TABLE `mdl_course_module_locks` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `courseid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `moduleid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `lockid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `requirement` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='table to store activity locks';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_course_request`
--

CREATE TABLE `mdl_course_request` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `fullname` varchar(254) NOT NULL DEFAULT '',
  `shortname` varchar(15) NOT NULL DEFAULT '',
  `summary` text NOT NULL,
  `reason` text NOT NULL,
  `requester` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `password` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='course requests';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_course_sections`
--

CREATE TABLE `mdl_course_sections` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `section` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `summary` text,
  `sequence` text,
  `visible` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='to define the sections for each course';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_course_temp`
--

CREATE TABLE `mdl_course_temp` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `category` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `sortorder` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `password` varchar(50) NOT NULL DEFAULT '',
  `fullname` varchar(254) NOT NULL DEFAULT '',
  `shortname` varchar(100) NOT NULL DEFAULT '',
  `idnumber` varchar(100) NOT NULL DEFAULT '',
  `summary` text,
  `format` varchar(10) NOT NULL DEFAULT 'topics',
  `showgrades` tinyint(2) UNSIGNED NOT NULL DEFAULT '1',
  `modinfo` longtext,
  `newsitems` mediumint(5) UNSIGNED NOT NULL DEFAULT '1',
  `teacher` varchar(100) NOT NULL DEFAULT 'Teacher',
  `teachers` varchar(100) NOT NULL DEFAULT 'Teachers',
  `student` varchar(100) NOT NULL DEFAULT 'Student',
  `students` varchar(100) NOT NULL DEFAULT 'Students',
  `guest` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `startdate` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `enrolperiod` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `numsections` mediumint(5) UNSIGNED NOT NULL DEFAULT '1',
  `marker` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `maxbytes` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `showreports` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `visible` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `hiddensections` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `groupmode` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `groupmodeforce` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `defaultgroupingid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `lang` varchar(30) NOT NULL DEFAULT '',
  `theme` varchar(50) NOT NULL DEFAULT '',
  `cost` varchar(10) NOT NULL DEFAULT '',
  `currency` varchar(3) NOT NULL DEFAULT 'USD',
  `timecreated` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `metacourse` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `requested` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `restrictmodules` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `expirynotify` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `expirythreshold` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `notifystudents` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `enrollable` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `enrolstartdate` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `enrolenddate` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `enrol` varchar(20) NOT NULL DEFAULT '',
  `defaultrole` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `practice` varchar(50) DEFAULT NULL,
  `stream` varchar(50) DEFAULT NULL,
  `grade` varchar(4) DEFAULT NULL,
  `level` tinyint(2) UNSIGNED DEFAULT NULL,
  `approval` tinyint(2) UNSIGNED DEFAULT NULL,
  `helpdesk` varchar(100) DEFAULT NULL,
  `prerequisite` varchar(254) DEFAULT NULL,
  `enrollgrade` varchar(4) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `enrolmentmessage` text,
  `sendmessage` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Central course table';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_cpd`
--

CREATE TABLE `mdl_cpd` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `userid` bigint(10) UNSIGNED NOT NULL,
  `objective` longtext,
  `development_need` longtext,
  `activitytypeid` bigint(10) UNSIGNED DEFAULT NULL,
  `activity` longtext,
  `duedate` bigint(10) UNSIGNED DEFAULT NULL,
  `startdate` bigint(10) UNSIGNED DEFAULT NULL,
  `enddate` bigint(10) UNSIGNED DEFAULT NULL,
  `statusid` bigint(10) UNSIGNED DEFAULT NULL,
  `cpdyearid` bigint(10) UNSIGNED NOT NULL,
  `timetaken` bigint(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Defines user CPDs';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_cpd_activity_type`
--

CREATE TABLE `mdl_cpd_activity_type` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Defines the activity types';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_cpd_status`
--

CREATE TABLE `mdl_cpd_status` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `display_order` bigint(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Defines the statuses';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_cpd_year`
--

CREATE TABLE `mdl_cpd_year` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `startdate` bigint(10) UNSIGNED NOT NULL,
  `enddate` bigint(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Defines the periods to view CPD data';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_csr`
--

CREATE TABLE `mdl_csr` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thirdparty` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thirdpartywaitlist` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `display` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `confirmationsubject` text COLLATE utf8_unicode_ci,
  `confirmationinstrmngr` mediumtext COLLATE utf8_unicode_ci,
  `confirmationmessage` mediumtext COLLATE utf8_unicode_ci,
  `waitlistedsubject` mediumtext COLLATE utf8_unicode_ci,
  `waitlistedmessage` mediumtext COLLATE utf8_unicode_ci,
  `cancellationsubject` text COLLATE utf8_unicode_ci,
  `cancellationinstrmngr` mediumtext COLLATE utf8_unicode_ci,
  `cancellationmessage` mediumtext COLLATE utf8_unicode_ci,
  `remindersubject` text COLLATE utf8_unicode_ci,
  `reminderinstrmngr` mediumtext COLLATE utf8_unicode_ci,
  `remindermessage` mediumtext COLLATE utf8_unicode_ci,
  `reminderperiod` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `requestsubject` text COLLATE utf8_unicode_ci,
  `requestinstrmngr` mediumtext COLLATE utf8_unicode_ci,
  `requestmessage` mediumtext COLLATE utf8_unicode_ci,
  `timecreated` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `shortname` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci,
  `showoncalendar` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `approvalreqd` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `guidelines` varchar(20000) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Each csr activity has an entry here';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_csr_notice`
--

CREATE TABLE `mdl_csr_notice` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text` mediumtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Site-wide notices shown on the Training Calendar';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_csr_notice_data`
--

CREATE TABLE `mdl_csr_notice_data` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `fieldid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `noticeid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Custom field filters for site notices';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_csr_sessions`
--

CREATE TABLE `mdl_csr_sessions` (
  `id` bigint(10) NOT NULL,
  `csr` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `capacity` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `allowoverbook` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `details` mediumtext COLLATE utf8_unicode_ci,
  `datetimeknown` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `duration` bigint(10) UNSIGNED DEFAULT NULL,
  `normalcost` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `discountcost` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timecreated` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `eventname` text COLLATE utf8_unicode_ci NOT NULL,
  `goals` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) CHARACTER SET latin1 NOT NULL,
  `subcategory` varchar(255) CHARACTER SET latin1 NOT NULL,
  `location` text COLLATE utf8_unicode_ci NOT NULL,
  `venue` text COLLATE utf8_unicode_ci,
  `userevent` bigint(11) UNSIGNED DEFAULT '0',
  `contactperson` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='A given csr activity may be given at different times and pla';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_csr_sessions_dates`
--

CREATE TABLE `mdl_csr_sessions_dates` (
  `id` bigint(10) NOT NULL,
  `sessionid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timestart` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `timefinish` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='The dates and times for each session.  Sessions can be set o';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_csr_session_data`
--

CREATE TABLE `mdl_csr_session_data` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `fieldid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `sessionid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contents of custom info fields for CSR session';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_csr_session_field`
--

CREATE TABLE `mdl_csr_session_field` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shortname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `possiblevalues` mediumtext COLLATE utf8_unicode_ci,
  `required` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `defaultvalue` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isfilter` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `showinsummary` tinyint(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Definitions of custom info fields for CSR session';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_csr_session_roles`
--

CREATE TABLE `mdl_csr_session_roles` (
  `id` bigint(10) NOT NULL,
  `sessionid` bigint(10) UNSIGNED NOT NULL,
  `roleid` bigint(10) UNSIGNED NOT NULL,
  `userid` bigint(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Users with a trainer role in a csr session';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_csr_signups`
--

CREATE TABLE `mdl_csr_signups` (
  `id` bigint(10) NOT NULL,
  `sessionid` bigint(10) UNSIGNED NOT NULL,
  `userid` bigint(10) UNSIGNED NOT NULL,
  `mailedreminder` bigint(10) UNSIGNED NOT NULL,
  `discountcode` text COLLATE utf8_unicode_ci,
  `notificationtype` bigint(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='User/session signups';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_csr_signups_status`
--

CREATE TABLE `mdl_csr_signups_status` (
  `id` bigint(10) NOT NULL,
  `signupid` bigint(10) UNSIGNED NOT NULL,
  `statuscode` bigint(10) UNSIGNED NOT NULL,
  `superceded` tinyint(1) UNSIGNED NOT NULL,
  `grade` decimal(10,5) DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `advice` text COLLATE utf8_unicode_ci,
  `createdby` bigint(10) UNSIGNED NOT NULL,
  `timecreated` bigint(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='User/session signup status';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_csr_user_signups`
--

CREATE TABLE `mdl_csr_user_signups` (
  `id` bigint(10) NOT NULL,
  `sessionid` bigint(10) UNSIGNED DEFAULT NULL,
  `userid` bigint(10) UNSIGNED DEFAULT NULL,
  `grade` bigint(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='User/session signups';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_data`
--

CREATE TABLE `mdl_data` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `intro` text NOT NULL,
  `comments` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `timeavailablefrom` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timeavailableto` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timeviewfrom` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timeviewto` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `requiredentries` int(8) UNSIGNED NOT NULL DEFAULT '0',
  `requiredentriestoview` int(8) UNSIGNED NOT NULL DEFAULT '0',
  `maxentries` int(8) UNSIGNED NOT NULL DEFAULT '0',
  `rssarticles` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `singletemplate` text,
  `listtemplate` text,
  `listtemplateheader` text,
  `listtemplatefooter` text,
  `addtemplate` text,
  `rsstemplate` text,
  `rsstitletemplate` text,
  `csstemplate` text,
  `jstemplate` text,
  `asearchtemplate` text,
  `approval` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `scale` bigint(10) NOT NULL DEFAULT '0',
  `assessed` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `defaultsort` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `defaultsortdir` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `editany` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `notification` bigint(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Removed ratings column';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_data_comments`
--

CREATE TABLE `mdl_data_comments` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `recordid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  `format` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `created` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `modified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='to comment data records';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_data_content`
--

CREATE TABLE `mdl_data_content` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `fieldid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `recordid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `content` longtext,
  `content1` longtext,
  `content2` longtext,
  `content3` longtext,
  `content4` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='the content introduced in each record/fields';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_data_fields`
--

CREATE TABLE `mdl_data_fields` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `dataid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `param1` text,
  `param2` text,
  `param3` text,
  `param4` text,
  `param5` text,
  `param6` text,
  `param7` text,
  `param8` text,
  `param9` text,
  `param10` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='every field available';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_data_ratings`
--

CREATE TABLE `mdl_data_ratings` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `recordid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `rating` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='to rate data records';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_data_records`
--

CREATE TABLE `mdl_data_records` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `groupid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `dataid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timecreated` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `approved` smallint(4) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='every record introduced';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_elearning_question`
--

CREATE TABLE `mdl_elearning_question` (
  `Id` bigint(10) NOT NULL,
  `Question_text` text,
  `required` tinyint(1) DEFAULT NULL,
  `timecreated` bigint(10) DEFAULT NULL,
  `timemodified` bigint(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mdl_elearning_value`
--

CREATE TABLE `mdl_elearning_value` (
  `Id` bigint(10) NOT NULL,
  `question` bigint(10) DEFAULT NULL,
  `answer` text,
  `user` bigint(11) DEFAULT NULL,
  `course` bigint(11) DEFAULT NULL,
  `timecreated` bigint(10) DEFAULT NULL,
  `timemodified` bigint(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mdl_enrol_approvalworkflow`
--

CREATE TABLE `mdl_enrol_approvalworkflow` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `courseid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `created` bigint(10) UNSIGNED NOT NULL,
  `updated` bigint(10) UNSIGNED NOT NULL,
  `status` bigint(11) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='enrol_approvalworkflow table retrofitted from MySQL';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_enrol_authorize`
--

CREATE TABLE `mdl_enrol_authorize` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `paymentmethod` enum('cc','echeck') NOT NULL DEFAULT 'cc',
  `refundinfo` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `ccname` varchar(255) NOT NULL DEFAULT '',
  `courseid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `transid` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `status` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timecreated` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `settletime` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `amount` varchar(10) NOT NULL DEFAULT '',
  `currency` varchar(3) NOT NULL DEFAULT 'USD'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Holds all known information about authorize.net transactions';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_enrol_authorize_refunds`
--

CREATE TABLE `mdl_enrol_authorize_refunds` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `orderid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `amount` varchar(10) NOT NULL DEFAULT '',
  `transid` bigint(20) UNSIGNED DEFAULT '0',
  `settletime` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Authorize.net refunds';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_enrol_paypal`
--

CREATE TABLE `mdl_enrol_paypal` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `business` varchar(255) NOT NULL DEFAULT '',
  `receiver_email` varchar(255) NOT NULL DEFAULT '',
  `receiver_id` varchar(255) NOT NULL DEFAULT '',
  `item_name` varchar(255) NOT NULL DEFAULT '',
  `courseid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `memo` varchar(255) NOT NULL DEFAULT '',
  `tax` varchar(255) NOT NULL DEFAULT '',
  `option_name1` varchar(255) NOT NULL DEFAULT '',
  `option_selection1_x` varchar(255) NOT NULL DEFAULT '',
  `option_name2` varchar(255) NOT NULL DEFAULT '',
  `option_selection2_x` varchar(255) NOT NULL DEFAULT '',
  `payment_status` varchar(255) NOT NULL DEFAULT '',
  `pending_reason` varchar(255) NOT NULL DEFAULT '',
  `reason_code` varchar(30) NOT NULL DEFAULT '',
  `txn_id` varchar(255) NOT NULL DEFAULT '',
  `parent_txn_id` varchar(255) NOT NULL DEFAULT '',
  `payment_type` varchar(30) NOT NULL DEFAULT '',
  `timeupdated` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Holds all known information about PayPal transactions';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_enrol_skillsoft`
--

CREATE TABLE `mdl_enrol_skillsoft` (
  `id` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `courseid` int(11) DEFAULT NULL,
  `reason` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  `updated` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mdl_enrol_userregister`
--

CREATE TABLE `mdl_enrol_userregister` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `courseid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `created` bigint(10) UNSIGNED NOT NULL,
  `updated` bigint(10) UNSIGNED NOT NULL,
  `status` bigint(11) UNSIGNED NOT NULL DEFAULT '0',
  `coursegroup` bigint(10) UNSIGNED DEFAULT NULL,
  `approver` bigint(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='enrol_approvalworkflow table retrofitted from MySQL' ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `mdl_event`
--

CREATE TABLE `mdl_event` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `format` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `courseid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `groupid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `repeatid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `modulename` varchar(20) NOT NULL DEFAULT '',
  `instance` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `eventtype` varchar(20) NOT NULL DEFAULT '',
  `timestart` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timeduration` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `visible` smallint(4) NOT NULL DEFAULT '1',
  `uuid` varchar(36) NOT NULL DEFAULT '',
  `sequence` bigint(10) UNSIGNED NOT NULL DEFAULT '1',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='For everything with a time associated to it';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_events_handlers`
--

CREATE TABLE `mdl_events_handlers` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `eventname` varchar(166) NOT NULL DEFAULT '',
  `handlermodule` varchar(166) NOT NULL DEFAULT '',
  `handlerfile` varchar(255) NOT NULL DEFAULT '',
  `handlerfunction` mediumtext,
  `schedule` varchar(255) DEFAULT NULL,
  `status` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This table is for storing which components requests what typ';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_events_queue`
--

CREATE TABLE `mdl_events_queue` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `eventdata` longtext NOT NULL,
  `stackdump` mediumtext,
  `userid` bigint(10) UNSIGNED DEFAULT NULL,
  `timecreated` bigint(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This table is for storing queued events. It stores only one ';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_events_queue_handlers`
--

CREATE TABLE `mdl_events_queue_handlers` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `queuedeventid` bigint(10) UNSIGNED NOT NULL,
  `handlerid` bigint(10) UNSIGNED NOT NULL,
  `status` bigint(10) DEFAULT NULL,
  `errormessage` mediumtext,
  `timemodified` bigint(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This is the list of queued handlers for processing. The even';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_feedback`
--

CREATE TABLE `mdl_feedback` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `summary` text NOT NULL,
  `anonymous` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `email_notification` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `multiple_submit` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `autonumbering` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `site_after_submit` varchar(255) NOT NULL DEFAULT '',
  `page_after_submit` text NOT NULL,
  `publish_stats` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `timeopen` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timeclose` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='all feedbacks';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_feedback_completed`
--

CREATE TABLE `mdl_feedback_completed` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `feedback` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `random_response` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `anonymous_response` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='filled out feedback';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_feedback_completedtmp`
--

CREATE TABLE `mdl_feedback_completedtmp` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `feedback` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `guestid` varchar(255) NOT NULL DEFAULT '',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `random_response` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `anonymous_response` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='filled out feedback';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_feedback_ext_employee_response`
--

CREATE TABLE `mdl_feedback_ext_employee_response` (
  `id` bigint(10) NOT NULL,
  `userid` bigint(10) DEFAULT NULL,
  `question_id` bigint(10) DEFAULT NULL,
  `response` text,
  `datecreated` bigint(20) DEFAULT NULL,
  `datemodified` bigint(20) DEFAULT NULL,
  `comments` text,
  `seesionid` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mdl_feedback_ext_items`
--

CREATE TABLE `mdl_feedback_ext_items` (
  `id` bigint(10) NOT NULL,
  `question_id` bigint(10) DEFAULT NULL,
  `itemname` varchar(100) DEFAULT NULL,
  `itemvalue` bigint(10) DEFAULT NULL,
  `datecreated` bigint(20) DEFAULT NULL,
  `datemodified` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mdl_feedback_ext_manager_response`
--

CREATE TABLE `mdl_feedback_ext_manager_response` (
  `id` bigint(10) NOT NULL,
  `userid` bigint(10) DEFAULT NULL,
  `resource_id` varchar(10) DEFAULT NULL,
  `question_id` bigint(10) DEFAULT NULL,
  `response` text,
  `datecreated` bigint(20) DEFAULT NULL,
  `datemodified` bigint(20) DEFAULT NULL,
  `comments` text,
  `seesionid` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mdl_feedback_ext_question`
--

CREATE TABLE `mdl_feedback_ext_question` (
  `id` bigint(10) NOT NULL,
  `questiontext` text,
  `datecreated` bigint(20) DEFAULT NULL,
  `datemodified` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mdl_feedback_item`
--

CREATE TABLE `mdl_feedback_item` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `feedback` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `template` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `presentation` text NOT NULL,
  `typ` varchar(255) NOT NULL DEFAULT '',
  `hasvalue` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `position` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `required` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `category` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='feedback_items';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_feedback_sitecourse_map`
--

CREATE TABLE `mdl_feedback_sitecourse_map` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `feedbackid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `courseid` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='feedback sitecourse map';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_feedback_template`
--

CREATE TABLE `mdl_feedback_template` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `ispublic` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='templates of feedbackstructures';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_feedback_tracking`
--

CREATE TABLE `mdl_feedback_tracking` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `feedback` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `completed` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `tmp_completed` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='feedback trackingdata';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_feedback_user`
--

CREATE TABLE `mdl_feedback_user` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `userid` bigint(10) UNSIGNED NOT NULL,
  `course_id` bigint(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `mdl_feedback_value`
--

CREATE TABLE `mdl_feedback_value` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `item` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `completed` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `tmp_completed` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `value` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='values of the completeds';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_feedback_valuetmp`
--

CREATE TABLE `mdl_feedback_valuetmp` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `item` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `completed` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `tmp_completed` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='values of the completedstmp';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_forum`
--

CREATE TABLE `mdl_forum` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `type` enum('single','news','general','social','eachuser','teacher','qanda') NOT NULL DEFAULT 'general',
  `name` varchar(255) NOT NULL DEFAULT '',
  `intro` text NOT NULL,
  `assessed` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `assesstimestart` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `assesstimefinish` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `scale` bigint(10) NOT NULL DEFAULT '0',
  `maxbytes` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `forcesubscribe` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `trackingtype` tinyint(2) UNSIGNED NOT NULL DEFAULT '1',
  `rsstype` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `rssarticles` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `warnafter` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `blockafter` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `blockperiod` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Forums contain and structure discussion';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_forum_discussions`
--

CREATE TABLE `mdl_forum_discussions` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `forum` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `firstpost` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `groupid` bigint(10) NOT NULL DEFAULT '-1',
  `assessed` tinyint(1) NOT NULL DEFAULT '1',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `usermodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timestart` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timeend` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Forums are composed of discussions';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_forum_posts`
--

CREATE TABLE `mdl_forum_posts` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `discussion` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `parent` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `created` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `modified` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `mailed` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `subject` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  `format` tinyint(2) NOT NULL DEFAULT '0',
  `attachment` varchar(100) NOT NULL DEFAULT '',
  `totalscore` smallint(4) NOT NULL DEFAULT '0',
  `mailnow` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='All posts are stored in this table';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_forum_queue`
--

CREATE TABLE `mdl_forum_queue` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `discussionid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `postid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='For keeping track of posts that will be mailed in digest for';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_forum_ratings`
--

CREATE TABLE `mdl_forum_ratings` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `post` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `time` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `rating` smallint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='forum_ratings table retrofitted from MySQL';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_forum_read`
--

CREATE TABLE `mdl_forum_read` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `forumid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `discussionid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `postid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `firstread` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `lastread` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tracks each users read posts';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_forum_subscriptions`
--

CREATE TABLE `mdl_forum_subscriptions` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `forum` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Keeps track of who is subscribed to what forum';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_forum_track_prefs`
--

CREATE TABLE `mdl_forum_track_prefs` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `forumid` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tracks each users untracked forums';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_game`
--

CREATE TABLE `mdl_game` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `course` bigint(10) UNSIGNED DEFAULT NULL,
  `sourcemodule` varchar(50) DEFAULT NULL,
  `timeopen` bigint(10) UNSIGNED DEFAULT '0',
  `timeclose` bigint(10) UNSIGNED DEFAULT '0',
  `quizid` bigint(10) UNSIGNED DEFAULT NULL,
  `glossaryid` bigint(10) UNSIGNED DEFAULT NULL,
  `glossarycategoryid` bigint(10) UNSIGNED DEFAULT NULL,
  `questioncategoryid` bigint(10) UNSIGNED DEFAULT NULL,
  `bookid` bigint(10) UNSIGNED DEFAULT NULL,
  `gamekind` varchar(20) DEFAULT NULL,
  `param1` bigint(10) UNSIGNED DEFAULT NULL,
  `param2` bigint(10) UNSIGNED DEFAULT NULL,
  `param3` bigint(10) UNSIGNED DEFAULT NULL,
  `param4` bigint(10) UNSIGNED DEFAULT NULL,
  `param5` bigint(10) UNSIGNED DEFAULT NULL,
  `param6` bigint(10) UNSIGNED DEFAULT NULL,
  `param7` bigint(10) UNSIGNED DEFAULT NULL,
  `param8` bigint(10) UNSIGNED DEFAULT NULL,
  `param9` mediumtext,
  `param10` bigint(10) UNSIGNED DEFAULT NULL,
  `shuffle` tinyint(2) UNSIGNED DEFAULT '1',
  `timemodified` bigint(10) UNSIGNED DEFAULT NULL,
  `gameinputid` bigint(10) UNSIGNED DEFAULT NULL,
  `toptext` text,
  `bottomtext` text,
  `grademethod` tinyint(2) UNSIGNED DEFAULT NULL,
  `grade` bigint(10) UNSIGNED DEFAULT NULL,
  `decimalpoints` tinyint(2) UNSIGNED DEFAULT NULL,
  `review` bigint(10) UNSIGNED DEFAULT NULL,
  `attempts` bigint(10) UNSIGNED DEFAULT NULL,
  `glossaryid2` bigint(10) UNSIGNED DEFAULT NULL,
  `glossarycategoryid2` bigint(10) UNSIGNED DEFAULT NULL,
  `language` varchar(10) DEFAULT NULL,
  `subcategories` tinyint(1) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='game';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_game_attempts`
--

CREATE TABLE `mdl_game_attempts` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `gameid` bigint(10) UNSIGNED DEFAULT NULL,
  `userid` bigint(10) UNSIGNED DEFAULT NULL,
  `timestart` bigint(10) UNSIGNED NOT NULL,
  `timefinish` bigint(10) UNSIGNED NOT NULL,
  `timelastattempt` bigint(10) UNSIGNED DEFAULT NULL,
  `lastip` varchar(30) DEFAULT NULL,
  `lastremotehost` varchar(50) DEFAULT NULL,
  `preview` tinyint(1) UNSIGNED DEFAULT NULL,
  `attempt` bigint(10) UNSIGNED DEFAULT NULL,
  `score` double UNSIGNED DEFAULT NULL,
  `attempts` bigint(10) UNSIGNED DEFAULT NULL,
  `language` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='game_attempts';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_game_badge`
--

CREATE TABLE `mdl_game_badge` (
  `id` int(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `file` varchar(100) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `point` bigint(7) UNSIGNED NOT NULL,
  `pointcomment` varchar(255) NOT NULL,
  `issuer` bigint(10) UNSIGNED NOT NULL,
  `timemodified` bigint(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Badges used for gamification';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_game_badge_track`
--

CREATE TABLE `mdl_game_badge_track` (
  `id` int(10) UNSIGNED NOT NULL,
  `userid` bigint(10) UNSIGNED NOT NULL,
  `badgeid` bigint(10) UNSIGNED NOT NULL,
  `issuer` bigint(10) UNSIGNED NOT NULL,
  `comment` varchar(255) NOT NULL,
  `timeissued` bigint(10) UNSIGNED NOT NULL,
  `timemodified` bigint(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mdl_game_bookquiz`
--

CREATE TABLE `mdl_game_bookquiz` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `bookid` bigint(10) UNSIGNED NOT NULL,
  `lastchapterid` varchar(81) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='game_bookquiz';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_game_bookquiz_chapters`
--

CREATE TABLE `mdl_game_bookquiz_chapters` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `attemptid` bigint(10) UNSIGNED NOT NULL,
  `chapterid` varchar(81) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='game_bookquiz_chapters';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_game_bookquiz_questions`
--

CREATE TABLE `mdl_game_bookquiz_questions` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `gameid` bigint(10) UNSIGNED DEFAULT NULL,
  `chapterid` bigint(10) UNSIGNED DEFAULT NULL,
  `questioncategoryid` bigint(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='game_bookquiz';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_game_course`
--

CREATE TABLE `mdl_game_course` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `messagewin` text,
  `messageloose` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='game_course';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_game_course_inputs`
--

CREATE TABLE `mdl_game_course_inputs` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `course` bigint(10) UNSIGNED DEFAULT NULL,
  `sourcemodule` varchar(20) DEFAULT NULL,
  `ids` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='game_course_inputs';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_game_cross`
--

CREATE TABLE `mdl_game_cross` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `cols` varchar(50) DEFAULT NULL,
  `rows` smallint(3) UNSIGNED DEFAULT NULL,
  `words` smallint(3) UNSIGNED DEFAULT NULL,
  `wordsall` bigint(10) UNSIGNED DEFAULT NULL,
  `createscore` smallint(3) UNSIGNED DEFAULT NULL,
  `createtries` bigint(10) UNSIGNED DEFAULT NULL,
  `createtimelimit` bigint(10) UNSIGNED DEFAULT NULL,
  `createconnectors` bigint(10) UNSIGNED DEFAULT NULL,
  `createfilleds` bigint(10) UNSIGNED DEFAULT NULL,
  `createspaces` bigint(10) UNSIGNED DEFAULT NULL,
  `triesplay` bigint(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='game_cross';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_game_cryptex`
--

CREATE TABLE `mdl_game_cryptex` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `letters` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='game_cryptex';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_game_export_html`
--

CREATE TABLE `mdl_game_export_html` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `filename` varchar(30) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `checkbutton` tinyint(2) UNSIGNED DEFAULT NULL,
  `printbutton` tinyint(2) UNSIGNED DEFAULT NULL,
  `inputsize` smallint(3) UNSIGNED DEFAULT NULL,
  `maxpicturewidth` bigint(10) UNSIGNED DEFAULT NULL,
  `maxpictureheight` bigint(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='game_export_html';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_game_export_javame`
--

CREATE TABLE `mdl_game_export_javame` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `filename` varchar(20) DEFAULT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `createdby` varchar(50) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `version` varchar(10) DEFAULT NULL,
  `maxpicturewidth` bigint(10) UNSIGNED DEFAULT NULL,
  `maxpictureheight` bigint(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='game_export_javame';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_game_grades`
--

CREATE TABLE `mdl_game_grades` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `gameid` bigint(10) UNSIGNED DEFAULT NULL,
  `userid` bigint(10) UNSIGNED DEFAULT NULL,
  `score` double UNSIGNED NOT NULL,
  `timemodified` bigint(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='game_grades';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_game_hangman`
--

CREATE TABLE `mdl_game_hangman` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `queryid` bigint(10) UNSIGNED DEFAULT NULL,
  `letters` varchar(100) DEFAULT NULL,
  `allletters` varchar(100) DEFAULT NULL,
  `try` smallint(4) UNSIGNED DEFAULT NULL,
  `maxtries` smallint(4) UNSIGNED DEFAULT NULL,
  `finishedword` smallint(4) UNSIGNED DEFAULT NULL,
  `corrects` smallint(4) UNSIGNED DEFAULT NULL,
  `iscorrect` tinyint(2) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='game_hangman';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_game_hiddenpicture`
--

CREATE TABLE `mdl_game_hiddenpicture` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `correct` smallint(4) UNSIGNED DEFAULT '0',
  `wrong` smallint(4) UNSIGNED DEFAULT '0',
  `found` smallint(4) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='game_hiddenpicture';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_game_millionaire`
--

CREATE TABLE `mdl_game_millionaire` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `queryid` bigint(10) UNSIGNED DEFAULT NULL,
  `state` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `level` tinyint(2) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='game_millionaire';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_game_point`
--

CREATE TABLE `mdl_game_point` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL,
  `module` bigint(10) UNSIGNED NOT NULL,
  `type` varchar(255) NOT NULL,
  `userid` bigint(10) UNSIGNED NOT NULL,
  `point` bigint(7) UNSIGNED NOT NULL DEFAULT '0',
  `issuer` bigint(10) UNSIGNED DEFAULT NULL,
  `comments` text,
  `timeissued` bigint(10) UNSIGNED NOT NULL,
  `timemodified` bigint(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table to capture user earned game points';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_game_queries`
--

CREATE TABLE `mdl_game_queries` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `attemptid` bigint(10) UNSIGNED DEFAULT NULL,
  `gameid` bigint(10) UNSIGNED DEFAULT NULL,
  `userid` bigint(10) UNSIGNED DEFAULT NULL,
  `sourcemodule` varchar(20) DEFAULT NULL,
  `questionid` bigint(10) UNSIGNED DEFAULT NULL,
  `glossaryentryid` bigint(10) UNSIGNED DEFAULT NULL,
  `questiontext` text,
  `score` double UNSIGNED DEFAULT NULL,
  `timelastattempt` bigint(10) UNSIGNED DEFAULT NULL,
  `studentanswer` text,
  `col` bigint(10) UNSIGNED DEFAULT NULL,
  `row` bigint(10) UNSIGNED DEFAULT NULL,
  `horizontal` bigint(10) UNSIGNED DEFAULT NULL,
  `answertext` text,
  `correct` bigint(10) UNSIGNED DEFAULT NULL,
  `attachment` varchar(200) DEFAULT NULL,
  `answerid` bigint(10) UNSIGNED DEFAULT NULL,
  `tries` bigint(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='game_queries';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_game_repetitions`
--

CREATE TABLE `mdl_game_repetitions` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `gameid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `questionid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `glossaryentryid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `repetitions` bigint(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='game_repetitions';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_game_snakes`
--

CREATE TABLE `mdl_game_snakes` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `snakesdatabaseid` bigint(10) UNSIGNED DEFAULT NULL,
  `position` bigint(10) UNSIGNED DEFAULT NULL,
  `queryid` bigint(10) UNSIGNED DEFAULT NULL,
  `dice` tinyint(1) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='game_snakes';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_game_snakes_database`
--

CREATE TABLE `mdl_game_snakes_database` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `cols` smallint(3) UNSIGNED DEFAULT NULL,
  `rows` smallint(3) UNSIGNED DEFAULT NULL,
  `data` text,
  `fileboard` varchar(100) DEFAULT NULL,
  `direction` tinyint(2) UNSIGNED DEFAULT NULL,
  `headerx` bigint(10) UNSIGNED DEFAULT NULL,
  `headery` bigint(10) UNSIGNED DEFAULT NULL,
  `footerx` bigint(10) UNSIGNED DEFAULT NULL,
  `footery` bigint(10) UNSIGNED DEFAULT NULL,
  `width` bigint(10) UNSIGNED DEFAULT NULL,
  `height` bigint(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='game_snakes_database';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_game_sudoku`
--

CREATE TABLE `mdl_game_sudoku` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `level` smallint(4) UNSIGNED DEFAULT '0',
  `data` varchar(81) NOT NULL DEFAULT '',
  `opened` smallint(4) UNSIGNED DEFAULT NULL,
  `guess` varchar(81) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='game_sudoku';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_game_sudoku_database`
--

CREATE TABLE `mdl_game_sudoku_database` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `level` smallint(3) UNSIGNED DEFAULT NULL,
  `opened` tinyint(2) UNSIGNED DEFAULT NULL,
  `data` varchar(81) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='game_sudoku_database';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_glossary`
--

CREATE TABLE `mdl_glossary` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `intro` text NOT NULL,
  `allowduplicatedentries` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `displayformat` varchar(50) NOT NULL DEFAULT 'dictionary',
  `mainglossary` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `showspecial` tinyint(2) UNSIGNED NOT NULL DEFAULT '1',
  `showalphabet` tinyint(2) UNSIGNED NOT NULL DEFAULT '1',
  `showall` tinyint(2) UNSIGNED NOT NULL DEFAULT '1',
  `allowcomments` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `allowprintview` tinyint(2) UNSIGNED NOT NULL DEFAULT '1',
  `usedynalink` tinyint(2) UNSIGNED NOT NULL DEFAULT '1',
  `defaultapproval` tinyint(2) UNSIGNED NOT NULL DEFAULT '1',
  `globalglossary` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `entbypage` smallint(3) UNSIGNED NOT NULL DEFAULT '10',
  `editalways` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `rsstype` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `rssarticles` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `assessed` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `assesstimestart` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `assesstimefinish` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `scale` bigint(10) NOT NULL DEFAULT '0',
  `timecreated` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='all glossaries';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_glossary_alias`
--

CREATE TABLE `mdl_glossary_alias` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `entryid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `alias` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='entries alias';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_glossary_categories`
--

CREATE TABLE `mdl_glossary_categories` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `glossaryid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `usedynalink` tinyint(2) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='all categories for glossary entries';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_glossary_comments`
--

CREATE TABLE `mdl_glossary_comments` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `entryid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `entrycomment` text NOT NULL,
  `format` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='comments on glossary entries';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_glossary_entries`
--

CREATE TABLE `mdl_glossary_entries` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `glossaryid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `concept` varchar(255) NOT NULL DEFAULT '',
  `definition` text NOT NULL,
  `format` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `attachment` varchar(100) NOT NULL DEFAULT '',
  `timecreated` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `teacherentry` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `sourceglossaryid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `usedynalink` tinyint(2) UNSIGNED NOT NULL DEFAULT '1',
  `casesensitive` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `fullmatch` tinyint(2) UNSIGNED NOT NULL DEFAULT '1',
  `approved` tinyint(2) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='all glossary entries';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_glossary_entries_categories`
--

CREATE TABLE `mdl_glossary_entries_categories` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `categoryid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `entryid` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='categories of each glossary entry';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_glossary_formats`
--

CREATE TABLE `mdl_glossary_formats` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `popupformatname` varchar(50) NOT NULL DEFAULT '',
  `visible` tinyint(2) UNSIGNED NOT NULL DEFAULT '1',
  `showgroup` tinyint(2) UNSIGNED NOT NULL DEFAULT '1',
  `defaultmode` varchar(50) NOT NULL DEFAULT '',
  `defaulthook` varchar(50) NOT NULL DEFAULT '',
  `sortkey` varchar(50) NOT NULL DEFAULT '',
  `sortorder` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Setting of the display formats';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_glossary_ratings`
--

CREATE TABLE `mdl_glossary_ratings` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `entryid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `time` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `rating` smallint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Contains user ratings for entries';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_grade_categories`
--

CREATE TABLE `mdl_grade_categories` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `courseid` bigint(10) UNSIGNED NOT NULL,
  `parent` bigint(10) UNSIGNED DEFAULT NULL,
  `depth` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `path` varchar(255) DEFAULT NULL,
  `fullname` varchar(255) NOT NULL DEFAULT '',
  `aggregation` bigint(10) NOT NULL DEFAULT '0',
  `keephigh` bigint(10) NOT NULL DEFAULT '0',
  `droplow` bigint(10) NOT NULL DEFAULT '0',
  `aggregateonlygraded` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `aggregateoutcomes` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `aggregatesubcats` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `timecreated` bigint(10) UNSIGNED NOT NULL,
  `timemodified` bigint(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This table keeps information about categories, used for grou';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_grade_categories_history`
--

CREATE TABLE `mdl_grade_categories_history` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `action` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `oldid` bigint(10) UNSIGNED NOT NULL,
  `source` varchar(255) DEFAULT NULL,
  `timemodified` bigint(10) UNSIGNED DEFAULT NULL,
  `loggeduser` bigint(10) UNSIGNED DEFAULT NULL,
  `courseid` bigint(10) UNSIGNED NOT NULL,
  `parent` bigint(10) UNSIGNED DEFAULT NULL,
  `depth` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `path` varchar(255) DEFAULT NULL,
  `fullname` varchar(255) NOT NULL DEFAULT '',
  `aggregation` bigint(10) NOT NULL DEFAULT '0',
  `keephigh` bigint(10) NOT NULL DEFAULT '0',
  `droplow` bigint(10) NOT NULL DEFAULT '0',
  `aggregateonlygraded` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `aggregateoutcomes` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `aggregatesubcats` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='History of grade_categories';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_grade_grades`
--

CREATE TABLE `mdl_grade_grades` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `itemid` bigint(10) UNSIGNED NOT NULL,
  `userid` bigint(10) UNSIGNED NOT NULL,
  `rawgrade` decimal(10,5) DEFAULT NULL,
  `rawgrademax` decimal(10,5) NOT NULL DEFAULT '100.00000',
  `rawgrademin` decimal(10,5) NOT NULL DEFAULT '0.00000',
  `rawscaleid` bigint(10) UNSIGNED DEFAULT NULL,
  `usermodified` bigint(10) UNSIGNED DEFAULT NULL,
  `finalgrade` decimal(10,5) DEFAULT NULL,
  `hidden` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `locked` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `locktime` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `exported` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `overridden` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `excluded` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `feedback` mediumtext,
  `feedbackformat` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `information` mediumtext,
  `informationformat` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timecreated` bigint(10) UNSIGNED DEFAULT NULL,
  `timemodified` bigint(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='grade_grades  This table keeps individual grades for each us';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_grade_grades_history`
--

CREATE TABLE `mdl_grade_grades_history` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `action` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `oldid` bigint(10) UNSIGNED NOT NULL,
  `source` varchar(255) DEFAULT NULL,
  `timemodified` bigint(10) UNSIGNED DEFAULT NULL,
  `loggeduser` bigint(10) UNSIGNED DEFAULT NULL,
  `itemid` bigint(10) UNSIGNED NOT NULL,
  `userid` bigint(10) UNSIGNED NOT NULL,
  `rawgrade` decimal(10,5) DEFAULT NULL,
  `rawgrademax` decimal(10,5) NOT NULL DEFAULT '100.00000',
  `rawgrademin` decimal(10,5) NOT NULL DEFAULT '0.00000',
  `rawscaleid` bigint(10) UNSIGNED DEFAULT NULL,
  `usermodified` bigint(10) UNSIGNED DEFAULT NULL,
  `finalgrade` decimal(10,5) DEFAULT NULL,
  `hidden` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `locked` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `locktime` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `exported` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `overridden` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `excluded` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `feedback` mediumtext,
  `feedbackformat` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `information` mediumtext,
  `informationformat` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='History table';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_grade_import_newitem`
--

CREATE TABLE `mdl_grade_import_newitem` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `itemname` varchar(255) NOT NULL DEFAULT '',
  `importcode` bigint(10) UNSIGNED NOT NULL,
  `importer` bigint(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='temporary table for storing new grade_item names from grade ';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_grade_import_values`
--

CREATE TABLE `mdl_grade_import_values` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `itemid` bigint(10) UNSIGNED DEFAULT NULL,
  `newgradeitem` bigint(10) UNSIGNED DEFAULT NULL,
  `userid` bigint(10) UNSIGNED NOT NULL,
  `finalgrade` decimal(10,5) DEFAULT NULL,
  `feedback` mediumtext,
  `importcode` bigint(10) UNSIGNED NOT NULL,
  `importer` bigint(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Temporary table for importing grades';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_grade_items`
--

CREATE TABLE `mdl_grade_items` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `courseid` bigint(10) UNSIGNED DEFAULT NULL,
  `categoryid` bigint(10) UNSIGNED DEFAULT NULL,
  `itemname` varchar(255) DEFAULT NULL,
  `itemtype` varchar(30) NOT NULL DEFAULT '',
  `itemmodule` varchar(30) DEFAULT NULL,
  `iteminstance` bigint(10) UNSIGNED DEFAULT NULL,
  `itemnumber` bigint(10) UNSIGNED DEFAULT NULL,
  `iteminfo` mediumtext,
  `idnumber` varchar(255) DEFAULT NULL,
  `calculation` mediumtext,
  `gradetype` smallint(4) NOT NULL DEFAULT '1',
  `grademax` decimal(10,5) NOT NULL DEFAULT '100.00000',
  `grademin` decimal(10,5) NOT NULL DEFAULT '0.00000',
  `scaleid` bigint(10) UNSIGNED DEFAULT NULL,
  `outcomeid` bigint(10) UNSIGNED DEFAULT NULL,
  `gradepass` decimal(10,5) NOT NULL DEFAULT '0.00000',
  `multfactor` decimal(10,5) NOT NULL DEFAULT '1.00000',
  `plusfactor` decimal(10,5) NOT NULL DEFAULT '0.00000',
  `aggregationcoef` decimal(10,5) NOT NULL DEFAULT '0.00000',
  `sortorder` bigint(10) NOT NULL DEFAULT '0',
  `display` bigint(10) NOT NULL DEFAULT '0',
  `decimals` tinyint(1) UNSIGNED DEFAULT NULL,
  `hidden` bigint(10) NOT NULL DEFAULT '0',
  `locked` bigint(10) NOT NULL DEFAULT '0',
  `locktime` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `needsupdate` bigint(10) NOT NULL DEFAULT '0',
  `timecreated` bigint(10) UNSIGNED DEFAULT NULL,
  `timemodified` bigint(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This table keeps information about gradeable items (ie colum';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_grade_items_history`
--

CREATE TABLE `mdl_grade_items_history` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `action` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `oldid` bigint(10) UNSIGNED NOT NULL,
  `source` varchar(255) DEFAULT NULL,
  `timemodified` bigint(10) UNSIGNED DEFAULT NULL,
  `loggeduser` bigint(10) UNSIGNED DEFAULT NULL,
  `courseid` bigint(10) UNSIGNED DEFAULT NULL,
  `categoryid` bigint(10) UNSIGNED DEFAULT NULL,
  `itemname` varchar(255) DEFAULT NULL,
  `itemtype` varchar(30) NOT NULL DEFAULT '',
  `itemmodule` varchar(30) DEFAULT NULL,
  `iteminstance` bigint(10) UNSIGNED DEFAULT NULL,
  `itemnumber` bigint(10) UNSIGNED DEFAULT NULL,
  `iteminfo` mediumtext,
  `idnumber` varchar(255) DEFAULT NULL,
  `calculation` mediumtext,
  `gradetype` smallint(4) NOT NULL DEFAULT '1',
  `grademax` decimal(10,5) NOT NULL DEFAULT '100.00000',
  `grademin` decimal(10,5) NOT NULL DEFAULT '0.00000',
  `scaleid` bigint(10) UNSIGNED DEFAULT NULL,
  `outcomeid` bigint(10) UNSIGNED DEFAULT NULL,
  `gradepass` decimal(10,5) NOT NULL DEFAULT '0.00000',
  `multfactor` decimal(10,5) NOT NULL DEFAULT '1.00000',
  `plusfactor` decimal(10,5) NOT NULL DEFAULT '0.00000',
  `aggregationcoef` decimal(10,5) NOT NULL DEFAULT '0.00000',
  `sortorder` bigint(10) NOT NULL DEFAULT '0',
  `hidden` bigint(10) NOT NULL DEFAULT '0',
  `locked` bigint(10) NOT NULL DEFAULT '0',
  `locktime` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `needsupdate` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='History of grade_items';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_grade_letters`
--

CREATE TABLE `mdl_grade_letters` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `contextid` bigint(10) UNSIGNED NOT NULL,
  `lowerboundary` decimal(10,5) NOT NULL,
  `letter` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Repository for grade letters, for courses and other moodle e';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_grade_outcomes`
--

CREATE TABLE `mdl_grade_outcomes` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `courseid` bigint(10) UNSIGNED DEFAULT NULL,
  `shortname` varchar(255) NOT NULL DEFAULT '',
  `fullname` text NOT NULL,
  `scaleid` bigint(10) UNSIGNED DEFAULT NULL,
  `description` text,
  `timecreated` bigint(10) UNSIGNED DEFAULT NULL,
  `timemodified` bigint(10) UNSIGNED DEFAULT NULL,
  `usermodified` bigint(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This table describes the outcomes used in the system. An out';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_grade_outcomes_courses`
--

CREATE TABLE `mdl_grade_outcomes_courses` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `courseid` bigint(10) UNSIGNED NOT NULL,
  `outcomeid` bigint(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='stores what outcomes are used in what courses.';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_grade_outcomes_history`
--

CREATE TABLE `mdl_grade_outcomes_history` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `action` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `oldid` bigint(10) UNSIGNED NOT NULL,
  `source` varchar(255) DEFAULT NULL,
  `timemodified` bigint(10) UNSIGNED DEFAULT NULL,
  `loggeduser` bigint(10) UNSIGNED DEFAULT NULL,
  `courseid` bigint(10) UNSIGNED DEFAULT NULL,
  `shortname` varchar(255) NOT NULL DEFAULT '',
  `fullname` text NOT NULL,
  `scaleid` bigint(10) UNSIGNED DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='History table';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_grade_settings`
--

CREATE TABLE `mdl_grade_settings` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `courseid` bigint(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='gradebook settings';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_grade_training`
--

CREATE TABLE `mdl_grade_training` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `company` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grade` varchar(50) CHARACTER SET utf8 DEFAULT '0',
  `hours` varchar(50) CHARACTER SET utf8 DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mdl_groupings`
--

CREATE TABLE `mdl_groupings` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `courseid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `configdata` text,
  `timecreated` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='A grouping is a collection of groups. WAS: groups_groupings';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_groupings_groups`
--

CREATE TABLE `mdl_groupings_groups` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `groupingid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `groupid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timeadded` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Link a grouping to a group (note, groups can be in multiple ';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_groups`
--

CREATE TABLE `mdl_groups` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `courseid` bigint(10) UNSIGNED NOT NULL,
  `name` varchar(254) NOT NULL DEFAULT '',
  `description` text,
  `enrolmentkey` varchar(50) DEFAULT NULL,
  `picture` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `hidepicture` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `setasdefault` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `timecreated` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Each record represents a group.';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_groups_members`
--

CREATE TABLE `mdl_groups_members` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `groupid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timeadded` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Link a user to a group.';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_journal`
--

CREATE TABLE `mdl_journal` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `intro` text NOT NULL,
  `introformat` tinyint(2) NOT NULL DEFAULT '0',
  `days` mediumint(5) UNSIGNED NOT NULL DEFAULT '7',
  `assessed` bigint(10) NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='data for each journal';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_journal_entries`
--

CREATE TABLE `mdl_journal_entries` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `journal` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `modified` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `text` text NOT NULL,
  `format` tinyint(2) NOT NULL DEFAULT '0',
  `rating` bigint(10) DEFAULT '0',
  `entrycomment` text,
  `teacher` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timemarked` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `mailed` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='All the journal entries of all people';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_klci_grade`
--

CREATE TABLE `mdl_klci_grade` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `grade` varchar(4) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='central grade table';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_klci_level`
--

CREATE TABLE `mdl_klci_level` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `level` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='central level table';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_klci_practice`
--

CREATE TABLE `mdl_klci_practice` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='master practice';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_klci_stream`
--

CREATE TABLE `mdl_klci_stream` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `practiceid` bigint(10) DEFAULT NULL,
  `name` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='central stream table';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_label`
--

CREATE TABLE `mdl_label` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Defines labels';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_lams`
--

CREATE TABLE `mdl_lams` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `introduction` text NOT NULL,
  `learning_session_id` bigint(20) DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='LAMS activity';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_lesson`
--

CREATE TABLE `mdl_lesson` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `practice` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `modattempts` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `usepassword` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `password` varchar(32) NOT NULL DEFAULT '',
  `dependency` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `conditions` text NOT NULL,
  `grade` smallint(3) NOT NULL DEFAULT '0',
  `custom` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `ongoing` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `usemaxgrade` smallint(3) NOT NULL DEFAULT '0',
  `maxanswers` smallint(3) UNSIGNED NOT NULL DEFAULT '4',
  `maxattempts` smallint(3) UNSIGNED NOT NULL DEFAULT '5',
  `review` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `nextpagedefault` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `feedback` smallint(3) UNSIGNED NOT NULL DEFAULT '1',
  `minquestions` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `maxpages` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `timed` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `maxtime` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `retake` smallint(3) UNSIGNED NOT NULL DEFAULT '1',
  `activitylink` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `mediafile` varchar(255) NOT NULL DEFAULT '',
  `mediaheight` bigint(10) UNSIGNED NOT NULL DEFAULT '100',
  `mediawidth` bigint(10) UNSIGNED NOT NULL DEFAULT '650',
  `mediaclose` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `slideshow` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `width` bigint(10) UNSIGNED NOT NULL DEFAULT '640',
  `height` bigint(10) UNSIGNED NOT NULL DEFAULT '480',
  `bgcolor` varchar(7) NOT NULL DEFAULT '#FFFFFF',
  `displayleft` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `displayleftif` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `progressbar` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `highscores` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `maxhighscores` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `available` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `deadline` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Defines lesson';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_lesson_answers`
--

CREATE TABLE `mdl_lesson_answers` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `lessonid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `pageid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `jumpto` bigint(11) NOT NULL DEFAULT '0',
  `grade` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `score` bigint(10) NOT NULL DEFAULT '0',
  `flags` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `timecreated` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `answer` text,
  `response` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Defines lesson_answers';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_lesson_attempts`
--

CREATE TABLE `mdl_lesson_attempts` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `lessonid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `pageid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `answerid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `retry` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `correct` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `useranswer` text,
  `timeseen` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Defines lesson_attempts';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_lesson_branch`
--

CREATE TABLE `mdl_lesson_branch` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `lessonid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `pageid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `retry` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `flag` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `timeseen` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='branches for each lesson/user';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_lesson_default`
--

CREATE TABLE `mdl_lesson_default` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `practice` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `modattempts` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `usepassword` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `password` varchar(32) NOT NULL DEFAULT '',
  `conditions` text NOT NULL,
  `grade` smallint(3) NOT NULL DEFAULT '0',
  `custom` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `ongoing` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `usemaxgrade` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `maxanswers` smallint(3) UNSIGNED NOT NULL DEFAULT '4',
  `maxattempts` smallint(3) UNSIGNED NOT NULL DEFAULT '5',
  `review` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `nextpagedefault` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `feedback` smallint(3) UNSIGNED NOT NULL DEFAULT '1',
  `minquestions` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `maxpages` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `timed` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `maxtime` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `retake` smallint(3) UNSIGNED NOT NULL DEFAULT '1',
  `mediaheight` bigint(10) UNSIGNED NOT NULL DEFAULT '100',
  `mediawidth` bigint(10) UNSIGNED NOT NULL DEFAULT '650',
  `mediaclose` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `slideshow` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `width` bigint(10) UNSIGNED NOT NULL DEFAULT '640',
  `height` bigint(10) UNSIGNED NOT NULL DEFAULT '480',
  `bgcolor` varchar(7) DEFAULT '#FFFFFF',
  `displayleft` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `displayleftif` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `progressbar` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `highscores` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `maxhighscores` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Defines lesson_default';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_lesson_grades`
--

CREATE TABLE `mdl_lesson_grades` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `lessonid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `grade` double UNSIGNED NOT NULL DEFAULT '0',
  `late` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `completed` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Defines lesson_grades';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_lesson_high_scores`
--

CREATE TABLE `mdl_lesson_high_scores` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `lessonid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `gradeid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `nickname` varchar(5) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='high scores for each lesson';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_lesson_pages`
--

CREATE TABLE `mdl_lesson_pages` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `lessonid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `prevpageid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `nextpageid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `qtype` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `qoption` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `layout` smallint(3) UNSIGNED NOT NULL DEFAULT '1',
  `display` smallint(3) UNSIGNED NOT NULL DEFAULT '1',
  `timecreated` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `contents` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Defines lesson_pages';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_lesson_timer`
--

CREATE TABLE `mdl_lesson_timer` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `lessonid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `starttime` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `lessontime` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='lesson timer for each lesson';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_log`
--

CREATE TABLE `mdl_log` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `time` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `ip` varchar(15) NOT NULL DEFAULT '',
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `module` varchar(20) NOT NULL DEFAULT '',
  `cmid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `action` varchar(40) NOT NULL DEFAULT '',
  `url` varchar(100) NOT NULL DEFAULT '',
  `info` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Every action is logged as far as possible';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_log2`
--

CREATE TABLE `mdl_log2` (
  `id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `time` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `ip` varchar(15) NOT NULL DEFAULT '',
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `module` varchar(20) NOT NULL DEFAULT '',
  `cmid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `action` varchar(40) NOT NULL DEFAULT '',
  `url` varchar(100) NOT NULL DEFAULT '',
  `info` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mdl_log_display`
--

CREATE TABLE `mdl_log_display` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `module` varchar(20) NOT NULL DEFAULT '',
  `action` varchar(40) NOT NULL DEFAULT '',
  `mtable` varchar(30) NOT NULL DEFAULT '',
  `field` varchar(200) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='For a particular module/action, specifies a moodle table/fie';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_message`
--

CREATE TABLE `mdl_message` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `useridfrom` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `useridto` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `message` text NOT NULL,
  `format` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `timecreated` bigint(10) NOT NULL DEFAULT '0',
  `messagetype` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores all unread messages';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_message_contacts`
--

CREATE TABLE `mdl_message_contacts` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `contactid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `blocked` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Maintains lists of relationships between users';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_message_read`
--

CREATE TABLE `mdl_message_read` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `useridfrom` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `useridto` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `message` text NOT NULL,
  `format` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `timecreated` bigint(10) NOT NULL DEFAULT '0',
  `timeread` bigint(10) NOT NULL DEFAULT '0',
  `messagetype` varchar(50) NOT NULL DEFAULT '',
  `mailed` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores all messages that have been read';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_mindmap`
--

CREATE TABLE `mdl_mindmap` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `intro` mediumtext,
  `introformat` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL,
  `editable` tinyint(1) UNSIGNED NOT NULL,
  `xmldata` longtext NOT NULL,
  `timecreated` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table for Moodle-Mindmaps';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_mnet_application`
--

CREATE TABLE `mdl_mnet_application` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `display_name` varchar(50) NOT NULL DEFAULT '',
  `xmlrpc_server_url` varchar(255) NOT NULL DEFAULT '',
  `sso_land_url` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Information about applications on remote hosts';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_mnet_enrol_assignments`
--

CREATE TABLE `mdl_mnet_enrol_assignments` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `hostid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `courseid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `rolename` varchar(255) NOT NULL DEFAULT '',
  `enroltime` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `enroltype` varchar(20) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Information about enrolments on courses on remote hosts';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_mnet_enrol_course`
--

CREATE TABLE `mdl_mnet_enrol_course` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `hostid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `remoteid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `cat_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `cat_name` varchar(255) NOT NULL DEFAULT '',
  `cat_description` mediumtext NOT NULL,
  `sortorder` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `fullname` varchar(254) NOT NULL DEFAULT '',
  `shortname` varchar(15) NOT NULL DEFAULT '',
  `idnumber` varchar(100) NOT NULL DEFAULT '',
  `summary` mediumtext NOT NULL,
  `startdate` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `cost` varchar(10) NOT NULL DEFAULT '',
  `currency` varchar(3) NOT NULL DEFAULT '',
  `defaultroleid` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `defaultrolename` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Information about courses on remote hosts';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_mnet_host`
--

CREATE TABLE `mdl_mnet_host` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `wwwroot` varchar(255) NOT NULL DEFAULT '',
  `ip_address` varchar(39) NOT NULL DEFAULT '',
  `name` varchar(80) NOT NULL DEFAULT '',
  `public_key` mediumtext NOT NULL,
  `public_key_expires` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `transport` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `portno` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `last_connect_time` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `last_log_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `force_theme` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `theme` varchar(100) DEFAULT NULL,
  `applicationid` bigint(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Information about the local and remote hosts for RPC';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_mnet_host2service`
--

CREATE TABLE `mdl_mnet_host2service` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `hostid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `serviceid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `publish` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `subscribe` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Information about the services for a given host';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_mnet_log`
--

CREATE TABLE `mdl_mnet_log` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `hostid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `remoteid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `time` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `ip` varchar(15) NOT NULL DEFAULT '',
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `coursename` varchar(40) NOT NULL DEFAULT '',
  `module` varchar(20) NOT NULL DEFAULT '',
  `cmid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `action` varchar(40) NOT NULL DEFAULT '',
  `url` varchar(100) NOT NULL DEFAULT '',
  `info` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Store session data from users migrating to other sites';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_mnet_rpc`
--

CREATE TABLE `mdl_mnet_rpc` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `function_name` varchar(40) NOT NULL DEFAULT '',
  `xmlrpc_path` varchar(80) NOT NULL DEFAULT '',
  `parent_type` varchar(6) NOT NULL DEFAULT '',
  `parent` varchar(20) NOT NULL DEFAULT '',
  `enabled` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `help` mediumtext NOT NULL,
  `profile` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Functions or methods that we may publish or subscribe to';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_mnet_service`
--

CREATE TABLE `mdl_mnet_service` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `name` varchar(40) NOT NULL DEFAULT '',
  `description` varchar(40) NOT NULL DEFAULT '',
  `apiversion` varchar(10) NOT NULL DEFAULT '',
  `offer` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='A service is a group of functions';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_mnet_service2rpc`
--

CREATE TABLE `mdl_mnet_service2rpc` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `serviceid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `rpcid` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Group functions or methods under a service';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_mnet_session`
--

CREATE TABLE `mdl_mnet_session` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `username` varchar(100) NOT NULL DEFAULT '',
  `token` varchar(40) NOT NULL DEFAULT '',
  `mnethostid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `useragent` varchar(40) NOT NULL DEFAULT '',
  `confirm_timeout` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `session_id` varchar(40) NOT NULL DEFAULT '',
  `expires` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Store session data from users migrating to other sites';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_mnet_sso_access_control`
--

CREATE TABLE `mdl_mnet_sso_access_control` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `username` varchar(100) NOT NULL DEFAULT '',
  `mnet_host_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `accessctrl` varchar(20) NOT NULL DEFAULT 'allow'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Users by host permitted (or not) to login from a remote prov';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_modules`
--

CREATE TABLE `mdl_modules` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL DEFAULT '',
  `version` bigint(10) NOT NULL DEFAULT '0',
  `cron` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `lastcron` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `search` varchar(255) NOT NULL DEFAULT '',
  `visible` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='modules available in the site';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_mplayer`
--

CREATE TABLE `mdl_mplayer` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `intro` mediumtext COLLATE utf8_unicode_ci,
  `introformat` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `timecreated` bigint(11) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(11) UNSIGNED NOT NULL DEFAULT '0',
  `configxml` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mplayerdate` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci,
  `infoboxcolor` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
  `infoboxposition` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `infoboxsize` smallint(4) UNSIGNED DEFAULT NULL,
  `duration` bigint(11) UNSIGNED DEFAULT NULL,
  `point` bigint(7) UNSIGNED NOT NULL DEFAULT '0',
  `mplayerfile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hdbitrate` int(7) UNSIGNED DEFAULT NULL,
  `hdfile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hdfullscreen` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hdstate` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `livestreamfile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `livestreamimage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `livestreaminterval` smallint(4) UNSIGNED DEFAULT NULL,
  `livestreammessage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `livestreamstreamer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `livestreamtags` mediumtext COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `audiodescriptionfile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `audiodescriptionstate` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `audiodescriptionvolume` smallint(3) UNSIGNED DEFAULT NULL,
  `mplayerstart` bigint(11) UNSIGNED DEFAULT '0',
  `tags` mediumtext COLLATE utf8_unicode_ci,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'undefined',
  `backcolor` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
  `frontcolor` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lightcolor` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
  `screencolor` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
  `controlbar` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
  `smoothing` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `height` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `playlist` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `playlistsize` smallint(4) UNSIGNED DEFAULT NULL,
  `skin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `width` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `autostart` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bufferlength` smallint(4) UNSIGNED DEFAULT NULL,
  `fullscreen` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icons` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item` smallint(3) UNSIGNED DEFAULT NULL,
  `logoboxalign` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logoboxfile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logoboxlink` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logoboxmargin` smallint(3) UNSIGNED DEFAULT NULL,
  `logoboxposition` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logofile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logolink` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logohide` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logoposition` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mute` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quality` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mplayerrepeat` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resizing` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shuffle` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stretching` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `volume` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plugins` mediumtext COLLATE utf8_unicode_ci,
  `streamer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tracecall` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `captionsback` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `captionsfile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `captionsfontsize` smallint(3) UNSIGNED DEFAULT NULL,
  `captionsstate` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fpversion` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` mediumtext COLLATE utf8_unicode_ci,
  `metaviewerposition` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metaviewersize` smallint(4) UNSIGNED DEFAULT NULL,
  `searchbarcolor` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
  `searchbarlabel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `searchbarposition` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `searchbarscript` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `snapshotbitmap` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `snapshotscript` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Wrapper for Jeroen Wijering FLV Player';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_navigator_feed`
--

CREATE TABLE `mdl_navigator_feed` (
  `userid` varchar(100) DEFAULT '',
  `navigator_id` varchar(100) DEFAULT '',
  `portalid` varchar(100) NOT NULL DEFAULT '',
  `LC` varchar(100) DEFAULT 'N',
  `firstname` varchar(100) DEFAULT '',
  `middle_name` varchar(100) DEFAULT '',
  `lastname` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `work_city` varchar(100) DEFAULT NULL,
  `grade` varchar(100) DEFAULT NULL,
  `hireddate` varchar(100) DEFAULT NULL,
  `SEPARATION DATE` varchar(100) DEFAULT NULL,
  `status_code` varchar(100) DEFAULT NULL,
  `auth` varchar(100) DEFAULT NULL,
  `emp_work_location` varchar(100) DEFAULT NULL,
  `emp_work_country` varchar(100) DEFAULT NULL,
  `emp_comapany_code` varchar(100) DEFAULT NULL,
  `OU` varchar(100) DEFAULT NULL,
  `BU` varchar(100) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `costcenter` varchar(100) DEFAULT NULL,
  `emp_subgroup` varchar(100) DEFAULT NULL,
  `manager_portalid` varchar(100) DEFAULT NULL,
  `emp_base_country` varchar(100) DEFAULT NULL,
  `acq_company` varchar(100) DEFAULT NULL,
  `practise` varchar(100) DEFAULT NULL,
  `job_level` varchar(100) DEFAULT NULL,
  `emp_work_state` varchar(100) DEFAULT NULL,
  `employee_type` varchar(100) DEFAULT NULL,
  `employee_group` varchar(100) DEFAULT NULL,
  `emp_gender` varchar(100) DEFAULT '',
  `legacy_hr_sys` varchar(100) DEFAULT NULL,
  `tran_company` varchar(100) DEFAULT NULL,
  `cc_manager_portalid` varchar(100) DEFAULT NULL,
  `cc` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `currency` varchar(100) DEFAULT NULL,
  `position_type` varchar(100) DEFAULT NULL,
  `loaddate` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='One record for each person';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_navigator_feed_backup`
--

CREATE TABLE `mdl_navigator_feed_backup` (
  `userid` varchar(100) DEFAULT '',
  `navigator_id` varchar(100) DEFAULT '',
  `portalid` varchar(100) NOT NULL DEFAULT '',
  `LC` varchar(100) DEFAULT 'N',
  `firstname` varchar(100) DEFAULT '',
  `middle_name` varchar(100) DEFAULT '',
  `lastname` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `work_city` varchar(100) DEFAULT NULL,
  `grade` varchar(100) DEFAULT NULL,
  `hireddate` varchar(100) DEFAULT NULL,
  `SEPARATION DATE` varchar(100) DEFAULT NULL,
  `status_code` varchar(100) DEFAULT NULL,
  `auth` varchar(100) DEFAULT NULL,
  `emp_work_location` varchar(100) DEFAULT NULL,
  `emp_work_country` varchar(100) DEFAULT NULL,
  `emp_comapany_code` varchar(100) DEFAULT NULL,
  `OU` varchar(100) DEFAULT NULL,
  `BU` varchar(100) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `costcenter` varchar(100) DEFAULT NULL,
  `emp_subgroup` varchar(100) DEFAULT NULL,
  `manager_portalid` varchar(100) DEFAULT NULL,
  `emp_base_country` varchar(100) DEFAULT NULL,
  `acq_company` varchar(100) DEFAULT NULL,
  `practise` varchar(100) DEFAULT NULL,
  `job_level` varchar(100) DEFAULT NULL,
  `emp_work_state` varchar(100) DEFAULT NULL,
  `employee_type` varchar(100) DEFAULT NULL,
  `employee_group` varchar(100) DEFAULT NULL,
  `emp_gender` varchar(100) DEFAULT '',
  `legacy_hr_sys` varchar(100) DEFAULT NULL,
  `tran_company` varchar(100) DEFAULT NULL,
  `cc_manager_portalid` varchar(100) DEFAULT NULL,
  `cc` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `currency` varchar(100) DEFAULT NULL,
  `position_type` varchar(100) DEFAULT NULL,
  `loaddate` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mdl_openmeetings`
--

CREATE TABLE `mdl_openmeetings` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `teacher` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `type` bigint(10) UNSIGNED NOT NULL DEFAULT '1',
  `is_moderated_room` bigint(10) UNSIGNED NOT NULL DEFAULT '1',
  `max_user` bigint(10) UNSIGNED NOT NULL DEFAULT '4',
  `language` bigint(10) UNSIGNED NOT NULL DEFAULT '1',
  `name` varchar(255) NOT NULL DEFAULT '',
  `intro` mediumtext,
  `introformat` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `timecreated` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `room_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='openmeetings';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_post`
--

CREATE TABLE `mdl_post` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `module` varchar(20) NOT NULL DEFAULT '',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `courseid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `groupid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `moduleid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `coursemoduleid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `subject` varchar(128) NOT NULL DEFAULT '',
  `summary` longtext,
  `content` longtext,
  `uniquehash` varchar(128) NOT NULL DEFAULT '',
  `rating` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `format` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `attachment` varchar(100) DEFAULT NULL,
  `publishstate` enum('draft','site','public') NOT NULL DEFAULT 'draft',
  `lastmodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `created` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `usermodified` bigint(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Generic post table to hold data blog entries etc in differen';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_protrack_upload_temp`
--

CREATE TABLE `mdl_protrack_upload_temp` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `portalid` varchar(100) NOT NULL,
  `userid` varchar(45) NOT NULL,
  `sessionname` varchar(100) NOT NULL,
  `trainingtype` varchar(100) NOT NULL,
  `date` varchar(100) NOT NULL,
  `timestart` varchar(100) NOT NULL,
  `duration` varchar(100) NOT NULL,
  `trainername` varchar(100) NOT NULL,
  `trainerportalid` varchar(100) NOT NULL,
  `comment` varchar(100) DEFAULT NULL,
  `insertkey` bigint(20) UNSIGNED DEFAULT NULL,
  `venue` varchar(100) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='this is a temp table to hold the uploaded file';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_question`
--

CREATE TABLE `mdl_question` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `category` bigint(10) NOT NULL DEFAULT '0',
  `parent` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `questiontext` text NOT NULL,
  `questiontextformat` tinyint(2) NOT NULL DEFAULT '0',
  `image` varchar(255) NOT NULL DEFAULT '',
  `generalfeedback` text NOT NULL,
  `defaultgrade` bigint(10) UNSIGNED NOT NULL DEFAULT '1',
  `penalty` double NOT NULL DEFAULT '0.1',
  `qtype` varchar(20) NOT NULL DEFAULT '',
  `length` bigint(10) UNSIGNED NOT NULL DEFAULT '1',
  `stamp` varchar(255) NOT NULL DEFAULT '',
  `version` varchar(255) NOT NULL DEFAULT '',
  `hidden` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `timecreated` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `createdby` bigint(10) UNSIGNED DEFAULT NULL,
  `modifiedby` bigint(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='The questions themselves';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_questionnaire`
--

CREATE TABLE `mdl_questionnaire` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(11) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `summary` text NOT NULL,
  `qtype` bigint(10) NOT NULL DEFAULT '0',
  `respondenttype` enum('fullname','anonymous') NOT NULL DEFAULT 'fullname',
  `resp_eligible` enum('all','students','teachers') NOT NULL DEFAULT 'all',
  `resp_view` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `opendate` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `closedate` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `resume` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `navigate` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `grade` bigint(10) NOT NULL DEFAULT '0',
  `sid` bigint(11) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Main questionnaire table.';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_questionnaire_attempts`
--

CREATE TABLE `mdl_questionnaire_attempts` (
  `id` bigint(11) UNSIGNED NOT NULL,
  `qid` bigint(11) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(11) UNSIGNED NOT NULL DEFAULT '0',
  `rid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='questionnaire_attempts table retrofitted from MySQL';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_questionnaire_question`
--

CREATE TABLE `mdl_questionnaire_question` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `survey_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(30) DEFAULT NULL,
  `type_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `result_id` bigint(10) UNSIGNED DEFAULT NULL,
  `length` bigint(11) UNSIGNED NOT NULL DEFAULT '0',
  `precise` bigint(11) UNSIGNED NOT NULL DEFAULT '0',
  `position` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  `required` enum('y','n') NOT NULL DEFAULT 'n',
  `deleted` enum('y','n') NOT NULL DEFAULT 'n'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='questionnaire_question table retrofitted from MySQL';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_questionnaire_question_type`
--

CREATE TABLE `mdl_questionnaire_question_type` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `typeid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(32) NOT NULL DEFAULT '',
  `has_choices` enum('y','n') NOT NULL DEFAULT 'y',
  `response_table` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='questionnaire_question_type table retrofitted from MySQL';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_questionnaire_quest_choice`
--

CREATE TABLE `mdl_questionnaire_quest_choice` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `question_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  `value` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='questionnaire_quest_choice table retrofitted from MySQL';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_questionnaire_response`
--

CREATE TABLE `mdl_questionnaire_response` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `survey_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `submitted` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `complete` enum('y','n') NOT NULL DEFAULT 'n',
  `grade` bigint(10) NOT NULL DEFAULT '0',
  `username` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='questionnaire_response table retrofitted from MySQL';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_questionnaire_response_bool`
--

CREATE TABLE `mdl_questionnaire_response_bool` (
  `id` bigint(11) UNSIGNED NOT NULL,
  `response_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `question_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `choice_id` enum('y','n') NOT NULL DEFAULT 'y'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='questionnaire_response_bool table retrofitted from MySQL';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_questionnaire_response_date`
--

CREATE TABLE `mdl_questionnaire_response_date` (
  `id` bigint(11) UNSIGNED NOT NULL,
  `response_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `question_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `response` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='questionnaire_response_date table retrofitted from MySQL';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_questionnaire_response_other`
--

CREATE TABLE `mdl_questionnaire_response_other` (
  `id` bigint(11) UNSIGNED NOT NULL,
  `response_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `question_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `choice_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `response` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='questionnaire_response_other table retrofitted from MySQL';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_questionnaire_response_rank`
--

CREATE TABLE `mdl_questionnaire_response_rank` (
  `id` bigint(11) UNSIGNED NOT NULL,
  `response_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `question_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `choice_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `rank` bigint(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='questionnaire_response_rank table retrofitted from MySQL';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_questionnaire_response_text`
--

CREATE TABLE `mdl_questionnaire_response_text` (
  `id` bigint(11) UNSIGNED NOT NULL,
  `response_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `question_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `response` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='questionnaire_response_text table retrofitted from MySQL';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_questionnaire_resp_multiple`
--

CREATE TABLE `mdl_questionnaire_resp_multiple` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `response_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `question_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `choice_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='questionnaire_resp_multiple table retrofitted from MySQL';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_questionnaire_resp_single`
--

CREATE TABLE `mdl_questionnaire_resp_single` (
  `id` bigint(11) UNSIGNED NOT NULL,
  `response_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `question_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `choice_id` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='questionnaire_resp_single table retrofitted from MySQL';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_questionnaire_survey`
--

CREATE TABLE `mdl_questionnaire_survey` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL DEFAULT '',
  `owner` varchar(16) NOT NULL DEFAULT '',
  `realm` varchar(64) NOT NULL DEFAULT '',
  `status` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) DEFAULT NULL,
  `subtitle` text,
  `info` text,
  `theme` varchar(64) DEFAULT NULL,
  `thanks_page` varchar(255) DEFAULT NULL,
  `thank_head` varchar(255) DEFAULT NULL,
  `thank_body` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='questionnaire_survey table retrofitted from MySQL';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_question_answers`
--

CREATE TABLE `mdl_question_answers` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `question` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `answer` text NOT NULL,
  `fraction` double NOT NULL DEFAULT '0',
  `feedback` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Answers, with a fractional grade (0-1) and feedback';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_question_attempts`
--

CREATE TABLE `mdl_question_attempts` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `modulename` varchar(20) NOT NULL DEFAULT 'quiz'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Student attempts. This table gets extended by the modules';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_question_calculated`
--

CREATE TABLE `mdl_question_calculated` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `question` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `answer` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `tolerance` varchar(20) NOT NULL DEFAULT '0.0',
  `tolerancetype` bigint(10) NOT NULL DEFAULT '1',
  `correctanswerlength` bigint(10) NOT NULL DEFAULT '2',
  `correctanswerformat` bigint(10) NOT NULL DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Options for questions of type calculated';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_question_categories`
--

CREATE TABLE `mdl_question_categories` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `contextid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `info` text NOT NULL,
  `stamp` varchar(255) NOT NULL DEFAULT '',
  `parent` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `sortorder` bigint(10) UNSIGNED NOT NULL DEFAULT '999'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Categories are for grouping questions';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_question_datasets`
--

CREATE TABLE `mdl_question_datasets` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `question` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `datasetdefinition` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Many-many relation between questions and dataset definitions';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_question_dataset_definitions`
--

CREATE TABLE `mdl_question_dataset_definitions` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `category` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `type` bigint(10) NOT NULL DEFAULT '0',
  `options` varchar(255) NOT NULL DEFAULT '',
  `itemcount` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Organises and stores properties for dataset items';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_question_dataset_items`
--

CREATE TABLE `mdl_question_dataset_items` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `definition` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `itemnumber` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `value` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Individual dataset items';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_question_match`
--

CREATE TABLE `mdl_question_match` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `question` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `subquestions` varchar(255) NOT NULL DEFAULT '',
  `shuffleanswers` smallint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Defines fixed matching questions';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_question_match_sub`
--

CREATE TABLE `mdl_question_match_sub` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `code` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `question` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `questiontext` text NOT NULL,
  `answertext` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Defines the subquestions that make up a matching question';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_question_multianswer`
--

CREATE TABLE `mdl_question_multianswer` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `question` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `sequence` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Options for multianswer questions';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_question_multichoice`
--

CREATE TABLE `mdl_question_multichoice` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `question` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `layout` smallint(4) NOT NULL DEFAULT '0',
  `answers` varchar(255) NOT NULL DEFAULT '',
  `single` smallint(4) NOT NULL DEFAULT '0',
  `shuffleanswers` smallint(4) NOT NULL DEFAULT '1',
  `correctfeedback` text NOT NULL,
  `partiallycorrectfeedback` text NOT NULL,
  `incorrectfeedback` text NOT NULL,
  `answernumbering` varchar(10) NOT NULL DEFAULT 'abc'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Options for multiple choice questions';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_question_numerical`
--

CREATE TABLE `mdl_question_numerical` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `question` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `answer` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `tolerance` varchar(255) NOT NULL DEFAULT '0.0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Options for numerical questions';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_question_numerical_units`
--

CREATE TABLE `mdl_question_numerical_units` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `question` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `multiplier` decimal(40,20) NOT NULL DEFAULT '1.00000000000000000000',
  `unit` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Optional unit options for numerical questions';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_question_randomsamatch`
--

CREATE TABLE `mdl_question_randomsamatch` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `question` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `choose` bigint(10) UNSIGNED NOT NULL DEFAULT '4'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Info about a random short-answer matching question';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_question_sessions`
--

CREATE TABLE `mdl_question_sessions` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `attemptid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `questionid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `newest` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `newgraded` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `sumpenalty` double NOT NULL DEFAULT '0',
  `manualcomment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Gives ids of the newest open and newest graded states';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_question_shortanswer`
--

CREATE TABLE `mdl_question_shortanswer` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `question` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `answers` varchar(255) NOT NULL DEFAULT '',
  `usecase` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Options for short answer questions';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_question_states`
--

CREATE TABLE `mdl_question_states` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `attempt` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `question` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `originalquestion` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `seq_number` mediumint(6) UNSIGNED NOT NULL DEFAULT '0',
  `answer` text NOT NULL,
  `timestamp` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `event` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `grade` double NOT NULL DEFAULT '0',
  `raw_grade` double NOT NULL DEFAULT '0',
  `penalty` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores user responses to an attempt, and percentage grades';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_question_truefalse`
--

CREATE TABLE `mdl_question_truefalse` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `question` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `trueanswer` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `falseanswer` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Options for True-False questions';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_quiz`
--

CREATE TABLE `mdl_quiz` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `intro` text NOT NULL,
  `timeopen` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timeclose` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `optionflags` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `penaltyscheme` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `attempts` mediumint(6) NOT NULL DEFAULT '0',
  `attemptonlast` smallint(4) NOT NULL DEFAULT '0',
  `grademethod` smallint(4) NOT NULL DEFAULT '1',
  `decimalpoints` smallint(4) NOT NULL DEFAULT '2',
  `review` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `questionsperpage` bigint(10) NOT NULL DEFAULT '0',
  `shufflequestions` smallint(4) NOT NULL DEFAULT '0',
  `shuffleanswers` smallint(4) NOT NULL DEFAULT '0',
  `questions` text NOT NULL,
  `sumgrades` bigint(10) NOT NULL DEFAULT '0',
  `grade` bigint(10) NOT NULL DEFAULT '0',
  `timecreated` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timelimit` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `password` varchar(255) NOT NULL DEFAULT '',
  `subnet` varchar(255) NOT NULL DEFAULT '',
  `popup` smallint(4) NOT NULL DEFAULT '0',
  `delay1` bigint(10) NOT NULL DEFAULT '0',
  `delay2` bigint(10) NOT NULL DEFAULT '0',
  `point` bigint(7) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Main information about each quiz';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_quiz_attempts`
--

CREATE TABLE `mdl_quiz_attempts` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `uniqueid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `quiz` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `attempt` mediumint(6) NOT NULL DEFAULT '0',
  `sumgrades` double NOT NULL DEFAULT '0',
  `timestart` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timefinish` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `layout` text NOT NULL,
  `preview` smallint(3) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores various attempts on a quiz';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_quiz_feedback`
--

CREATE TABLE `mdl_quiz_feedback` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `quizid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `feedbacktext` text NOT NULL,
  `mingrade` double NOT NULL DEFAULT '0',
  `maxgrade` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Feedback given to students based on their overall score on t';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_quiz_grades`
--

CREATE TABLE `mdl_quiz_grades` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `quiz` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `grade` double NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Final quiz grade (may be best of several attempts)';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_quiz_question_instances`
--

CREATE TABLE `mdl_quiz_question_instances` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `quiz` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `question` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `grade` mediumint(6) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='The grade for a question in a quiz';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_quiz_question_versions`
--

CREATE TABLE `mdl_quiz_question_versions` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `quiz` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `oldquestion` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `newquestion` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `originalquestion` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timestamp` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='quiz_question_versions table retrofitted from MySQL';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_realtimeattendance`
--

CREATE TABLE `mdl_realtimeattendance` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `status` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `currentquestion` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `nextendtime` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `currentsessionid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `questiontime` smallint(3) UNSIGNED NOT NULL DEFAULT '30',
  `classresult` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `questionresult` smallint(3) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Defines realtime quizzes';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_realtimeattendance_answer`
--

CREATE TABLE `mdl_realtimeattendance_answer` (
  `id` bigint(10) NOT NULL,
  `questionid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `answertext` varchar(255) NOT NULL DEFAULT '',
  `correct` tinyint(2) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='The available answers for each question in the realtime quiz';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_realtimeattendance_question`
--

CREATE TABLE `mdl_realtimeattendance_question` (
  `id` bigint(10) NOT NULL,
  `quizid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `questionnum` smallint(3) UNSIGNED NOT NULL DEFAULT '0',
  `questiontext` varchar(255) NOT NULL DEFAULT '',
  `questiontime` mediumint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Defines questions for the realtime quizzes';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_realtimeattendance_session`
--

CREATE TABLE `mdl_realtimeattendance_session` (
  `id` bigint(10) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `quizid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timestamp` bigint(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Details about each quiz session';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_realtimeattendance_submitted`
--

CREATE TABLE `mdl_realtimeattendance_submitted` (
  `id` bigint(10) NOT NULL,
  `questionid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `sessionid` bigint(10) UNSIGNED DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `answerid` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='The answers that have been submitted by students';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_resource`
--

CREATE TABLE `mdl_resource` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(30) NOT NULL DEFAULT '',
  `reference` varchar(255) NOT NULL DEFAULT '',
  `summary` text,
  `alltext` mediumtext NOT NULL,
  `duration` bigint(10) UNSIGNED NOT NULL,
  `point` bigint(7) UNSIGNED NOT NULL DEFAULT '0',
  `popup` text NOT NULL,
  `options` varchar(255) NOT NULL DEFAULT '',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `nongrade` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='each record is one resource and its config data';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_role`
--

CREATE TABLE `mdl_role` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `shortname` varchar(100) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `sortorder` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='moodle roles';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_role_allow_assign`
--

CREATE TABLE `mdl_role_allow_assign` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `roleid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `allowassign` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='this defines what role can assign what role';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_role_allow_override`
--

CREATE TABLE `mdl_role_allow_override` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `roleid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `allowoverride` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='this defines what role can override what role';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_role_assignments`
--

CREATE TABLE `mdl_role_assignments` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `roleid` bigint(10) UNSIGNED DEFAULT '0',
  `contextid` bigint(10) UNSIGNED DEFAULT '0',
  `userid` bigint(10) UNSIGNED DEFAULT '0',
  `hidden` tinyint(1) UNSIGNED DEFAULT '0',
  `timestart` bigint(10) UNSIGNED DEFAULT '0',
  `timeend` bigint(10) UNSIGNED DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED DEFAULT '0',
  `modifierid` bigint(10) UNSIGNED DEFAULT '0',
  `enrol` varchar(20) DEFAULT NULL,
  `sortorder` bigint(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='assigning roles to different context';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_role_assignments_backup2015`
--

CREATE TABLE `mdl_role_assignments_backup2015` (
  `id` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `roleid` bigint(10) UNSIGNED DEFAULT '0',
  `contextid` bigint(10) UNSIGNED DEFAULT '0',
  `userid` bigint(10) UNSIGNED DEFAULT '0',
  `hidden` tinyint(1) UNSIGNED DEFAULT '0',
  `timestart` bigint(10) UNSIGNED DEFAULT '0',
  `timeend` bigint(10) UNSIGNED DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED DEFAULT '0',
  `modifierid` bigint(10) UNSIGNED DEFAULT '0',
  `enrol` varchar(20) DEFAULT NULL,
  `sortorder` bigint(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mdl_role_capabilities`
--

CREATE TABLE `mdl_role_capabilities` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `contextid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `roleid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `capability` varchar(255) NOT NULL DEFAULT '',
  `permission` bigint(10) NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `modifierid` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='permission has to be signed, overriding a capability for a p';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_role_names`
--

CREATE TABLE `mdl_role_names` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `roleid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `contextid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='role names in native strings';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_role_sortorder`
--

CREATE TABLE `mdl_role_sortorder` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `userid` bigint(10) UNSIGNED NOT NULL,
  `roleid` bigint(10) UNSIGNED NOT NULL,
  `contextid` bigint(10) UNSIGNED NOT NULL,
  `sortoder` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='sort order of course managers in a course';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_scale`
--

CREATE TABLE `mdl_scale` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `courseid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `scale` text NOT NULL,
  `description` text NOT NULL,
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Defines grading scales';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_scale_history`
--

CREATE TABLE `mdl_scale_history` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `action` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `oldid` bigint(10) UNSIGNED NOT NULL,
  `source` varchar(255) DEFAULT NULL,
  `timemodified` bigint(10) UNSIGNED DEFAULT NULL,
  `loggeduser` bigint(10) UNSIGNED DEFAULT NULL,
  `courseid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `scale` text NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='History table';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_scheduler`
--

CREATE TABLE `mdl_scheduler` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `schedulermode` varchar(10) DEFAULT NULL,
  `reuseguardtime` bigint(10) DEFAULT NULL,
  `defaultslotduration` smallint(4) DEFAULT '15',
  `allownotifications` smallint(4) NOT NULL DEFAULT '0',
  `staffrolename` varchar(40) NOT NULL DEFAULT '',
  `teacher` bigint(10) NOT NULL DEFAULT '0',
  `scale` bigint(10) NOT NULL DEFAULT '0',
  `gradingstrategy` tinyint(2) NOT NULL DEFAULT '0',
  `timemodified` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='scheduler table retrofitted from MySQL';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_scheduler_appointment`
--

CREATE TABLE `mdl_scheduler_appointment` (
  `id` bigint(11) UNSIGNED NOT NULL,
  `slotid` bigint(11) NOT NULL,
  `studentid` bigint(11) NOT NULL,
  `attended` smallint(4) NOT NULL,
  `grade` smallint(4) DEFAULT NULL,
  `appointmentnote` text,
  `timecreated` bigint(11) DEFAULT NULL,
  `timemodified` bigint(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='scheduler_appointment table retrofitted from MySQL';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_scheduler_slots`
--

CREATE TABLE `mdl_scheduler_slots` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `schedulerid` bigint(10) NOT NULL DEFAULT '0',
  `starttime` bigint(10) NOT NULL DEFAULT '0',
  `duration` bigint(10) NOT NULL DEFAULT '0',
  `teacherid` bigint(11) DEFAULT '0',
  `appointmentlocation` varchar(50) NOT NULL DEFAULT '',
  `reuse` mediumint(5) UNSIGNED DEFAULT '0',
  `timemodified` bigint(10) NOT NULL DEFAULT '0',
  `notes` text,
  `exclusivity` smallint(4) UNSIGNED NOT NULL DEFAULT '1',
  `appointmentnote` text,
  `emaildate` bigint(11) DEFAULT '0',
  `hideuntil` bigint(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='scheduler_slots table retrofitted from MySQL';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_scorm`
--

CREATE TABLE `mdl_scorm` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `reference` varchar(255) NOT NULL DEFAULT '',
  `summary` text NOT NULL,
  `version` varchar(9) NOT NULL DEFAULT '',
  `maxgrade` double NOT NULL DEFAULT '0',
  `grademethod` tinyint(2) NOT NULL DEFAULT '0',
  `whatgrade` bigint(10) NOT NULL DEFAULT '0',
  `maxattempt` bigint(10) NOT NULL DEFAULT '1',
  `updatefreq` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `md5hash` varchar(32) NOT NULL DEFAULT '',
  `launch` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `skipview` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `hidebrowse` tinyint(1) NOT NULL DEFAULT '0',
  `hidetoc` tinyint(1) NOT NULL DEFAULT '0',
  `hidenav` tinyint(1) NOT NULL DEFAULT '0',
  `auto` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `popup` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `options` varchar(255) NOT NULL DEFAULT '',
  `width` bigint(10) UNSIGNED NOT NULL DEFAULT '100',
  `height` bigint(10) UNSIGNED NOT NULL DEFAULT '600',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `duration` bigint(10) UNSIGNED DEFAULT NULL,
  `point` bigint(7) UNSIGNED NOT NULL DEFAULT '0',
  `elearningversion` varchar(20) DEFAULT NULL,
  `scormimage` varchar(255) DEFAULT NULL,
  `capturefeedback` bigint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='each table is one SCORM module and its configuration';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_scorm_scoes`
--

CREATE TABLE `mdl_scorm_scoes` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `scorm` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `manifest` varchar(255) NOT NULL DEFAULT '',
  `organization` varchar(255) NOT NULL DEFAULT '',
  `parent` varchar(255) NOT NULL DEFAULT '',
  `identifier` varchar(255) NOT NULL DEFAULT '',
  `launch` varchar(255) NOT NULL DEFAULT '',
  `scormtype` varchar(5) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='each SCO part of the SCORM module';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_scorm_scoes_data`
--

CREATE TABLE `mdl_scorm_scoes_data` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `scoid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Contains variable data get from packages';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_scorm_scoes_track`
--

CREATE TABLE `mdl_scorm_scoes_track` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `scormid` bigint(10) NOT NULL DEFAULT '0',
  `scoid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `attempt` bigint(10) UNSIGNED NOT NULL DEFAULT '1',
  `element` varchar(255) NOT NULL DEFAULT '',
  `value` varchar(650) DEFAULT NULL,
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='to track SCOes';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_scorm_seq_mapinfo`
--

CREATE TABLE `mdl_scorm_seq_mapinfo` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `scoid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `objectiveid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `targetobjectiveid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `readsatisfiedstatus` tinyint(1) NOT NULL DEFAULT '1',
  `readnormalizedmeasure` tinyint(1) NOT NULL DEFAULT '1',
  `writesatisfiedstatus` tinyint(1) NOT NULL DEFAULT '0',
  `writenormalizedmeasure` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='SCORM2004 objective mapinfo description';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_skillsoft`
--

CREATE TABLE `mdl_skillsoft` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED DEFAULT '0',
  `assetid` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `summary` text,
  `audience` text,
  `prereq` text,
  `launch` varchar(255) NOT NULL,
  `mastery` varchar(5) DEFAULT NULL,
  `assettype` varchar(128) DEFAULT NULL,
  `duration` bigint(10) UNSIGNED DEFAULT '0',
  `point` bigint(7) UNSIGNED NOT NULL DEFAULT '0',
  `completable` tinyint(1) UNSIGNED DEFAULT '1',
  `timemodified` bigint(10) UNSIGNED DEFAULT '0',
  `timecreated` bigint(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='each table is one skillsoft asset and its configuration';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_skillsoft_au_track`
--

CREATE TABLE `mdl_skillsoft_au_track` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `skillsoftid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `attempt` bigint(10) UNSIGNED NOT NULL DEFAULT '1',
  `element` varchar(255) NOT NULL,
  `value` longtext NOT NULL,
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='to track AUs' ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `mdl_skillsoft_session_track`
--

CREATE TABLE `mdl_skillsoft_session_track` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `sessionid` varchar(128) NOT NULL,
  `skillsoftid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timecreated` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='to track sessions' ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `mdl_subcourse`
--

CREATE TABLE `mdl_subcourse` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `intro` mediumtext,
  `introformat` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `timecreated` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timefetched` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `refcourse` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `grade` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `autoenrol` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Default comment for subcourse, please edit me';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_survey`
--

CREATE TABLE `mdl_survey` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `template` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `days` mediumint(6) NOT NULL DEFAULT '0',
  `timecreated` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `intro` text NOT NULL,
  `questions` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Each record is one SURVEY module with its configuration';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_survey_analysis`
--

CREATE TABLE `mdl_survey_analysis` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `survey` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `userid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `notes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='text about each survey submission';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_survey_questions`
--

CREATE TABLE `mdl_survey_questions` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `text` varchar(255) NOT NULL DEFAULT '',
  `shorttext` varchar(30) NOT NULL DEFAULT '',
  `multi` varchar(100) NOT NULL DEFAULT '',
  `intro` varchar(50) NOT NULL DEFAULT '',
  `type` smallint(3) NOT NULL DEFAULT '0',
  `options` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='the questions conforming one survey';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_swf`
--

CREATE TABLE `mdl_swf` (
  `id` bigint(11) UNSIGNED NOT NULL,
  `course` bigint(11) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `intro` mediumtext COLLATE utf8_unicode_ci,
  `introformat` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `timecreated` bigint(11) UNSIGNED NOT NULL DEFAULT '0',
  `timemodified` bigint(11) UNSIGNED NOT NULL DEFAULT '0',
  `swfurl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `width` varchar(6) COLLATE utf8_unicode_ci DEFAULT '900',
  `height` varchar(6) COLLATE utf8_unicode_ci NOT NULL DEFAULT '480',
  `fullbrowser` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false',
  `notes` mediumtext COLLATE utf8_unicode_ci,
  `version` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '9.0.115',
  `interaction` bigint(11) UNSIGNED DEFAULT NULL,
  `xmlurl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apikey` mediumtext COLLATE utf8_unicode_ci,
  `play` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'true',
  `loopswf` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'true',
  `menu` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'true',
  `quality` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'best',
  `scale` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'noscale',
  `salign` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'tl',
  `wmode` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'opaque',
  `bgcolor` varchar(6) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'FFFFFF',
  `devicefont` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'true',
  `seamlesstabbing` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'true',
  `allowfullscreen` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false',
  `allowscriptaccess` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'sameDomain',
  `allownetworking` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'all',
  `align` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'middle',
  `name1` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value1` longtext COLLATE utf8_unicode_ci,
  `name2` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value2` longtext COLLATE utf8_unicode_ci,
  `name3` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value3` longtext COLLATE utf8_unicode_ci,
  `gradetype` smallint(4) UNSIGNED NOT NULL DEFAULT '1',
  `grademax` float(10,5) UNSIGNED NOT NULL DEFAULT '100.00000',
  `grademin` float(10,5) UNSIGNED NOT NULL DEFAULT '0.00000',
  `gradepass` float(10,5) UNSIGNED NOT NULL DEFAULT '60.00000',
  `allowreview` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'true',
  `sequenced` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false',
  `feedback` longtext COLLATE utf8_unicode_ci,
  `feedbacklink` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `skin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `configxml` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Instances of SWF activity module';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_tag`
--

CREATE TABLE `mdl_tag` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `userid` bigint(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `rawname` varchar(255) NOT NULL DEFAULT '',
  `tagtype` varchar(255) DEFAULT NULL,
  `description` text,
  `descriptionformat` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `flag` smallint(4) UNSIGNED DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tag table - this generic table will replace the old "tags" t';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_tracker`
--

CREATE TABLE `mdl_tracker` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(11) UNSIGNED DEFAULT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `format` smallint(4) NOT NULL,
  `requirelogin` tinyint(1) NOT NULL DEFAULT '1',
  `allownotifications` smallint(3) UNSIGNED NOT NULL,
  `enablecomments` smallint(3) UNSIGNED NOT NULL,
  `ticketprefix` varchar(16) NOT NULL DEFAULT '',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `parent` varchar(40) DEFAULT NULL,
  `supportmode` varchar(20) NOT NULL DEFAULT 'bugtracker'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='tracker table retrofitted from MySQL';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_tracker_element`
--

CREATE TABLE `mdl_tracker_element` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `type` varchar(32) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='tracker_element table retrofitted from MySQL';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_tracker_issue`
--

CREATE TABLE `mdl_tracker_issue` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `trackerid` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `summary` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `format` smallint(4) NOT NULL,
  `datereported` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `reportedby` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `status` bigint(10) UNSIGNED DEFAULT NULL,
  `assignedto` bigint(11) NOT NULL,
  `bywhomid` bigint(11) NOT NULL,
  `timeassigned` bigint(11) DEFAULT NULL,
  `resolution` text,
  `resolutionformat` bigint(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='tracker_issue table retrofitted from MySQL';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_user`
--

CREATE TABLE `mdl_user` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `auth` varchar(20) DEFAULT 'ldap',
  `confirmed` tinyint(1) DEFAULT '1',
  `policyagreed` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `mnethostid` bigint(10) UNSIGNED DEFAULT '1',
  `username` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(32) DEFAULT 'not cached',
  `idnumber` bigint(10) UNSIGNED DEFAULT '0',
  `firstname` varchar(100) NOT NULL DEFAULT '',
  `lastname` varchar(100) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `emailstop` tinyint(1) UNSIGNED DEFAULT '0',
  `icq` varchar(30) DEFAULT NULL,
  `skype` varchar(50) DEFAULT '0',
  `yahoo` varchar(50) DEFAULT '0',
  `aim` varchar(50) DEFAULT '0',
  `msn` varchar(50) DEFAULT '0',
  `phone1` varchar(20) DEFAULT '0',
  `phone2` varchar(20) DEFAULT '0',
  `institution` varchar(255) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `address` varchar(70) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `country` varchar(10) DEFAULT NULL,
  `lang` varchar(30) DEFAULT 'en_utf8',
  `theme` varchar(50) DEFAULT NULL,
  `timezone` varchar(100) DEFAULT 'America/New_York',
  `firstaccess` bigint(10) UNSIGNED DEFAULT '0',
  `lastaccess` bigint(10) UNSIGNED DEFAULT '0',
  `lastlogin` bigint(10) UNSIGNED DEFAULT '0',
  `currentlogin` bigint(10) UNSIGNED DEFAULT '0',
  `lastip` varchar(15) DEFAULT NULL,
  `secret` varchar(15) DEFAULT NULL,
  `picture` tinyint(1) DEFAULT '0',
  `url` varchar(255) DEFAULT NULL,
  `description` text,
  `mailformat` tinyint(1) UNSIGNED DEFAULT '1',
  `maildigest` tinyint(1) UNSIGNED DEFAULT '0',
  `maildisplay` tinyint(2) UNSIGNED DEFAULT '2',
  `htmleditor` tinyint(1) UNSIGNED DEFAULT '1',
  `ajax` tinyint(1) UNSIGNED DEFAULT '1',
  `autosubscribe` tinyint(1) UNSIGNED DEFAULT '1',
  `trackforums` tinyint(1) UNSIGNED DEFAULT '0',
  `timemodified` bigint(10) UNSIGNED DEFAULT '0',
  `trustbitmask` bigint(10) UNSIGNED DEFAULT '0',
  `imagealt` varchar(255) DEFAULT NULL,
  `screenreader` tinyint(1) DEFAULT '0',
  `costcenter` varchar(100) DEFAULT NULL,
  `vertical` varchar(255) DEFAULT NULL,
  `grade` varchar(45) DEFAULT NULL,
  `core_skill` varchar(100) DEFAULT NULL,
  `BU` varchar(55) DEFAULT NULL,
  `RU` varchar(20) DEFAULT NULL,
  `employee_type` varchar(20) DEFAULT NULL,
  `manager_portalid` varchar(100) DEFAULT NULL,
  `source` varchar(100) DEFAULT NULL,
  `mobilekey` varchar(255) DEFAULT NULL,
  `hireddate` varchar(15) DEFAULT NULL,
  `mandatorytrainings` varchar(50) DEFAULT 'null',
  `LC` varchar(10) DEFAULT 'N',
  `practise` varchar(100) DEFAULT NULL,
  `timeupdated` varchar(45) DEFAULT NULL,
  `timecreated` varchar(45) DEFAULT NULL,
  `loaddate` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='One record for each person';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_user_info_category`
--

CREATE TABLE `mdl_user_info_category` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `sortorder` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customisable fields categories';

-- --------------------------------------------------------

--
-- Table structure for table `mdl_user_map`
--

CREATE TABLE `mdl_user_map` (
  `portalid` varchar(6) CHARACTER SET latin1 NOT NULL,
  `BU` varchar(20) DEFAULT NULL,
  `manager_portalid` varchar(55) DEFAULT NULL,
  `grade` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mdl_wiki`
--

CREATE TABLE `mdl_wiki` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `course` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `summary` text NOT NULL,
  `pagename` varchar(255) NOT NULL DEFAULT '',
  `wtype` enum('teacher','group','student') NOT NULL DEFAULT 'group',
  `ewikiprinttitle` smallint(4) UNSIGNED NOT NULL DEFAULT '1',
  `htmlmode` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `ewikiacceptbinary` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `disablecamelcase` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `setpageflags` smallint(4) UNSIGNED NOT NULL DEFAULT '1',
  `strippages` smallint(4) UNSIGNED NOT NULL DEFAULT '1',
  `removepages` smallint(4) UNSIGNED NOT NULL DEFAULT '1',
  `revertchanges` smallint(4) UNSIGNED NOT NULL DEFAULT '1',
  `initialcontent` varchar(255) NOT NULL DEFAULT '',
  `timemodified` bigint(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Main wik table';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adodb_logsql`
--
ALTER TABLE `adodb_logsql`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_assignment`
--
ALTER TABLE `mdl_assignment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_assi_cou_ix` (`course`);

--
-- Indexes for table `mdl_assignment_submissions`
--
ALTER TABLE `mdl_assignment_submissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_assisubm_use_ix` (`userid`),
  ADD KEY `mdl_assisubm_mai_ix` (`mailed`),
  ADD KEY `mdl_assisubm_tim_ix` (`timemarked`),
  ADD KEY `mdl_assisubm_ass_ix` (`assignment`);

--
-- Indexes for table `mdl_attendance_log`
--
ALTER TABLE `mdl_attendance_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_attelog_ses_ix` (`sessionid`),
  ADD KEY `mdl_attelog_stu_ix` (`studentid`),
  ADD KEY `mdl_attelog_sta_ix` (`statusid`);

--
-- Indexes for table `mdl_attendance_sessions`
--
ALTER TABLE `mdl_attendance_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_attesess_cou_ix` (`courseid`),
  ADD KEY `mdl_attesess_ses_ix` (`sessdate`);

--
-- Indexes for table `mdl_attendance_statuses`
--
ALTER TABLE `mdl_attendance_statuses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_attestat_cou_ix` (`courseid`),
  ADD KEY `mdl_attestat_vis_ix` (`visible`),
  ADD KEY `mdl_attestat_del_ix` (`deleted`);

--
-- Indexes for table `mdl_attforblock`
--
ALTER TABLE `mdl_attforblock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_attf_cou_ix` (`course`);

--
-- Indexes for table `mdl_backup_config`
--
ALTER TABLE `mdl_backup_config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_backconf_nam_uix` (`name`);

--
-- Indexes for table `mdl_backup_courses`
--
ALTER TABLE `mdl_backup_courses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_backcour_cou_uix` (`courseid`);

--
-- Indexes for table `mdl_backup_files`
--
ALTER TABLE `mdl_backup_files`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_backfile_bacfilpat_uix` (`backup_code`,`file_type`,`path`);

--
-- Indexes for table `mdl_backup_ids`
--
ALTER TABLE `mdl_backup_ids`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_backids_bactabold_uix` (`backup_code`,`table_name`,`old_id`);

--
-- Indexes for table `mdl_backup_log`
--
ALTER TABLE `mdl_backup_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_backlog_cou_ix` (`courseid`);

--
-- Indexes for table `mdl_block`
--
ALTER TABLE `mdl_block`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_block_fn_my_menu`
--
ALTER TABLE `mdl_block_fn_my_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_block_fn_my_menu_group_settings`
--
ALTER TABLE `mdl_block_fn_my_menu_group_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_blocfnmymenugrousett_gr_ix` (`groupid`);

--
-- Indexes for table `mdl_block_instance`
--
ALTER TABLE `mdl_block_instance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_blocinst_pag_ix` (`pageid`),
  ADD KEY `mdl_blocinst_pag2_ix` (`pagetype`),
  ADD KEY `mdl_blocinst_blo_ix` (`blockid`);

--
-- Indexes for table `mdl_block_moodle_notifications_courses`
--
ALTER TABLE `mdl_block_moodle_notifications_courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_block_moodle_notifications_log`
--
ALTER TABLE `mdl_block_moodle_notifications_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_block_moodle_notifications_users`
--
ALTER TABLE `mdl_block_moodle_notifications_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_block_my_courses`
--
ALTER TABLE `mdl_block_my_courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_block_pinned`
--
ALTER TABLE `mdl_block_pinned`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_blocpinn_pag_ix` (`pagetype`),
  ADD KEY `mdl_blocpinn_blo_ix` (`blockid`);

--
-- Indexes for table `mdl_block_rate_course`
--
ALTER TABLE `mdl_block_rate_course`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_blocratecour_couuse_uix` (`course`,`userid`);

--
-- Indexes for table `mdl_block_rss_client`
--
ALTER TABLE `mdl_block_rss_client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_block_search_documents`
--
ALTER TABLE `mdl_block_search_documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_blocseardocu_doc_ix` (`docid`),
  ADD KEY `mdl_blocseardocu_doc2_ix` (`doctype`),
  ADD KEY `mdl_blocseardocu_ite_ix` (`itemtype`);

--
-- Indexes for table `mdl_booking`
--
ALTER TABLE `mdl_booking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_book_cou_ix` (`course`);

--
-- Indexes for table `mdl_booking_answers`
--
ALTER TABLE `mdl_booking_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_bookansw_use_ix` (`userid`),
  ADD KEY `mdl_bookansw_boo_ix` (`bookingid`),
  ADD KEY `mdl_bookansw_opt_ix` (`optionid`);

--
-- Indexes for table `mdl_booking_options`
--
ALTER TABLE `mdl_booking_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_bookopti_boo_ix` (`bookingid`);

--
-- Indexes for table `mdl_bu_description`
--
ALTER TABLE `mdl_bu_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_cache_filters`
--
ALTER TABLE `mdl_cache_filters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_cachfilt_filmd5_ix` (`filter`,`md5key`);

--
-- Indexes for table `mdl_cache_flags`
--
ALTER TABLE `mdl_cache_flags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_cachflag_fla_ix` (`flagtype`),
  ADD KEY `mdl_cachflag_nam_ix` (`name`),
  ADD KEY `mdl_cachflag_ix01` (`flagtype`,`expiry`,`timemodified`);

--
-- Indexes for table `mdl_cache_text`
--
ALTER TABLE `mdl_cache_text`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_cachtext_md5_ix` (`md5key`),
  ADD KEY `mdl_cachtext_tim_ix` (`timemodified`);

--
-- Indexes for table `mdl_capabilities`
--
ALTER TABLE `mdl_capabilities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_capa_nam_uix` (`name`);

--
-- Indexes for table `mdl_certificate`
--
ALTER TABLE `mdl_certificate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_certificate_issues`
--
ALTER TABLE `mdl_certificate_issues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_certificate_linked_modules`
--
ALTER TABLE `mdl_certificate_linked_modules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_certlinkmodu_lin_ix` (`linkid`),
  ADD KEY `mdl_certlinkmodu_cer_ix` (`certificate_id`);

--
-- Indexes for table `mdl_chat`
--
ALTER TABLE `mdl_chat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_chat_cou_ix` (`course`);

--
-- Indexes for table `mdl_chat_messages`
--
ALTER TABLE `mdl_chat_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_chatmess_use_ix` (`userid`),
  ADD KEY `mdl_chatmess_gro_ix` (`groupid`),
  ADD KEY `mdl_chatmess_timcha_ix` (`timestamp`,`chatid`),
  ADD KEY `mdl_chatmess_cha_ix` (`chatid`);

--
-- Indexes for table `mdl_chat_users`
--
ALTER TABLE `mdl_chat_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_chatuser_use_ix` (`userid`),
  ADD KEY `mdl_chatuser_las_ix` (`lastping`),
  ADD KEY `mdl_chatuser_gro_ix` (`groupid`),
  ADD KEY `mdl_chatuser_cha_ix` (`chatid`);

--
-- Indexes for table `mdl_choice`
--
ALTER TABLE `mdl_choice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_choi_cou_ix` (`course`);

--
-- Indexes for table `mdl_choice_answers`
--
ALTER TABLE `mdl_choice_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_choiansw_use_ix` (`userid`),
  ADD KEY `mdl_choiansw_cho_ix` (`choiceid`),
  ADD KEY `mdl_choiansw_opt_ix` (`optionid`);

--
-- Indexes for table `mdl_choice_options`
--
ALTER TABLE `mdl_choice_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_choiopti_cho_ix` (`choiceid`);

--
-- Indexes for table `mdl_classroom`
--
ALTER TABLE `mdl_classroom`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_face_cou_ix` (`course`);

--
-- Indexes for table `mdl_classroom_detailsmaster`
--
ALTER TABLE `mdl_classroom_detailsmaster`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_classroom_sessions`
--
ALTER TABLE `mdl_classroom_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_facesess_fac_ix` (`classroom`);

--
-- Indexes for table `mdl_classroom_sessions_dates`
--
ALTER TABLE `mdl_classroom_sessions_dates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_facesessdate_ses_ix` (`sessionid`);

--
-- Indexes for table `mdl_classroom_sessions_external`
--
ALTER TABLE `mdl_classroom_sessions_external`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_eventfile` (`eventnamefile`);

--
-- Indexes for table `mdl_classroom_submissions`
--
ALTER TABLE `mdl_classroom_submissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_facesubm_use_ix` (`userid`),
  ADD KEY `mdl_facesubm_mai_ix` (`mailedconfirmation`),
  ADD KEY `mdl_facesubm_mai2_ix` (`mailedreminder`),
  ADD KEY `mdl_facesubm_fac_ix` (`classroom`),
  ADD KEY `mdl_facesubm_ses_ix` (`sessionid`);

--
-- Indexes for table `mdl_classroom_trainners`
--
ALTER TABLE `mdl_classroom_trainners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_facesubm_use_ix` (`userid`),
  ADD KEY `mdl_facesubm_mai_ix` (`mailedconfirmation`),
  ADD KEY `mdl_facesubm_mai2_ix` (`mailedreminder`),
  ADD KEY `mdl_facesubm_fac_ix` (`classroom`),
  ADD KEY `mdl_facesubm_ses_ix` (`sessionid`);

--
-- Indexes for table `mdl_config`
--
ALTER TABLE `mdl_config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_conf_nam_uix` (`name`);

--
-- Indexes for table `mdl_config_plugins`
--
ALTER TABLE `mdl_config_plugins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_confplug_plunam_uix` (`plugin`,`name`);

--
-- Indexes for table `mdl_context`
--
ALTER TABLE `mdl_context`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_cont_conins_uix` (`contextlevel`,`instanceid`),
  ADD KEY `mdl_cont_ins_ix` (`instanceid`),
  ADD KEY `mdl_cont_pat_ix` (`path`);

--
-- Indexes for table `mdl_context_temp`
--
ALTER TABLE `mdl_context_temp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_course`
--
ALTER TABLE `mdl_course`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_cour_cat_ix` (`category`),
  ADD KEY `mdl_cour_idn_ix` (`idnumber`),
  ADD KEY `mdl_cour_sho_ix` (`shortname`);

--
-- Indexes for table `mdl_course_allowed_modules`
--
ALTER TABLE `mdl_course_allowed_modules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_courallomodu_cou_ix` (`course`),
  ADD KEY `mdl_courallomodu_mod_ix` (`module`);

--
-- Indexes for table `mdl_course_allowed_modules_temp`
--
ALTER TABLE `mdl_course_allowed_modules_temp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_courallomodu_cou_ix` (`course`),
  ADD KEY `mdl_courallomodu_mod_ix` (`module`);

--
-- Indexes for table `mdl_course_categories`
--
ALTER TABLE `mdl_course_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_courcate_par_ix` (`parent`);

--
-- Indexes for table `mdl_course_categories_temp`
--
ALTER TABLE `mdl_course_categories_temp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_courcate_par_ix` (`parent`);

--
-- Indexes for table `mdl_course_display`
--
ALTER TABLE `mdl_course_display`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_courdisp_couuse_ix` (`course`,`userid`);

--
-- Indexes for table `mdl_course_meta`
--
ALTER TABLE `mdl_course_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_courmeta_par_ix` (`parent_course`),
  ADD KEY `mdl_courmeta_chi_ix` (`child_course`);

--
-- Indexes for table `mdl_course_modules`
--
ALTER TABLE `mdl_course_modules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_courmodu_vis_ix` (`visible`),
  ADD KEY `mdl_courmodu_cou_ix` (`course`),
  ADD KEY `mdl_courmodu_mod_ix` (`module`),
  ADD KEY `mdl_courmodu_ins_ix` (`instance`),
  ADD KEY `mdl_courmodu_idncou_ix` (`idnumber`,`course`),
  ADD KEY `mdl_courmodu_gro_ix` (`groupingid`);

--
-- Indexes for table `mdl_course_module_locks`
--
ALTER TABLE `mdl_course_module_locks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_courmodulock_loc_ix` (`lockid`),
  ADD KEY `mdl_courmodulock_mod_ix` (`moduleid`),
  ADD KEY `mdl_courmodulock_cou_ix` (`courseid`);

--
-- Indexes for table `mdl_course_request`
--
ALTER TABLE `mdl_course_request`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_courrequ_sho_ix` (`shortname`);

--
-- Indexes for table `mdl_course_sections`
--
ALTER TABLE `mdl_course_sections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_coursect_cousec_ix` (`course`,`section`);

--
-- Indexes for table `mdl_course_temp`
--
ALTER TABLE `mdl_course_temp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_cour_cat_ix` (`category`),
  ADD KEY `mdl_cour_idn_ix` (`idnumber`),
  ADD KEY `mdl_cour_sho_ix` (`shortname`);

--
-- Indexes for table `mdl_cpd`
--
ALTER TABLE `mdl_cpd`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_cpd_use_ix` (`userid`),
  ADD KEY `mdl_cpd_act_ix` (`activitytypeid`),
  ADD KEY `mdl_cpd_sta_ix` (`statusid`),
  ADD KEY `mdl_cpd_cpd_ix` (`cpdyearid`);

--
-- Indexes for table `mdl_cpd_activity_type`
--
ALTER TABLE `mdl_cpd_activity_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_cpd_status`
--
ALTER TABLE `mdl_cpd_status`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_cpdstat_nam_uix` (`name`);

--
-- Indexes for table `mdl_cpd_year`
--
ALTER TABLE `mdl_cpd_year`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_csr`
--
ALTER TABLE `mdl_csr`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_csr_cou_ix` (`course`);

--
-- Indexes for table `mdl_csr_notice`
--
ALTER TABLE `mdl_csr_notice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_csr_notice_data`
--
ALTER TABLE `mdl_csr_notice_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_csrnotidata_fie_ix` (`fieldid`);

--
-- Indexes for table `mdl_csr_sessions`
--
ALTER TABLE `mdl_csr_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_csrsess_csr_ix` (`csr`);

--
-- Indexes for table `mdl_csr_sessions_dates`
--
ALTER TABLE `mdl_csr_sessions_dates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_csrsessdate_ses_ix` (`sessionid`);

--
-- Indexes for table `mdl_csr_session_data`
--
ALTER TABLE `mdl_csr_session_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_csr_session_field`
--
ALTER TABLE `mdl_csr_session_field`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_csrsessfiel_sho_uix` (`shortname`);

--
-- Indexes for table `mdl_csr_session_roles`
--
ALTER TABLE `mdl_csr_session_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_csrsessrole_ses_ix` (`sessionid`);

--
-- Indexes for table `mdl_csr_signups`
--
ALTER TABLE `mdl_csr_signups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_csrsign_ses_ix` (`sessionid`);

--
-- Indexes for table `mdl_csr_signups_status`
--
ALTER TABLE `mdl_csr_signups_status`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_csrsignstat_sig_ix` (`signupid`);

--
-- Indexes for table `mdl_csr_user_signups`
--
ALTER TABLE `mdl_csr_user_signups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_csrsign_ses_ix` (`sessionid`);

--
-- Indexes for table `mdl_data`
--
ALTER TABLE `mdl_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_data_cou_ix` (`course`);

--
-- Indexes for table `mdl_data_comments`
--
ALTER TABLE `mdl_data_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_datacomm_rec_ix` (`recordid`);

--
-- Indexes for table `mdl_data_content`
--
ALTER TABLE `mdl_data_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_datacont_rec_ix` (`recordid`),
  ADD KEY `mdl_datacont_fie_ix` (`fieldid`);

--
-- Indexes for table `mdl_data_fields`
--
ALTER TABLE `mdl_data_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_datafiel_typdat_ix` (`type`,`dataid`),
  ADD KEY `mdl_datafiel_dat_ix` (`dataid`);

--
-- Indexes for table `mdl_data_ratings`
--
ALTER TABLE `mdl_data_ratings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_datarati_rec_ix` (`recordid`);

--
-- Indexes for table `mdl_data_records`
--
ALTER TABLE `mdl_data_records`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_datareco_dat_ix` (`dataid`);

--
-- Indexes for table `mdl_elearning_question`
--
ALTER TABLE `mdl_elearning_question`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `mdl_elearning_value`
--
ALTER TABLE `mdl_elearning_value`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `mdl_enrol_approvalworkflow`
--
ALTER TABLE `mdl_enrol_approvalworkflow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_enrol_authorize`
--
ALTER TABLE `mdl_enrol_authorize`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_enroauth_cou_ix` (`courseid`),
  ADD KEY `mdl_enroauth_use_ix` (`userid`),
  ADD KEY `mdl_enroauth_sta_ix` (`status`),
  ADD KEY `mdl_enroauth_tra_ix` (`transid`);

--
-- Indexes for table `mdl_enrol_authorize_refunds`
--
ALTER TABLE `mdl_enrol_authorize_refunds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_enroauthrefu_tra_ix` (`transid`),
  ADD KEY `mdl_enroauthrefu_ord_ix` (`orderid`);

--
-- Indexes for table `mdl_enrol_paypal`
--
ALTER TABLE `mdl_enrol_paypal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_enrol_skillsoft`
--
ALTER TABLE `mdl_enrol_skillsoft`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_enrol_userregister`
--
ALTER TABLE `mdl_enrol_userregister`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_event`
--
ALTER TABLE `mdl_event`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_even_cou_ix` (`courseid`),
  ADD KEY `mdl_even_use_ix` (`userid`),
  ADD KEY `mdl_even_tim_ix` (`timestart`),
  ADD KEY `mdl_even_tim2_ix` (`timeduration`),
  ADD KEY `mdl_even_grocouvisuse_ix` (`groupid`,`courseid`,`visible`,`userid`);

--
-- Indexes for table `mdl_events_handlers`
--
ALTER TABLE `mdl_events_handlers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_evenhand_evehan_uix` (`eventname`,`handlermodule`);

--
-- Indexes for table `mdl_events_queue`
--
ALTER TABLE `mdl_events_queue`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_evenqueu_use_ix` (`userid`);

--
-- Indexes for table `mdl_events_queue_handlers`
--
ALTER TABLE `mdl_events_queue_handlers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_evenqueuhand_que_ix` (`queuedeventid`),
  ADD KEY `mdl_evenqueuhand_han_ix` (`handlerid`);

--
-- Indexes for table `mdl_feedback`
--
ALTER TABLE `mdl_feedback`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_feed_cou_ix` (`course`);

--
-- Indexes for table `mdl_feedback_completed`
--
ALTER TABLE `mdl_feedback_completed`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_feedcomp_use_ix` (`userid`),
  ADD KEY `mdl_feedcomp_fee_ix` (`feedback`);

--
-- Indexes for table `mdl_feedback_completedtmp`
--
ALTER TABLE `mdl_feedback_completedtmp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_feedcomp_use2_ix` (`userid`),
  ADD KEY `mdl_feedcomp_fee2_ix` (`feedback`);

--
-- Indexes for table `mdl_feedback_ext_employee_response`
--
ALTER TABLE `mdl_feedback_ext_employee_response`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `my_unique_key` (`userid`,`seesionid`);

--
-- Indexes for table `mdl_feedback_ext_items`
--
ALTER TABLE `mdl_feedback_ext_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_feedback_ext_manager_response`
--
ALTER TABLE `mdl_feedback_ext_manager_response`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `my_unique_key` (`userid`,`resource_id`,`seesionid`);

--
-- Indexes for table `mdl_feedback_ext_question`
--
ALTER TABLE `mdl_feedback_ext_question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_feedback_item`
--
ALTER TABLE `mdl_feedback_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_feeditem_fee_ix` (`feedback`),
  ADD KEY `mdl_feeditem_tem_ix` (`template`),
  ADD KEY `index_4` (`category`);

--
-- Indexes for table `mdl_feedback_sitecourse_map`
--
ALTER TABLE `mdl_feedback_sitecourse_map`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_feedsitemap_cou_ix` (`courseid`),
  ADD KEY `mdl_feedsitemap_fee_ix` (`feedbackid`);

--
-- Indexes for table `mdl_feedback_template`
--
ALTER TABLE `mdl_feedback_template`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_feedtemp_cou_ix` (`course`);

--
-- Indexes for table `mdl_feedback_tracking`
--
ALTER TABLE `mdl_feedback_tracking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_feedtrac_use_ix` (`userid`),
  ADD KEY `mdl_feedtrac_fee_ix` (`feedback`),
  ADD KEY `mdl_feedtrac_com_ix` (`completed`);

--
-- Indexes for table `mdl_feedback_user`
--
ALTER TABLE `mdl_feedback_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_feedback_value`
--
ALTER TABLE `mdl_feedback_value`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_feedvalu_cou_ix` (`course_id`),
  ADD KEY `mdl_feedvalu_ite_ix` (`item`),
  ADD KEY `index_4` (`value`(255)) USING BTREE;

--
-- Indexes for table `mdl_feedback_valuetmp`
--
ALTER TABLE `mdl_feedback_valuetmp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_feedvalu_cou2_ix` (`course_id`),
  ADD KEY `mdl_feedvalu_ite2_ix` (`item`);

--
-- Indexes for table `mdl_forum`
--
ALTER TABLE `mdl_forum`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_foru_cou_ix` (`course`);

--
-- Indexes for table `mdl_forum_discussions`
--
ALTER TABLE `mdl_forum_discussions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_forudisc_use_ix` (`userid`),
  ADD KEY `mdl_forudisc_for_ix` (`forum`);

--
-- Indexes for table `mdl_forum_posts`
--
ALTER TABLE `mdl_forum_posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_forupost_use_ix` (`userid`),
  ADD KEY `mdl_forupost_cre_ix` (`created`),
  ADD KEY `mdl_forupost_mai_ix` (`mailed`),
  ADD KEY `mdl_forupost_dis_ix` (`discussion`),
  ADD KEY `mdl_forupost_par_ix` (`parent`);

--
-- Indexes for table `mdl_forum_queue`
--
ALTER TABLE `mdl_forum_queue`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_foruqueu_use_ix` (`userid`),
  ADD KEY `mdl_foruqueu_dis_ix` (`discussionid`),
  ADD KEY `mdl_foruqueu_pos_ix` (`postid`);

--
-- Indexes for table `mdl_forum_ratings`
--
ALTER TABLE `mdl_forum_ratings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_forurati_use_ix` (`userid`),
  ADD KEY `mdl_forurati_pos_ix` (`post`);

--
-- Indexes for table `mdl_forum_read`
--
ALTER TABLE `mdl_forum_read`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_foruread_usefor_ix` (`userid`,`forumid`),
  ADD KEY `mdl_foruread_usedis_ix` (`userid`,`discussionid`),
  ADD KEY `mdl_foruread_usepos_ix` (`userid`,`postid`);

--
-- Indexes for table `mdl_forum_subscriptions`
--
ALTER TABLE `mdl_forum_subscriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_forusubs_use_ix` (`userid`),
  ADD KEY `mdl_forusubs_for_ix` (`forum`);

--
-- Indexes for table `mdl_forum_track_prefs`
--
ALTER TABLE `mdl_forum_track_prefs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_forutracpref_usefor_ix` (`userid`,`forumid`);

--
-- Indexes for table `mdl_game`
--
ALTER TABLE `mdl_game`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_game_attempts`
--
ALTER TABLE `mdl_game_attempts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_gameatte_gamusetim_ix` (`gameid`,`userid`,`timefinish`);

--
-- Indexes for table `mdl_game_badge`
--
ALTER TABLE `mdl_game_badge`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_game_badge_track`
--
ALTER TABLE `mdl_game_badge_track`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_game_bookquiz`
--
ALTER TABLE `mdl_game_bookquiz`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_game_bookquiz_chapters`
--
ALTER TABLE `mdl_game_bookquiz_chapters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_gamebookchap_attcha_ix` (`attemptid`,`chapterid`);

--
-- Indexes for table `mdl_game_bookquiz_questions`
--
ALTER TABLE `mdl_game_bookquiz_questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_gamebookques_gamcha_ix` (`gameid`,`chapterid`);

--
-- Indexes for table `mdl_game_course`
--
ALTER TABLE `mdl_game_course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_game_course_inputs`
--
ALTER TABLE `mdl_game_course_inputs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_game_cross`
--
ALTER TABLE `mdl_game_cross`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_game_cryptex`
--
ALTER TABLE `mdl_game_cryptex`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_game_export_html`
--
ALTER TABLE `mdl_game_export_html`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_game_export_javame`
--
ALTER TABLE `mdl_game_export_javame`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_game_grades`
--
ALTER TABLE `mdl_game_grades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_gamegrad_use_ix` (`userid`),
  ADD KEY `mdl_gamegrad_gam_ix` (`gameid`);

--
-- Indexes for table `mdl_game_hangman`
--
ALTER TABLE `mdl_game_hangman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_game_hiddenpicture`
--
ALTER TABLE `mdl_game_hiddenpicture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_game_millionaire`
--
ALTER TABLE `mdl_game_millionaire`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_game_point`
--
ALTER TABLE `mdl_game_point`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_gam_point_ix01` (`course`,`module`,`userid`);

--
-- Indexes for table `mdl_game_queries`
--
ALTER TABLE `mdl_game_queries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_gamequer_att_ix` (`attemptid`);

--
-- Indexes for table `mdl_game_repetitions`
--
ALTER TABLE `mdl_game_repetitions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_gamerepe_gamusequeglo_uix` (`gameid`,`userid`,`questionid`,`glossaryentryid`);

--
-- Indexes for table `mdl_game_snakes`
--
ALTER TABLE `mdl_game_snakes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_game_snakes_database`
--
ALTER TABLE `mdl_game_snakes_database`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_game_sudoku`
--
ALTER TABLE `mdl_game_sudoku`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_game_sudoku_database`
--
ALTER TABLE `mdl_game_sudoku_database`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_gamesudodata_dat_uix` (`data`);

--
-- Indexes for table `mdl_glossary`
--
ALTER TABLE `mdl_glossary`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_glos_cou_ix` (`course`);

--
-- Indexes for table `mdl_glossary_alias`
--
ALTER TABLE `mdl_glossary_alias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_glosalia_ent_ix` (`entryid`);

--
-- Indexes for table `mdl_glossary_categories`
--
ALTER TABLE `mdl_glossary_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_gloscate_glo_ix` (`glossaryid`);

--
-- Indexes for table `mdl_glossary_comments`
--
ALTER TABLE `mdl_glossary_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_gloscomm_use_ix` (`userid`),
  ADD KEY `mdl_gloscomm_ent_ix` (`entryid`);

--
-- Indexes for table `mdl_glossary_entries`
--
ALTER TABLE `mdl_glossary_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_glosentr_use_ix` (`userid`),
  ADD KEY `mdl_glosentr_con_ix` (`concept`),
  ADD KEY `mdl_glosentr_glo_ix` (`glossaryid`);

--
-- Indexes for table `mdl_glossary_entries_categories`
--
ALTER TABLE `mdl_glossary_entries_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_glosentrcate_cat_ix` (`categoryid`),
  ADD KEY `mdl_glosentrcate_ent_ix` (`entryid`);

--
-- Indexes for table `mdl_glossary_formats`
--
ALTER TABLE `mdl_glossary_formats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_glossary_ratings`
--
ALTER TABLE `mdl_glossary_ratings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_glosrati_use_ix` (`userid`),
  ADD KEY `mdl_glosrati_ent_ix` (`entryid`);

--
-- Indexes for table `mdl_grade_categories`
--
ALTER TABLE `mdl_grade_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_gradcate_cou_ix` (`courseid`),
  ADD KEY `mdl_gradcate_par_ix` (`parent`);

--
-- Indexes for table `mdl_grade_categories_history`
--
ALTER TABLE `mdl_grade_categories_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_gradcatehist_act_ix` (`action`),
  ADD KEY `mdl_gradcatehist_old_ix` (`oldid`),
  ADD KEY `mdl_gradcatehist_cou_ix` (`courseid`),
  ADD KEY `mdl_gradcatehist_par_ix` (`parent`),
  ADD KEY `mdl_gradcatehist_log_ix` (`loggeduser`);

--
-- Indexes for table `mdl_grade_grades`
--
ALTER TABLE `mdl_grade_grades`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_gradgrad_useite_uix` (`userid`,`itemid`),
  ADD KEY `mdl_gradgrad_locloc_ix` (`locked`,`locktime`),
  ADD KEY `mdl_gradgrad_ite_ix` (`itemid`),
  ADD KEY `mdl_gradgrad_use_ix` (`userid`),
  ADD KEY `mdl_gradgrad_raw_ix` (`rawscaleid`),
  ADD KEY `mdl_gradgrad_use2_ix` (`usermodified`),
  ADD KEY `mdl_grade_grades_timecreated` (`timecreated`),
  ADD KEY `mdl_grade_grades_timemodified` (`timemodified`);

--
-- Indexes for table `mdl_grade_grades_history`
--
ALTER TABLE `mdl_grade_grades_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_gradgradhist_act_ix` (`action`),
  ADD KEY `mdl_gradgradhist_old_ix` (`oldid`),
  ADD KEY `mdl_gradgradhist_ite_ix` (`itemid`),
  ADD KEY `mdl_gradgradhist_use_ix` (`userid`),
  ADD KEY `mdl_gradgradhist_raw_ix` (`rawscaleid`),
  ADD KEY `mdl_gradgradhist_use2_ix` (`usermodified`),
  ADD KEY `mdl_gradgradhist_log_ix` (`loggeduser`);

--
-- Indexes for table `mdl_grade_import_newitem`
--
ALTER TABLE `mdl_grade_import_newitem`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_gradimponewi_imp_ix` (`importer`);

--
-- Indexes for table `mdl_grade_import_values`
--
ALTER TABLE `mdl_grade_import_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_gradimpovalu_ite_ix` (`itemid`),
  ADD KEY `mdl_gradimpovalu_new_ix` (`newgradeitem`),
  ADD KEY `mdl_gradimpovalu_imp_ix` (`importer`);

--
-- Indexes for table `mdl_grade_items`
--
ALTER TABLE `mdl_grade_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_graditem_locloc_ix` (`locked`,`locktime`),
  ADD KEY `mdl_graditem_itenee_ix` (`itemtype`,`needsupdate`),
  ADD KEY `mdl_graditem_gra_ix` (`gradetype`),
  ADD KEY `mdl_graditem_idncou_ix` (`idnumber`,`courseid`),
  ADD KEY `mdl_graditem_cou_ix` (`courseid`),
  ADD KEY `mdl_graditem_cat_ix` (`categoryid`),
  ADD KEY `mdl_graditem_sca_ix` (`scaleid`),
  ADD KEY `mdl_graditem_out_ix` (`outcomeid`),
  ADD KEY `mdl_grade_items_timecreated` (`timecreated`),
  ADD KEY `mdl_grade_items_timemodified` (`timemodified`);

--
-- Indexes for table `mdl_grade_items_history`
--
ALTER TABLE `mdl_grade_items_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_graditemhist_act_ix` (`action`),
  ADD KEY `mdl_graditemhist_old_ix` (`oldid`),
  ADD KEY `mdl_graditemhist_cou_ix` (`courseid`),
  ADD KEY `mdl_graditemhist_cat_ix` (`categoryid`),
  ADD KEY `mdl_graditemhist_sca_ix` (`scaleid`),
  ADD KEY `mdl_graditemhist_out_ix` (`outcomeid`),
  ADD KEY `mdl_graditemhist_log_ix` (`loggeduser`);

--
-- Indexes for table `mdl_grade_letters`
--
ALTER TABLE `mdl_grade_letters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_gradlett_conlow_ix` (`contextid`,`lowerboundary`);

--
-- Indexes for table `mdl_grade_outcomes`
--
ALTER TABLE `mdl_grade_outcomes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_gradoutc_cousho_uix` (`courseid`,`shortname`),
  ADD KEY `mdl_gradoutc_cou_ix` (`courseid`),
  ADD KEY `mdl_gradoutc_sca_ix` (`scaleid`),
  ADD KEY `mdl_gradoutc_use_ix` (`usermodified`);

--
-- Indexes for table `mdl_grade_outcomes_courses`
--
ALTER TABLE `mdl_grade_outcomes_courses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_gradoutccour_couout_uix` (`courseid`,`outcomeid`),
  ADD KEY `mdl_gradoutccour_cou_ix` (`courseid`),
  ADD KEY `mdl_gradoutccour_out_ix` (`outcomeid`);

--
-- Indexes for table `mdl_grade_outcomes_history`
--
ALTER TABLE `mdl_grade_outcomes_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_gradoutchist_act_ix` (`action`),
  ADD KEY `mdl_gradoutchist_old_ix` (`oldid`),
  ADD KEY `mdl_gradoutchist_cou_ix` (`courseid`),
  ADD KEY `mdl_gradoutchist_sca_ix` (`scaleid`),
  ADD KEY `mdl_gradoutchist_log_ix` (`loggeduser`);

--
-- Indexes for table `mdl_grade_settings`
--
ALTER TABLE `mdl_grade_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_gradsett_counam_uix` (`courseid`,`name`),
  ADD KEY `mdl_gradsett_cou_ix` (`courseid`);

--
-- Indexes for table `mdl_grade_training`
--
ALTER TABLE `mdl_grade_training`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_groupings`
--
ALTER TABLE `mdl_groupings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_grou_cou2_ix` (`courseid`);

--
-- Indexes for table `mdl_groupings_groups`
--
ALTER TABLE `mdl_groupings_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_grougrou_gro_ix` (`groupingid`),
  ADD KEY `mdl_grougrou_gro2_ix` (`groupid`);

--
-- Indexes for table `mdl_groups`
--
ALTER TABLE `mdl_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_grou_cou_ix` (`courseid`);

--
-- Indexes for table `mdl_groups_members`
--
ALTER TABLE `mdl_groups_members`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_groumemb_gro_ix` (`groupid`),
  ADD KEY `mdl_groumemb_use_ix` (`userid`);

--
-- Indexes for table `mdl_journal`
--
ALTER TABLE `mdl_journal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_jour_cou_ix` (`course`);

--
-- Indexes for table `mdl_journal_entries`
--
ALTER TABLE `mdl_journal_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_jourentr_use_ix` (`userid`),
  ADD KEY `mdl_jourentr_jou_ix` (`journal`);

--
-- Indexes for table `mdl_klci_grade`
--
ALTER TABLE `mdl_klci_grade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_klci_level`
--
ALTER TABLE `mdl_klci_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_klci_practice`
--
ALTER TABLE `mdl_klci_practice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_klci_stream`
--
ALTER TABLE `mdl_klci_stream`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_label`
--
ALTER TABLE `mdl_label`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_labe_cou_ix` (`course`);

--
-- Indexes for table `mdl_lams`
--
ALTER TABLE `mdl_lams`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_lams_cou_ix` (`course`);

--
-- Indexes for table `mdl_lesson`
--
ALTER TABLE `mdl_lesson`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_less_cou_ix` (`course`);

--
-- Indexes for table `mdl_lesson_answers`
--
ALTER TABLE `mdl_lesson_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_lessansw_les_ix` (`lessonid`),
  ADD KEY `mdl_lessansw_pag_ix` (`pageid`);

--
-- Indexes for table `mdl_lesson_attempts`
--
ALTER TABLE `mdl_lesson_attempts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_lessatte_use_ix` (`userid`),
  ADD KEY `mdl_lessatte_les_ix` (`lessonid`),
  ADD KEY `mdl_lessatte_pag_ix` (`pageid`),
  ADD KEY `mdl_lessatte_ans_ix` (`answerid`);

--
-- Indexes for table `mdl_lesson_branch`
--
ALTER TABLE `mdl_lesson_branch`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_lessbran_use_ix` (`userid`),
  ADD KEY `mdl_lessbran_les_ix` (`lessonid`),
  ADD KEY `mdl_lessbran_pag_ix` (`pageid`);

--
-- Indexes for table `mdl_lesson_default`
--
ALTER TABLE `mdl_lesson_default`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_lesson_grades`
--
ALTER TABLE `mdl_lesson_grades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_lessgrad_use_ix` (`userid`),
  ADD KEY `mdl_lessgrad_les_ix` (`lessonid`);

--
-- Indexes for table `mdl_lesson_high_scores`
--
ALTER TABLE `mdl_lesson_high_scores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_lesshighscor_use_ix` (`userid`),
  ADD KEY `mdl_lesshighscor_les_ix` (`lessonid`);

--
-- Indexes for table `mdl_lesson_pages`
--
ALTER TABLE `mdl_lesson_pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_lesspage_les_ix` (`lessonid`);

--
-- Indexes for table `mdl_lesson_timer`
--
ALTER TABLE `mdl_lesson_timer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_lesstime_use_ix` (`userid`),
  ADD KEY `mdl_lesstime_les_ix` (`lessonid`);

--
-- Indexes for table `mdl_log`
--
ALTER TABLE `mdl_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_log_coumodact_ix` (`course`,`module`,`action`),
  ADD KEY `mdl_log_tim_ix` (`time`),
  ADD KEY `mdl_log_act_ix` (`action`),
  ADD KEY `mdl_log_usecou_ix` (`userid`,`course`),
  ADD KEY `mdl_log_cmi_ix` (`cmid`);

--
-- Indexes for table `mdl_log_display`
--
ALTER TABLE `mdl_log_display`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_logdisp_modact_uix` (`module`,`action`);

--
-- Indexes for table `mdl_message`
--
ALTER TABLE `mdl_message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_mess_use_ix` (`useridfrom`),
  ADD KEY `mdl_mess_use2_ix` (`useridto`);

--
-- Indexes for table `mdl_message_contacts`
--
ALTER TABLE `mdl_message_contacts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_messcont_usecon_uix` (`userid`,`contactid`);

--
-- Indexes for table `mdl_message_read`
--
ALTER TABLE `mdl_message_read`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_messread_use_ix` (`useridfrom`),
  ADD KEY `mdl_messread_use2_ix` (`useridto`);

--
-- Indexes for table `mdl_mindmap`
--
ALTER TABLE `mdl_mindmap`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_mind_cou_ix` (`course`),
  ADD KEY `mdl_mind_use_ix` (`userid`);

--
-- Indexes for table `mdl_mnet_application`
--
ALTER TABLE `mdl_mnet_application`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_mnet_enrol_assignments`
--
ALTER TABLE `mdl_mnet_enrol_assignments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_mnetenroassi_hoscou_ix` (`hostid`,`courseid`),
  ADD KEY `mdl_mnetenroassi_use_ix` (`userid`);

--
-- Indexes for table `mdl_mnet_enrol_course`
--
ALTER TABLE `mdl_mnet_enrol_course`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_mnetenrocour_hosrem_uix` (`hostid`,`remoteid`);

--
-- Indexes for table `mdl_mnet_host`
--
ALTER TABLE `mdl_mnet_host`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_mnethost_app_ix` (`applicationid`);

--
-- Indexes for table `mdl_mnet_host2service`
--
ALTER TABLE `mdl_mnet_host2service`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_mnethost_hosser_uix` (`hostid`,`serviceid`);

--
-- Indexes for table `mdl_mnet_log`
--
ALTER TABLE `mdl_mnet_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_mnetlog_hosusecou_ix` (`hostid`,`userid`,`course`);

--
-- Indexes for table `mdl_mnet_rpc`
--
ALTER TABLE `mdl_mnet_rpc`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_mnetrpc_enaxml_ix` (`enabled`,`xmlrpc_path`);

--
-- Indexes for table `mdl_mnet_service`
--
ALTER TABLE `mdl_mnet_service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_mnet_service2rpc`
--
ALTER TABLE `mdl_mnet_service2rpc`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_mnetserv_rpcser_uix` (`rpcid`,`serviceid`);

--
-- Indexes for table `mdl_mnet_session`
--
ALTER TABLE `mdl_mnet_session`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_mnetsess_tok_uix` (`token`);

--
-- Indexes for table `mdl_mnet_sso_access_control`
--
ALTER TABLE `mdl_mnet_sso_access_control`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_mnetssoaccecont_mneuse_uix` (`mnet_host_id`,`username`);

--
-- Indexes for table `mdl_modules`
--
ALTER TABLE `mdl_modules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_modu_nam_ix` (`name`);

--
-- Indexes for table `mdl_mplayer`
--
ALTER TABLE `mdl_mplayer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_mpla_cou_ix` (`course`);

--
-- Indexes for table `mdl_navigator_feed`
--
ALTER TABLE `mdl_navigator_feed`
  ADD UNIQUE KEY `mdl_user_mneuse_uix` (`portalid`),
  ADD KEY `mdl_user_fir_ix` (`firstname`),
  ADD KEY `mdl_user_las_ix` (`lastname`),
  ADD KEY `mdl_user_cou_ix` (`emp_work_country`),
  ADD KEY `mdl_user_ema_ix` (`email`),
  ADD KEY `mdl_user_aut_ix` (`auth`),
  ADD KEY `Index_username` (`portalid`),
  ADD KEY `mdl_user_grade_ix` (`grade`),
  ADD KEY `mdl_user_BU_ix` (`BU`),
  ADD KEY `mdl_user_manager_portalid_ix` (`manager_portalid`),
  ADD KEY `mdl_user_costcenter_ix` (`costcenter`);

--
-- Indexes for table `mdl_openmeetings`
--
ALTER TABLE `mdl_openmeetings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_open_cou_ix` (`course`);

--
-- Indexes for table `mdl_post`
--
ALTER TABLE `mdl_post`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_post_iduse_uix` (`id`,`userid`),
  ADD KEY `mdl_post_las_ix` (`lastmodified`),
  ADD KEY `mdl_post_mod_ix` (`module`),
  ADD KEY `mdl_post_sub_ix` (`subject`),
  ADD KEY `mdl_post_use_ix` (`usermodified`);

--
-- Indexes for table `mdl_protrack_upload_temp`
--
ALTER TABLE `mdl_protrack_upload_temp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_question`
--
ALTER TABLE `mdl_question`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_ques_cat_ix` (`category`),
  ADD KEY `mdl_ques_par_ix` (`parent`),
  ADD KEY `mdl_ques_cre_ix` (`createdby`),
  ADD KEY `mdl_ques_mod_ix` (`modifiedby`);

--
-- Indexes for table `mdl_questionnaire`
--
ALTER TABLE `mdl_questionnaire`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_ques_sid_ix` (`sid`);

--
-- Indexes for table `mdl_questionnaire_attempts`
--
ALTER TABLE `mdl_questionnaire_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_questionnaire_question`
--
ALTER TABLE `mdl_questionnaire_question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_questionnaire_question_type`
--
ALTER TABLE `mdl_questionnaire_question_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_quesquestype_typ_uix` (`typeid`);

--
-- Indexes for table `mdl_questionnaire_quest_choice`
--
ALTER TABLE `mdl_questionnaire_quest_choice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_questionnaire_response`
--
ALTER TABLE `mdl_questionnaire_response`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_questionnaire_response_bool`
--
ALTER TABLE `mdl_questionnaire_response_bool`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_quesrespbool_resque_ix` (`response_id`,`question_id`);

--
-- Indexes for table `mdl_questionnaire_response_date`
--
ALTER TABLE `mdl_questionnaire_response_date`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_quesrespdate_resque_ix` (`response_id`,`question_id`);

--
-- Indexes for table `mdl_questionnaire_response_other`
--
ALTER TABLE `mdl_questionnaire_response_other`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_quesrespothe_resquecho_ix` (`response_id`,`question_id`,`choice_id`);

--
-- Indexes for table `mdl_questionnaire_response_rank`
--
ALTER TABLE `mdl_questionnaire_response_rank`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_quesresprank_resquecho_ix` (`response_id`,`question_id`,`choice_id`);

--
-- Indexes for table `mdl_questionnaire_response_text`
--
ALTER TABLE `mdl_questionnaire_response_text`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_quesresptext_resque_ix` (`response_id`,`question_id`);

--
-- Indexes for table `mdl_questionnaire_resp_multiple`
--
ALTER TABLE `mdl_questionnaire_resp_multiple`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_quesrespmult_resquecho_ix` (`response_id`,`question_id`,`choice_id`);

--
-- Indexes for table `mdl_questionnaire_resp_single`
--
ALTER TABLE `mdl_questionnaire_resp_single`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_quesrespsing_resque_ix` (`response_id`,`question_id`);

--
-- Indexes for table `mdl_questionnaire_survey`
--
ALTER TABLE `mdl_questionnaire_survey`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_quessurv_nam_ix` (`name`),
  ADD KEY `mdl_quessurv_own_ix` (`owner`);

--
-- Indexes for table `mdl_question_answers`
--
ALTER TABLE `mdl_question_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_quesansw_que_ix` (`question`);

--
-- Indexes for table `mdl_question_attempts`
--
ALTER TABLE `mdl_question_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_question_calculated`
--
ALTER TABLE `mdl_question_calculated`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_quescalc_ans_ix` (`answer`),
  ADD KEY `mdl_quescalc_que_ix` (`question`);

--
-- Indexes for table `mdl_question_categories`
--
ALTER TABLE `mdl_question_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_quescate_con_ix` (`contextid`),
  ADD KEY `mdl_quescate_par_ix` (`parent`);

--
-- Indexes for table `mdl_question_datasets`
--
ALTER TABLE `mdl_question_datasets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_quesdata_quedat_ix` (`question`,`datasetdefinition`),
  ADD KEY `mdl_quesdata_que_ix` (`question`),
  ADD KEY `mdl_quesdata_dat_ix` (`datasetdefinition`);

--
-- Indexes for table `mdl_question_dataset_definitions`
--
ALTER TABLE `mdl_question_dataset_definitions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_quesdatadefi_cat_ix` (`category`);

--
-- Indexes for table `mdl_question_dataset_items`
--
ALTER TABLE `mdl_question_dataset_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_quesdataitem_def_ix` (`definition`);

--
-- Indexes for table `mdl_question_match`
--
ALTER TABLE `mdl_question_match`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_quesmatc_que_ix` (`question`);

--
-- Indexes for table `mdl_question_match_sub`
--
ALTER TABLE `mdl_question_match_sub`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_quesmatcsub_que_ix` (`question`);

--
-- Indexes for table `mdl_question_multianswer`
--
ALTER TABLE `mdl_question_multianswer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_quesmult_que_ix` (`question`);

--
-- Indexes for table `mdl_question_multichoice`
--
ALTER TABLE `mdl_question_multichoice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_quesmult_que2_ix` (`question`);

--
-- Indexes for table `mdl_question_numerical`
--
ALTER TABLE `mdl_question_numerical`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_quesnume_ans_ix` (`answer`),
  ADD KEY `mdl_quesnume_que_ix` (`question`);

--
-- Indexes for table `mdl_question_numerical_units`
--
ALTER TABLE `mdl_question_numerical_units`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_quesnumeunit_queuni_uix` (`question`,`unit`),
  ADD KEY `mdl_quesnumeunit_que_ix` (`question`);

--
-- Indexes for table `mdl_question_randomsamatch`
--
ALTER TABLE `mdl_question_randomsamatch`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_quesrand_que_ix` (`question`);

--
-- Indexes for table `mdl_question_sessions`
--
ALTER TABLE `mdl_question_sessions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_quessess_attque_uix` (`attemptid`,`questionid`),
  ADD KEY `mdl_quessess_att_ix` (`attemptid`),
  ADD KEY `mdl_quessess_que_ix` (`questionid`),
  ADD KEY `mdl_quessess_new_ix` (`newest`),
  ADD KEY `mdl_quessess_new2_ix` (`newgraded`);

--
-- Indexes for table `mdl_question_shortanswer`
--
ALTER TABLE `mdl_question_shortanswer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_quesshor_que_ix` (`question`);

--
-- Indexes for table `mdl_question_states`
--
ALTER TABLE `mdl_question_states`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_quesstat_att_ix` (`attempt`),
  ADD KEY `mdl_quesstat_que_ix` (`question`);

--
-- Indexes for table `mdl_question_truefalse`
--
ALTER TABLE `mdl_question_truefalse`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_questrue_que_ix` (`question`);

--
-- Indexes for table `mdl_quiz`
--
ALTER TABLE `mdl_quiz`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_quiz_cou_ix` (`course`);

--
-- Indexes for table `mdl_quiz_attempts`
--
ALTER TABLE `mdl_quiz_attempts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_quizatte_uni_uix` (`uniqueid`),
  ADD KEY `mdl_quizatte_use_ix` (`userid`),
  ADD KEY `mdl_quizatte_qui_ix` (`quiz`);

--
-- Indexes for table `mdl_quiz_feedback`
--
ALTER TABLE `mdl_quiz_feedback`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_quizfeed_qui_ix` (`quizid`);

--
-- Indexes for table `mdl_quiz_grades`
--
ALTER TABLE `mdl_quiz_grades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_quizgrad_use_ix` (`userid`),
  ADD KEY `mdl_quizgrad_qui_ix` (`quiz`);

--
-- Indexes for table `mdl_quiz_question_instances`
--
ALTER TABLE `mdl_quiz_question_instances`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_quizquesinst_qui_ix` (`quiz`),
  ADD KEY `mdl_quizquesinst_que_ix` (`question`);

--
-- Indexes for table `mdl_quiz_question_versions`
--
ALTER TABLE `mdl_quiz_question_versions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_quizquesvers_qui_ix` (`quiz`),
  ADD KEY `mdl_quizquesvers_old_ix` (`oldquestion`),
  ADD KEY `mdl_quizquesvers_new_ix` (`newquestion`),
  ADD KEY `mdl_quizquesvers_ori_ix` (`originalquestion`);

--
-- Indexes for table `mdl_realtimeattendance`
--
ALTER TABLE `mdl_realtimeattendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_realtimeattendance_answer`
--
ALTER TABLE `mdl_realtimeattendance_answer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_realansw_que_ix` (`questionid`);

--
-- Indexes for table `mdl_realtimeattendance_question`
--
ALTER TABLE `mdl_realtimeattendance_question`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_realques_quique_ix` (`quizid`,`questionnum`);

--
-- Indexes for table `mdl_realtimeattendance_session`
--
ALTER TABLE `mdl_realtimeattendance_session`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_realsess_qui_ix` (`quizid`);

--
-- Indexes for table `mdl_realtimeattendance_submitted`
--
ALTER TABLE `mdl_realtimeattendance_submitted`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_realsubm_anssesque_ix` (`answerid`,`sessionid`,`questionid`),
  ADD KEY `mdl_realsubm_use_ix` (`userid`);

--
-- Indexes for table `mdl_resource`
--
ALTER TABLE `mdl_resource`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_reso_cou_ix` (`course`);

--
-- Indexes for table `mdl_role`
--
ALTER TABLE `mdl_role`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_role_sor_uix` (`sortorder`);

--
-- Indexes for table `mdl_role_allow_assign`
--
ALTER TABLE `mdl_role_allow_assign`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_rolealloassi_rolall_uix` (`roleid`,`allowassign`),
  ADD KEY `mdl_rolealloassi_rol_ix` (`roleid`),
  ADD KEY `mdl_rolealloassi_all_ix` (`allowassign`);

--
-- Indexes for table `mdl_role_allow_override`
--
ALTER TABLE `mdl_role_allow_override`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_rolealloover_rolall_uix` (`roleid`,`allowoverride`),
  ADD KEY `mdl_rolealloover_rol_ix` (`roleid`),
  ADD KEY `mdl_rolealloover_all_ix` (`allowoverride`);

--
-- Indexes for table `mdl_role_assignments`
--
ALTER TABLE `mdl_role_assignments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_roleassi_conroluse_uix` (`contextid`,`roleid`,`userid`),
  ADD KEY `mdl_roleassi_sor_ix` (`sortorder`),
  ADD KEY `mdl_roleassi_rol_ix` (`roleid`),
  ADD KEY `mdl_roleassi_con_ix` (`contextid`),
  ADD KEY `mdl_roleassi_use_ix` (`userid`);

--
-- Indexes for table `mdl_role_capabilities`
--
ALTER TABLE `mdl_role_capabilities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_rolecapa_rolconcap_uix` (`roleid`,`contextid`,`capability`),
  ADD KEY `mdl_rolecapa_rol_ix` (`roleid`),
  ADD KEY `mdl_rolecapa_con_ix` (`contextid`),
  ADD KEY `mdl_rolecapa_mod_ix` (`modifierid`),
  ADD KEY `mdl_rolecapa_cap_ix` (`capability`);

--
-- Indexes for table `mdl_role_names`
--
ALTER TABLE `mdl_role_names`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_rolename_rolcon_uix` (`roleid`,`contextid`),
  ADD KEY `mdl_rolename_rol_ix` (`roleid`),
  ADD KEY `mdl_rolename_con_ix` (`contextid`);

--
-- Indexes for table `mdl_role_sortorder`
--
ALTER TABLE `mdl_role_sortorder`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_rolesort_userolcon_uix` (`userid`,`roleid`,`contextid`),
  ADD KEY `mdl_rolesort_use_ix` (`userid`),
  ADD KEY `mdl_rolesort_rol_ix` (`roleid`),
  ADD KEY `mdl_rolesort_con_ix` (`contextid`);

--
-- Indexes for table `mdl_scale`
--
ALTER TABLE `mdl_scale`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_scal_cou_ix` (`courseid`);

--
-- Indexes for table `mdl_scale_history`
--
ALTER TABLE `mdl_scale_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_scalhist_act_ix` (`action`),
  ADD KEY `mdl_scalhist_old_ix` (`oldid`),
  ADD KEY `mdl_scalhist_cou_ix` (`courseid`),
  ADD KEY `mdl_scalhist_log_ix` (`loggeduser`);

--
-- Indexes for table `mdl_scheduler`
--
ALTER TABLE `mdl_scheduler`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_scheduler_appointment`
--
ALTER TABLE `mdl_scheduler_appointment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_scheduler_slots`
--
ALTER TABLE `mdl_scheduler_slots`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_scorm`
--
ALTER TABLE `mdl_scorm`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_scor_cou_ix` (`course`);

--
-- Indexes for table `mdl_scorm_scoes`
--
ALTER TABLE `mdl_scorm_scoes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_scorscoe_sco_ix` (`scorm`);

--
-- Indexes for table `mdl_scorm_scoes_data`
--
ALTER TABLE `mdl_scorm_scoes_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_scorscoedata_sco_ix` (`scoid`);

--
-- Indexes for table `mdl_scorm_scoes_track`
--
ALTER TABLE `mdl_scorm_scoes_track`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_scorscoetrac_usescosco_uix` (`userid`,`scormid`,`scoid`,`attempt`,`element`),
  ADD KEY `mdl_scorscoetrac_use_ix` (`userid`),
  ADD KEY `mdl_scorscoetrac_ele_ix` (`element`),
  ADD KEY `mdl_scorscoetrac_sco_ix` (`scormid`),
  ADD KEY `mdl_scorscoetrac_sco2_ix` (`scoid`),
  ADD KEY `index_time` (`timemodified`);

--
-- Indexes for table `mdl_scorm_seq_mapinfo`
--
ALTER TABLE `mdl_scorm_seq_mapinfo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_scorseqmapi_scoidobj_uix` (`scoid`,`id`,`objectiveid`),
  ADD KEY `mdl_scorseqmapi_sco_ix` (`scoid`),
  ADD KEY `mdl_scorseqmapi_obj_ix` (`objectiveid`);

--
-- Indexes for table `mdl_skillsoft`
--
ALTER TABLE `mdl_skillsoft`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_skil_cou_ix` (`course`);

--
-- Indexes for table `mdl_skillsoft_au_track`
--
ALTER TABLE `mdl_skillsoft_au_track`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_skilautrac_useskiattel_uix` (`userid`,`skillsoftid`,`attempt`,`element`),
  ADD KEY `mdl_skilautrac_use_ix` (`userid`),
  ADD KEY `mdl_skilautrac_ele_ix` (`element`);

--
-- Indexes for table `mdl_skillsoft_session_track`
--
ALTER TABLE `mdl_skillsoft_session_track`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_skilsesstrac_sesuseski_uix` (`sessionid`,`userid`,`skillsoftid`),
  ADD KEY `mdl_skilsesstrac_use_ix` (`userid`);

--
-- Indexes for table `mdl_subcourse`
--
ALTER TABLE `mdl_subcourse`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_subc_cou_ix` (`course`);

--
-- Indexes for table `mdl_survey`
--
ALTER TABLE `mdl_survey`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_surv_cou_ix` (`course`);

--
-- Indexes for table `mdl_survey_analysis`
--
ALTER TABLE `mdl_survey_analysis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_survanal_use_ix` (`userid`),
  ADD KEY `mdl_survanal_sur_ix` (`survey`);

--
-- Indexes for table `mdl_survey_questions`
--
ALTER TABLE `mdl_survey_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_swf`
--
ALTER TABLE `mdl_swf`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_swf_cou_ix` (`course`);

--
-- Indexes for table `mdl_tag`
--
ALTER TABLE `mdl_tag`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_tag_nam_uix` (`name`),
  ADD KEY `mdl_tag_use_ix` (`userid`);

--
-- Indexes for table `mdl_tracker`
--
ALTER TABLE `mdl_tracker`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_tracker_element`
--
ALTER TABLE `mdl_tracker_element`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_tracker_issue`
--
ALTER TABLE `mdl_tracker_issue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_user`
--
ALTER TABLE `mdl_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdl_user_mneuse_uix` (`mnethostid`,`username`),
  ADD KEY `mdl_user_del_ix` (`deleted`),
  ADD KEY `mdl_user_con_ix` (`confirmed`),
  ADD KEY `mdl_user_fir_ix` (`firstname`),
  ADD KEY `mdl_user_las_ix` (`lastname`),
  ADD KEY `mdl_user_cit_ix` (`city`),
  ADD KEY `mdl_user_cou_ix` (`country`),
  ADD KEY `mdl_user_las2_ix` (`lastaccess`),
  ADD KEY `mdl_user_ema_ix` (`email`),
  ADD KEY `mdl_user_aut_ix` (`auth`),
  ADD KEY `mdl_user_idn_ix` (`idnumber`),
  ADD KEY `Index_username` (`username`),
  ADD KEY `mdl_user_grade_ix` (`grade`),
  ADD KEY `mdl_user_BU_ix` (`BU`),
  ADD KEY `mdl_user_RU_ix` (`RU`),
  ADD KEY `mdl_user_manager_portalid_ix` (`manager_portalid`),
  ADD KEY `mdl_user_source_ix` (`source`),
  ADD KEY `mdl_user_costcenter_ix` (`costcenter`);

--
-- Indexes for table `mdl_user_info_category`
--
ALTER TABLE `mdl_user_info_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_wiki`
--
ALTER TABLE `mdl_wiki`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mdl_wiki_cou_ix` (`course`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adodb_logsql`
--
ALTER TABLE `adodb_logsql`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_assignment`
--
ALTER TABLE `mdl_assignment`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=219;
--
-- AUTO_INCREMENT for table `mdl_assignment_submissions`
--
ALTER TABLE `mdl_assignment_submissions`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4757;
--
-- AUTO_INCREMENT for table `mdl_attendance_log`
--
ALTER TABLE `mdl_attendance_log`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_attendance_sessions`
--
ALTER TABLE `mdl_attendance_sessions`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_attendance_statuses`
--
ALTER TABLE `mdl_attendance_statuses`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `mdl_attforblock`
--
ALTER TABLE `mdl_attforblock`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_backup_config`
--
ALTER TABLE `mdl_backup_config`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `mdl_backup_courses`
--
ALTER TABLE `mdl_backup_courses`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_backup_files`
--
ALTER TABLE `mdl_backup_files`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_backup_ids`
--
ALTER TABLE `mdl_backup_ids`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_backup_log`
--
ALTER TABLE `mdl_backup_log`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_block`
--
ALTER TABLE `mdl_block`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT for table `mdl_block_fn_my_menu`
--
ALTER TABLE `mdl_block_fn_my_menu`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_block_fn_my_menu_group_settings`
--
ALTER TABLE `mdl_block_fn_my_menu_group_settings`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_block_instance`
--
ALTER TABLE `mdl_block_instance`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53499;
--
-- AUTO_INCREMENT for table `mdl_block_moodle_notifications_courses`
--
ALTER TABLE `mdl_block_moodle_notifications_courses`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mdl_block_moodle_notifications_log`
--
ALTER TABLE `mdl_block_moodle_notifications_log`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `mdl_block_moodle_notifications_users`
--
ALTER TABLE `mdl_block_moodle_notifications_users`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `mdl_block_my_courses`
--
ALTER TABLE `mdl_block_my_courses`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2299;
--
-- AUTO_INCREMENT for table `mdl_block_pinned`
--
ALTER TABLE `mdl_block_pinned`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `mdl_block_rate_course`
--
ALTER TABLE `mdl_block_rate_course`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3166;
--
-- AUTO_INCREMENT for table `mdl_block_rss_client`
--
ALTER TABLE `mdl_block_rss_client`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mdl_block_search_documents`
--
ALTER TABLE `mdl_block_search_documents`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_booking`
--
ALTER TABLE `mdl_booking`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `mdl_booking_answers`
--
ALTER TABLE `mdl_booking_answers`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=566;
--
-- AUTO_INCREMENT for table `mdl_booking_options`
--
ALTER TABLE `mdl_booking_options`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3030;
--
-- AUTO_INCREMENT for table `mdl_bu_description`
--
ALTER TABLE `mdl_bu_description`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;
--
-- AUTO_INCREMENT for table `mdl_cache_filters`
--
ALTER TABLE `mdl_cache_filters`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_cache_flags`
--
ALTER TABLE `mdl_cache_flags`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19658;
--
-- AUTO_INCREMENT for table `mdl_cache_text`
--
ALTER TABLE `mdl_cache_text`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=482437;
--
-- AUTO_INCREMENT for table `mdl_capabilities`
--
ALTER TABLE `mdl_capabilities`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=383;
--
-- AUTO_INCREMENT for table `mdl_certificate`
--
ALTER TABLE `mdl_certificate`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `mdl_certificate_issues`
--
ALTER TABLE `mdl_certificate_issues`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3250;
--
-- AUTO_INCREMENT for table `mdl_certificate_linked_modules`
--
ALTER TABLE `mdl_certificate_linked_modules`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `mdl_chat`
--
ALTER TABLE `mdl_chat`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mdl_chat_messages`
--
ALTER TABLE `mdl_chat_messages`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `mdl_chat_users`
--
ALTER TABLE `mdl_chat_users`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_choice`
--
ALTER TABLE `mdl_choice`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `mdl_choice_answers`
--
ALTER TABLE `mdl_choice_answers`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT for table `mdl_choice_options`
--
ALTER TABLE `mdl_choice_options`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `mdl_classroom`
--
ALTER TABLE `mdl_classroom`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2416;
--
-- AUTO_INCREMENT for table `mdl_classroom_detailsmaster`
--
ALTER TABLE `mdl_classroom_detailsmaster`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=502;
--
-- AUTO_INCREMENT for table `mdl_classroom_sessions`
--
ALTER TABLE `mdl_classroom_sessions`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103966;
--
-- AUTO_INCREMENT for table `mdl_classroom_sessions_dates`
--
ALTER TABLE `mdl_classroom_sessions_dates`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=152576;
--
-- AUTO_INCREMENT for table `mdl_classroom_sessions_external`
--
ALTER TABLE `mdl_classroom_sessions_external`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13605;
--
-- AUTO_INCREMENT for table `mdl_classroom_submissions`
--
ALTER TABLE `mdl_classroom_submissions`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=593020;
--
-- AUTO_INCREMENT for table `mdl_classroom_trainners`
--
ALTER TABLE `mdl_classroom_trainners`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82062;
--
-- AUTO_INCREMENT for table `mdl_config`
--
ALTER TABLE `mdl_config`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=591;
--
-- AUTO_INCREMENT for table `mdl_config_plugins`
--
ALTER TABLE `mdl_config_plugins`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;
--
-- AUTO_INCREMENT for table `mdl_context`
--
ALTER TABLE `mdl_context`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135738;
--
-- AUTO_INCREMENT for table `mdl_course`
--
ALTER TABLE `mdl_course`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9186;
--
-- AUTO_INCREMENT for table `mdl_course_allowed_modules`
--
ALTER TABLE `mdl_course_allowed_modules`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_course_allowed_modules_temp`
--
ALTER TABLE `mdl_course_allowed_modules_temp`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_course_categories`
--
ALTER TABLE `mdl_course_categories`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1001956;
--
-- AUTO_INCREMENT for table `mdl_course_categories_temp`
--
ALTER TABLE `mdl_course_categories_temp`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1001809;
--
-- AUTO_INCREMENT for table `mdl_course_display`
--
ALTER TABLE `mdl_course_display`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=756318;
--
-- AUTO_INCREMENT for table `mdl_course_meta`
--
ALTER TABLE `mdl_course_meta`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1153;
--
-- AUTO_INCREMENT for table `mdl_course_modules`
--
ALTER TABLE `mdl_course_modules`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26574;
--
-- AUTO_INCREMENT for table `mdl_course_module_locks`
--
ALTER TABLE `mdl_course_module_locks`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=591;
--
-- AUTO_INCREMENT for table `mdl_course_request`
--
ALTER TABLE `mdl_course_request`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `mdl_course_sections`
--
ALTER TABLE `mdl_course_sections`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19751;
--
-- AUTO_INCREMENT for table `mdl_course_temp`
--
ALTER TABLE `mdl_course_temp`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8283;
--
-- AUTO_INCREMENT for table `mdl_cpd`
--
ALTER TABLE `mdl_cpd`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_cpd_activity_type`
--
ALTER TABLE `mdl_cpd_activity_type`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `mdl_cpd_status`
--
ALTER TABLE `mdl_cpd_status`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `mdl_cpd_year`
--
ALTER TABLE `mdl_cpd_year`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `mdl_csr`
--
ALTER TABLE `mdl_csr`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mdl_csr_notice`
--
ALTER TABLE `mdl_csr_notice`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mdl_csr_notice_data`
--
ALTER TABLE `mdl_csr_notice_data`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mdl_csr_sessions`
--
ALTER TABLE `mdl_csr_sessions`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=777;
--
-- AUTO_INCREMENT for table `mdl_csr_sessions_dates`
--
ALTER TABLE `mdl_csr_sessions_dates`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1344;
--
-- AUTO_INCREMENT for table `mdl_csr_session_data`
--
ALTER TABLE `mdl_csr_session_data`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=316;
--
-- AUTO_INCREMENT for table `mdl_csr_session_field`
--
ALTER TABLE `mdl_csr_session_field`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mdl_csr_session_roles`
--
ALTER TABLE `mdl_csr_session_roles`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mdl_csr_signups`
--
ALTER TABLE `mdl_csr_signups`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8074;
--
-- AUTO_INCREMENT for table `mdl_csr_signups_status`
--
ALTER TABLE `mdl_csr_signups_status`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9626;
--
-- AUTO_INCREMENT for table `mdl_csr_user_signups`
--
ALTER TABLE `mdl_csr_user_signups`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=379;
--
-- AUTO_INCREMENT for table `mdl_data`
--
ALTER TABLE `mdl_data`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `mdl_data_comments`
--
ALTER TABLE `mdl_data_comments`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_data_content`
--
ALTER TABLE `mdl_data_content`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1876;
--
-- AUTO_INCREMENT for table `mdl_data_fields`
--
ALTER TABLE `mdl_data_fields`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `mdl_data_ratings`
--
ALTER TABLE `mdl_data_ratings`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_data_records`
--
ALTER TABLE `mdl_data_records`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=270;
--
-- AUTO_INCREMENT for table `mdl_elearning_question`
--
ALTER TABLE `mdl_elearning_question`
  MODIFY `Id` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `mdl_elearning_value`
--
ALTER TABLE `mdl_elearning_value`
  MODIFY `Id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_enrol_approvalworkflow`
--
ALTER TABLE `mdl_enrol_approvalworkflow`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_enrol_authorize`
--
ALTER TABLE `mdl_enrol_authorize`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_enrol_authorize_refunds`
--
ALTER TABLE `mdl_enrol_authorize_refunds`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_enrol_paypal`
--
ALTER TABLE `mdl_enrol_paypal`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_enrol_skillsoft`
--
ALTER TABLE `mdl_enrol_skillsoft`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13396;
--
-- AUTO_INCREMENT for table `mdl_enrol_userregister`
--
ALTER TABLE `mdl_enrol_userregister`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1698;
--
-- AUTO_INCREMENT for table `mdl_event`
--
ALTER TABLE `mdl_event`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111705;
--
-- AUTO_INCREMENT for table `mdl_events_handlers`
--
ALTER TABLE `mdl_events_handlers`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mdl_events_queue`
--
ALTER TABLE `mdl_events_queue`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_events_queue_handlers`
--
ALTER TABLE `mdl_events_queue_handlers`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_feedback`
--
ALTER TABLE `mdl_feedback`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `mdl_feedback_completed`
--
ALTER TABLE `mdl_feedback_completed`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15957;
--
-- AUTO_INCREMENT for table `mdl_feedback_completedtmp`
--
ALTER TABLE `mdl_feedback_completedtmp`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97807;
--
-- AUTO_INCREMENT for table `mdl_feedback_ext_employee_response`
--
ALTER TABLE `mdl_feedback_ext_employee_response`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_feedback_ext_items`
--
ALTER TABLE `mdl_feedback_ext_items`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `mdl_feedback_ext_manager_response`
--
ALTER TABLE `mdl_feedback_ext_manager_response`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_feedback_ext_question`
--
ALTER TABLE `mdl_feedback_ext_question`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `mdl_feedback_item`
--
ALTER TABLE `mdl_feedback_item`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1634;
--
-- AUTO_INCREMENT for table `mdl_feedback_sitecourse_map`
--
ALTER TABLE `mdl_feedback_sitecourse_map`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mdl_feedback_template`
--
ALTER TABLE `mdl_feedback_template`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `mdl_feedback_tracking`
--
ALTER TABLE `mdl_feedback_tracking`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102630;
--
-- AUTO_INCREMENT for table `mdl_feedback_user`
--
ALTER TABLE `mdl_feedback_user`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101138;
--
-- AUTO_INCREMENT for table `mdl_feedback_value`
--
ALTER TABLE `mdl_feedback_value`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44063759;
--
-- AUTO_INCREMENT for table `mdl_feedback_valuetmp`
--
ALTER TABLE `mdl_feedback_valuetmp`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40892716;
--
-- AUTO_INCREMENT for table `mdl_forum`
--
ALTER TABLE `mdl_forum`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;
--
-- AUTO_INCREMENT for table `mdl_forum_discussions`
--
ALTER TABLE `mdl_forum_discussions`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=191;
--
-- AUTO_INCREMENT for table `mdl_forum_posts`
--
ALTER TABLE `mdl_forum_posts`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=514;
--
-- AUTO_INCREMENT for table `mdl_forum_queue`
--
ALTER TABLE `mdl_forum_queue`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_forum_ratings`
--
ALTER TABLE `mdl_forum_ratings`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_forum_read`
--
ALTER TABLE `mdl_forum_read`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_forum_subscriptions`
--
ALTER TABLE `mdl_forum_subscriptions`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=501;
--
-- AUTO_INCREMENT for table `mdl_forum_track_prefs`
--
ALTER TABLE `mdl_forum_track_prefs`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mdl_game`
--
ALTER TABLE `mdl_game`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `mdl_game_attempts`
--
ALTER TABLE `mdl_game_attempts`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=605;
--
-- AUTO_INCREMENT for table `mdl_game_badge`
--
ALTER TABLE `mdl_game_badge`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `mdl_game_badge_track`
--
ALTER TABLE `mdl_game_badge_track`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92509;
--
-- AUTO_INCREMENT for table `mdl_game_bookquiz_chapters`
--
ALTER TABLE `mdl_game_bookquiz_chapters`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_game_bookquiz_questions`
--
ALTER TABLE `mdl_game_bookquiz_questions`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_game_course`
--
ALTER TABLE `mdl_game_course`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_game_course_inputs`
--
ALTER TABLE `mdl_game_course_inputs`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_game_grades`
--
ALTER TABLE `mdl_game_grades`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=277;
--
-- AUTO_INCREMENT for table `mdl_game_point`
--
ALTER TABLE `mdl_game_point`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1251568;
--
-- AUTO_INCREMENT for table `mdl_game_queries`
--
ALTER TABLE `mdl_game_queries`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1115;
--
-- AUTO_INCREMENT for table `mdl_game_repetitions`
--
ALTER TABLE `mdl_game_repetitions`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=670;
--
-- AUTO_INCREMENT for table `mdl_game_snakes_database`
--
ALTER TABLE `mdl_game_snakes_database`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mdl_game_sudoku_database`
--
ALTER TABLE `mdl_game_sudoku_database`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1681;
--
-- AUTO_INCREMENT for table `mdl_glossary`
--
ALTER TABLE `mdl_glossary`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mdl_glossary_alias`
--
ALTER TABLE `mdl_glossary_alias`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;
--
-- AUTO_INCREMENT for table `mdl_glossary_categories`
--
ALTER TABLE `mdl_glossary_categories`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_glossary_comments`
--
ALTER TABLE `mdl_glossary_comments`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_glossary_entries`
--
ALTER TABLE `mdl_glossary_entries`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `mdl_glossary_entries_categories`
--
ALTER TABLE `mdl_glossary_entries_categories`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_glossary_formats`
--
ALTER TABLE `mdl_glossary_formats`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `mdl_glossary_ratings`
--
ALTER TABLE `mdl_glossary_ratings`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_grade_categories`
--
ALTER TABLE `mdl_grade_categories`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10016;
--
-- AUTO_INCREMENT for table `mdl_grade_categories_history`
--
ALTER TABLE `mdl_grade_categories_history`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29700;
--
-- AUTO_INCREMENT for table `mdl_grade_grades`
--
ALTER TABLE `mdl_grade_grades`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6623431;
--
-- AUTO_INCREMENT for table `mdl_grade_grades_history`
--
ALTER TABLE `mdl_grade_grades_history`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13008622;
--
-- AUTO_INCREMENT for table `mdl_grade_import_newitem`
--
ALTER TABLE `mdl_grade_import_newitem`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `mdl_grade_import_values`
--
ALTER TABLE `mdl_grade_import_values`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25049;
--
-- AUTO_INCREMENT for table `mdl_grade_items`
--
ALTER TABLE `mdl_grade_items`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34317;
--
-- AUTO_INCREMENT for table `mdl_grade_items_history`
--
ALTER TABLE `mdl_grade_items_history`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131967;
--
-- AUTO_INCREMENT for table `mdl_grade_letters`
--
ALTER TABLE `mdl_grade_letters`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=512;
--
-- AUTO_INCREMENT for table `mdl_grade_outcomes`
--
ALTER TABLE `mdl_grade_outcomes`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_grade_outcomes_courses`
--
ALTER TABLE `mdl_grade_outcomes_courses`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_grade_outcomes_history`
--
ALTER TABLE `mdl_grade_outcomes_history`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_grade_settings`
--
ALTER TABLE `mdl_grade_settings`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4608;
--
-- AUTO_INCREMENT for table `mdl_grade_training`
--
ALTER TABLE `mdl_grade_training`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `mdl_groupings`
--
ALTER TABLE `mdl_groupings`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=961;
--
-- AUTO_INCREMENT for table `mdl_groupings_groups`
--
ALTER TABLE `mdl_groupings_groups`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1098;
--
-- AUTO_INCREMENT for table `mdl_groups`
--
ALTER TABLE `mdl_groups`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2325;
--
-- AUTO_INCREMENT for table `mdl_groups_members`
--
ALTER TABLE `mdl_groups_members`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=433439;
--
-- AUTO_INCREMENT for table `mdl_journal`
--
ALTER TABLE `mdl_journal`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_journal_entries`
--
ALTER TABLE `mdl_journal_entries`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_klci_grade`
--
ALTER TABLE `mdl_klci_grade`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `mdl_klci_level`
--
ALTER TABLE `mdl_klci_level`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `mdl_klci_practice`
--
ALTER TABLE `mdl_klci_practice`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `mdl_klci_stream`
--
ALTER TABLE `mdl_klci_stream`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `mdl_label`
--
ALTER TABLE `mdl_label`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2528;
--
-- AUTO_INCREMENT for table `mdl_lams`
--
ALTER TABLE `mdl_lams`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_lesson`
--
ALTER TABLE `mdl_lesson`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mdl_lesson_answers`
--
ALTER TABLE `mdl_lesson_answers`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_lesson_attempts`
--
ALTER TABLE `mdl_lesson_attempts`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_lesson_branch`
--
ALTER TABLE `mdl_lesson_branch`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_lesson_default`
--
ALTER TABLE `mdl_lesson_default`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_lesson_grades`
--
ALTER TABLE `mdl_lesson_grades`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_lesson_high_scores`
--
ALTER TABLE `mdl_lesson_high_scores`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_lesson_pages`
--
ALTER TABLE `mdl_lesson_pages`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_lesson_timer`
--
ALTER TABLE `mdl_lesson_timer`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_log`
--
ALTER TABLE `mdl_log`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1931341;
--
-- AUTO_INCREMENT for table `mdl_log_display`
--
ALTER TABLE `mdl_log_display`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=238;
--
-- AUTO_INCREMENT for table `mdl_message`
--
ALTER TABLE `mdl_message`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22651;
--
-- AUTO_INCREMENT for table `mdl_message_contacts`
--
ALTER TABLE `mdl_message_contacts`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3314;
--
-- AUTO_INCREMENT for table `mdl_message_read`
--
ALTER TABLE `mdl_message_read`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23058;
--
-- AUTO_INCREMENT for table `mdl_mindmap`
--
ALTER TABLE `mdl_mindmap`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_mnet_application`
--
ALTER TABLE `mdl_mnet_application`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mdl_mnet_enrol_assignments`
--
ALTER TABLE `mdl_mnet_enrol_assignments`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_mnet_enrol_course`
--
ALTER TABLE `mdl_mnet_enrol_course`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_mnet_host`
--
ALTER TABLE `mdl_mnet_host`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mdl_mnet_host2service`
--
ALTER TABLE `mdl_mnet_host2service`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_mnet_log`
--
ALTER TABLE `mdl_mnet_log`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_mnet_rpc`
--
ALTER TABLE `mdl_mnet_rpc`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `mdl_mnet_service`
--
ALTER TABLE `mdl_mnet_service`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `mdl_mnet_service2rpc`
--
ALTER TABLE `mdl_mnet_service2rpc`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `mdl_mnet_session`
--
ALTER TABLE `mdl_mnet_session`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_mnet_sso_access_control`
--
ALTER TABLE `mdl_mnet_sso_access_control`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_modules`
--
ALTER TABLE `mdl_modules`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `mdl_mplayer`
--
ALTER TABLE `mdl_mplayer`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=556;
--
-- AUTO_INCREMENT for table `mdl_openmeetings`
--
ALTER TABLE `mdl_openmeetings`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_post`
--
ALTER TABLE `mdl_post`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1010;
--
-- AUTO_INCREMENT for table `mdl_protrack_upload_temp`
--
ALTER TABLE `mdl_protrack_upload_temp`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53953;
--
-- AUTO_INCREMENT for table `mdl_question`
--
ALTER TABLE `mdl_question`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68659;
--
-- AUTO_INCREMENT for table `mdl_questionnaire`
--
ALTER TABLE `mdl_questionnaire`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;
--
-- AUTO_INCREMENT for table `mdl_questionnaire_attempts`
--
ALTER TABLE `mdl_questionnaire_attempts`
  MODIFY `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20181;
--
-- AUTO_INCREMENT for table `mdl_questionnaire_question`
--
ALTER TABLE `mdl_questionnaire_question`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1418;
--
-- AUTO_INCREMENT for table `mdl_questionnaire_question_type`
--
ALTER TABLE `mdl_questionnaire_question_type`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `mdl_questionnaire_quest_choice`
--
ALTER TABLE `mdl_questionnaire_quest_choice`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3395;
--
-- AUTO_INCREMENT for table `mdl_questionnaire_response`
--
ALTER TABLE `mdl_questionnaire_response`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20227;
--
-- AUTO_INCREMENT for table `mdl_questionnaire_response_bool`
--
ALTER TABLE `mdl_questionnaire_response_bool`
  MODIFY `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=984;
--
-- AUTO_INCREMENT for table `mdl_questionnaire_response_date`
--
ALTER TABLE `mdl_questionnaire_response_date`
  MODIFY `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1220;
--
-- AUTO_INCREMENT for table `mdl_questionnaire_response_other`
--
ALTER TABLE `mdl_questionnaire_response_other`
  MODIFY `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_questionnaire_response_rank`
--
ALTER TABLE `mdl_questionnaire_response_rank`
  MODIFY `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26935;
--
-- AUTO_INCREMENT for table `mdl_questionnaire_response_text`
--
ALTER TABLE `mdl_questionnaire_response_text`
  MODIFY `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73290;
--
-- AUTO_INCREMENT for table `mdl_questionnaire_resp_multiple`
--
ALTER TABLE `mdl_questionnaire_resp_multiple`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=390;
--
-- AUTO_INCREMENT for table `mdl_questionnaire_resp_single`
--
ALTER TABLE `mdl_questionnaire_resp_single`
  MODIFY `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92425;
--
-- AUTO_INCREMENT for table `mdl_questionnaire_survey`
--
ALTER TABLE `mdl_questionnaire_survey`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;
--
-- AUTO_INCREMENT for table `mdl_question_answers`
--
ALTER TABLE `mdl_question_answers`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=227084;
--
-- AUTO_INCREMENT for table `mdl_question_attempts`
--
ALTER TABLE `mdl_question_attempts`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73824;
--
-- AUTO_INCREMENT for table `mdl_question_calculated`
--
ALTER TABLE `mdl_question_calculated`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_question_categories`
--
ALTER TABLE `mdl_question_categories`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1778;
--
-- AUTO_INCREMENT for table `mdl_question_datasets`
--
ALTER TABLE `mdl_question_datasets`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_question_dataset_definitions`
--
ALTER TABLE `mdl_question_dataset_definitions`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_question_dataset_items`
--
ALTER TABLE `mdl_question_dataset_items`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_question_match`
--
ALTER TABLE `mdl_question_match`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `mdl_question_match_sub`
--
ALTER TABLE `mdl_question_match_sub`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `mdl_question_multianswer`
--
ALTER TABLE `mdl_question_multianswer`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_question_multichoice`
--
ALTER TABLE `mdl_question_multichoice`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56667;
--
-- AUTO_INCREMENT for table `mdl_question_numerical`
--
ALTER TABLE `mdl_question_numerical`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_question_numerical_units`
--
ALTER TABLE `mdl_question_numerical_units`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_question_randomsamatch`
--
ALTER TABLE `mdl_question_randomsamatch`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_question_sessions`
--
ALTER TABLE `mdl_question_sessions`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1514567;
--
-- AUTO_INCREMENT for table `mdl_question_shortanswer`
--
ALTER TABLE `mdl_question_shortanswer`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=249;
--
-- AUTO_INCREMENT for table `mdl_question_states`
--
ALTER TABLE `mdl_question_states`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4275590;
--
-- AUTO_INCREMENT for table `mdl_question_truefalse`
--
ALTER TABLE `mdl_question_truefalse`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1244;
--
-- AUTO_INCREMENT for table `mdl_quiz`
--
ALTER TABLE `mdl_quiz`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=419;
--
-- AUTO_INCREMENT for table `mdl_quiz_attempts`
--
ALTER TABLE `mdl_quiz_attempts`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73823;
--
-- AUTO_INCREMENT for table `mdl_quiz_feedback`
--
ALTER TABLE `mdl_quiz_feedback`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28158;
--
-- AUTO_INCREMENT for table `mdl_quiz_grades`
--
ALTER TABLE `mdl_quiz_grades`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32905;
--
-- AUTO_INCREMENT for table `mdl_quiz_question_instances`
--
ALTER TABLE `mdl_quiz_question_instances`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30712;
--
-- AUTO_INCREMENT for table `mdl_quiz_question_versions`
--
ALTER TABLE `mdl_quiz_question_versions`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_realtimeattendance`
--
ALTER TABLE `mdl_realtimeattendance`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_realtimeattendance_answer`
--
ALTER TABLE `mdl_realtimeattendance_answer`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_realtimeattendance_question`
--
ALTER TABLE `mdl_realtimeattendance_question`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_realtimeattendance_session`
--
ALTER TABLE `mdl_realtimeattendance_session`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_realtimeattendance_submitted`
--
ALTER TABLE `mdl_realtimeattendance_submitted`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_resource`
--
ALTER TABLE `mdl_resource`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4777;
--
-- AUTO_INCREMENT for table `mdl_role`
--
ALTER TABLE `mdl_role`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `mdl_role_allow_assign`
--
ALTER TABLE `mdl_role_allow_assign`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;
--
-- AUTO_INCREMENT for table `mdl_role_allow_override`
--
ALTER TABLE `mdl_role_allow_override`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;
--
-- AUTO_INCREMENT for table `mdl_role_assignments`
--
ALTER TABLE `mdl_role_assignments`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1465061;
--
-- AUTO_INCREMENT for table `mdl_role_capabilities`
--
ALTER TABLE `mdl_role_capabilities`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4991;
--
-- AUTO_INCREMENT for table `mdl_role_names`
--
ALTER TABLE `mdl_role_names`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `mdl_role_sortorder`
--
ALTER TABLE `mdl_role_sortorder`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_scale`
--
ALTER TABLE `mdl_scale`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mdl_scale_history`
--
ALTER TABLE `mdl_scale_history`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_scheduler`
--
ALTER TABLE `mdl_scheduler`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_scheduler_appointment`
--
ALTER TABLE `mdl_scheduler_appointment`
  MODIFY `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_scheduler_slots`
--
ALTER TABLE `mdl_scheduler_slots`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_scorm`
--
ALTER TABLE `mdl_scorm`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3780;
--
-- AUTO_INCREMENT for table `mdl_scorm_scoes`
--
ALTER TABLE `mdl_scorm_scoes`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12050;
--
-- AUTO_INCREMENT for table `mdl_scorm_scoes_data`
--
ALTER TABLE `mdl_scorm_scoes_data`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16155;
--
-- AUTO_INCREMENT for table `mdl_scorm_scoes_track`
--
ALTER TABLE `mdl_scorm_scoes_track`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48885331;
--
-- AUTO_INCREMENT for table `mdl_scorm_seq_mapinfo`
--
ALTER TABLE `mdl_scorm_seq_mapinfo`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_skillsoft`
--
ALTER TABLE `mdl_skillsoft`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15609;
--
-- AUTO_INCREMENT for table `mdl_skillsoft_au_track`
--
ALTER TABLE `mdl_skillsoft_au_track`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1470060;
--
-- AUTO_INCREMENT for table `mdl_skillsoft_session_track`
--
ALTER TABLE `mdl_skillsoft_session_track`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=337095;
--
-- AUTO_INCREMENT for table `mdl_subcourse`
--
ALTER TABLE `mdl_subcourse`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4154;
--
-- AUTO_INCREMENT for table `mdl_survey`
--
ALTER TABLE `mdl_survey`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=387;
--
-- AUTO_INCREMENT for table `mdl_survey_analysis`
--
ALTER TABLE `mdl_survey_analysis`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mdl_survey_questions`
--
ALTER TABLE `mdl_survey_questions`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5549;
--
-- AUTO_INCREMENT for table `mdl_swf`
--
ALTER TABLE `mdl_swf`
  MODIFY `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `mdl_tag`
--
ALTER TABLE `mdl_tag`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2307;
--
-- AUTO_INCREMENT for table `mdl_tracker`
--
ALTER TABLE `mdl_tracker`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `mdl_tracker_element`
--
ALTER TABLE `mdl_tracker_element`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mdl_tracker_issue`
--
ALTER TABLE `mdl_tracker_issue`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `mdl_user`
--
ALTER TABLE `mdl_user`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=140053;
--
-- AUTO_INCREMENT for table `mdl_user_info_category`
--
ALTER TABLE `mdl_user_info_category`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mdl_wiki`
--
ALTER TABLE `mdl_wiki`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
